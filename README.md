# bsg_fpga #
Create FPGA-bitstreams for the Xilinx ML605 and DoubleTrouble boards.

## Working projects 07/24/2017 ##
* bsg_ml605
    * bsg_two_loopback_new
* bsg_zedboard
    * bsg_two_coyote_accum
    * bsg_two_rocket
    * bsg_rocket_baseline
    * bsg_rocket_node
    * bsg_rocket_gateway

Other projects require some plumbing work in order to work...

### Setup ###
* Here are the building tools required and supported by this repository:
    * Synopsys VCS J-2014.12-SP2 (simulation)
    * Synopsys Synplify J-2014.09-SPI (synthesis)
    * Synopsys DesignCompiler G-2012.06-SP5-4 (DesignWare)
    * Xilinx ISE 14.7 (place and route)
    * Xilinx Vivado 2014.4 (synthesis, place and route for Zynq7000)
* Here are the tools for programming and debugging:
    * Xilinx Impact (programming)
    * Xilinx ChipScope Pro Analyzer (JTAG debugging)
* Normally, projects rely on hardware modules located on the [bsg_ip_cores](https://bitbucket.org/taylor-bsg/bsg_ip_cores) repository
* This [Makefile](https://bitbucket.org/taylor-bsg/bsg_fpga/src/master/project/Makefile.include)
defines tool path/license and [bsg_ip_cores](https://bitbucket.org/taylor-bsg/bsg_ip_cores) paths

### Folders ###

* ip: contains hardware modules and software code (microblaze) for projects\
* project: contains projects based on the supported boards
    * Every project should have its own README file with detailed information
    * Check the [wiki](https://bitbucket.org/taylor-bsg/bsg_fpga/wiki/Home) for
    architecture diagrams of complex projects

### FPGA-device name mapping ###

* bsg_ml605
    * Board: Xilinx ML605
    * Tech: Virtex6
    * Device: xc6vlx240t
    * Package: ff1156
    * Speed grade: -1
* bsg_gateway
    * Board: DoubleTrouble
    * Tech: Spartan6
    * Device: xc6slx150
    * Package: fgg676
    * Speed grade: -3
* bsg_asic
    * Board: DoubleTrouble
    * Tech: Spartan6
    * Device: xc6slx150
    * Package: fgg484
    * Speed grade: -3
* bsg_zedboard
    * Board: Zedboard
    * Tech: Zynq7000
    * Device: xc7z020
    * Package: clg484
    * Speed grade: -1

### Host name mapping ###

* bsg_host
    * OS: Centos 6.7
    * Kernel: 2.6.32-573.12.1.el6.x86_64
