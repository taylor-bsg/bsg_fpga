set xilinx_vivado_src_dir $::env(XILINX_VIVADO)/data/verilog/src
set xilinx_vivado_unisims_dir $::env(XILINX_VIVADO)/data/verilog/src/unisims
set xilinx_vivado_secure_dir $::env(XILINX_VIVADO)/data/secureip

set VIVADO_SIM_FILES [join "
  $xilinx_vivado_src_dir/glbl.v
  $xilinx_vivado_unisims_dir/MMCME2_ADV.v
  $xilinx_vivado_unisims_dir/BUFG.v
"]
