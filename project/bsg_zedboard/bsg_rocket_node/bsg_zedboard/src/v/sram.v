module DataArray_T9(
  input CLK,
  input RST,
  input init,
  input [7:0] W0A,
  input W0E,
  input [127:0] W0I,
  input [127:0] W0M,
  input [7:0] R1A,
  input R1E,
  output [127:0] R1O
);

`ifndef SYNTHESIS
integer i;
integer j;
always @(posedge CLK) begin
  for (i=0; i<2; i=i+64) begin
    for (j=1; j<64; j=j+1) begin
      if (W0M[i] != W0M[i+j]) begin
        $fwrite(32'h80000002, "ASSERTION FAILED: write mask granularity\n");
        $finish;
      end
    end
  end
end
`endif

reg [127:0] ram [255:0];
  `ifndef SYNTHESIS
    integer initvar;
    initial begin
      #0.002;
      for (initvar = 0; initvar < 256; initvar = initvar+1)
        ram[initvar] = {4 {$random}};
    end
  `endif
  reg [7:0] reg_R1A;
always @(posedge CLK) begin
  if (R1E) reg_R1A <= R1A;
  if (W0E && W0M[0]) ram[W0A][63:0] <= W0I[63:0];
  if (W0E && W0M[64]) ram[W0A][127:64] <= W0I[127:64];
end
assign R1O = ram[reg_R1A];

endmodule


module MetadataArray_tag_arr(
  input CLK,
  input RST,
  input init,
  input [5:0] W0A,
  input W0E,
  input [87:0] W0I,
  input [87:0] W0M,
  input [5:0] R1A,
  input R1E,
  output [87:0] R1O
);

`ifndef SYNTHESIS
integer i;
integer j;
always @(posedge CLK) begin
  for (i=0; i<4; i=i+22) begin
    for (j=1; j<22; j=j+1) begin
      if (W0M[i] != W0M[i+j]) begin
        $fwrite(32'h80000002, "ASSERTION FAILED: write mask granularity\n");
        $finish;
      end
    end
  end
end
`endif

reg [87:0] ram [63:0];
  `ifndef SYNTHESIS
    integer initvar;
    initial begin
      #0.002;
      for (initvar = 0; initvar < 64; initvar = initvar+1)
        ram[initvar] = {3 {$random}};
    end
  `endif
  reg [5:0] reg_R1A;
always @(posedge CLK) begin
  if (R1E) reg_R1A <= R1A;
  if (W0E && W0M[0]) ram[W0A][21:0] <= W0I[21:0];
  if (W0E && W0M[22]) ram[W0A][43:22] <= W0I[43:22];
  if (W0E && W0M[44]) ram[W0A][65:44] <= W0I[65:44];
  if (W0E && W0M[66]) ram[W0A][87:66] <= W0I[87:66];
end
assign R1O = ram[reg_R1A];

endmodule


module ICache_T198(
  input CLK,
  input RST,
  input init,
  input [7:0] RW0A,
  input RW0E,
  input RW0W,
  input [127:0] RW0I,
  output [127:0] RW0O
);

reg [127:0] ram [255:0];
  `ifndef SYNTHESIS
    integer initvar;
    initial begin
      #0.002;
      for (initvar = 0; initvar < 256; initvar = initvar+1)
        ram[initvar] = {4 {$random}};
    end
  `endif
  reg [7:0] reg_RW0A;
always @(posedge CLK) begin
  if (RW0E && !RW0W) reg_RW0A <= RW0A;
  if (RW0E && RW0W) ram[RW0A] <= RW0I;
end
assign RW0O = ram[reg_RW0A];

endmodule


module ICache_tag_array(
  input CLK,
  input RST,
  input init,
  input [5:0] RW0A,
  input RW0E,
  input RW0W,
  input [79:0] RW0M,
  input [79:0] RW0I,
  output [79:0] RW0O
);

`ifndef SYNTHESIS
integer i;
integer j;
always @(posedge CLK) begin
  for (i=0; i<4; i=i+20) begin
    for (j=1; j<20; j=j+1) begin
      if (RW0M[i] != RW0M[i+j]) begin
        $fwrite(32'h80000002, "ASSERTION FAILED: write mask granularity\n");
        $finish;
      end
    end
  end
end
`endif

reg [79:0] ram [63:0];
  `ifndef SYNTHESIS
    integer initvar;
    initial begin
      #0.002;
      for (initvar = 0; initvar < 64; initvar = initvar+1)
        ram[initvar] = {3 {$random}};
    end
  `endif
  reg [5:0] reg_RW0A;
always @(posedge CLK) begin
  if (RW0E && !RW0W) reg_RW0A <= RW0A;
  if (RW0E && RW0W && RW0M[0]) ram[RW0A][19:0] <= RW0I[19:0];
  if (RW0E && RW0W && RW0M[20]) ram[RW0A][39:20] <= RW0I[39:20];
  if (RW0E && RW0W && RW0M[40]) ram[RW0A][59:40] <= RW0I[59:40];
  if (RW0E && RW0W && RW0M[60]) ram[RW0A][79:60] <= RW0I[79:60];
end
assign RW0O = ram[reg_RW0A];

endmodule
