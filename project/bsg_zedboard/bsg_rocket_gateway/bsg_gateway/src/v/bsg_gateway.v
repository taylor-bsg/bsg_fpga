// bsg_gateway has FMC-support for these two boards:
//   * Xilinx ML605 (bsg_ml605)
//   * Digilent Zedboard (bsg_zedboard)
//
// BSG_ML605_FMC macro sets pinout for ML605
// BSG_ZEDBOARD_FMC macro sets pinout for Zedboard

module bsg_gateway
# (parameter nodes_p=1
  ,parameter enabled_at_start_vec_p=1'b0
  ,parameter num_channels_p=4
  ,parameter channel_width_p=8
  ,parameter ring_bytes_p=10
  ,parameter ring_width_p=ring_bytes_p*channel_width_p)
  (input CLK_OSC_P, CLK_OSC_N

  // LED
  ,output FPGA_LED2
  ,output FPGA_LED3

  // fmc reset in
  ,input F20_P, F20_N
  // fmc host reset out
  ,output F23_P, F23_N
  // fmc tx clk in
  ,input FCLK0_M2C_P, FCLK0_M2C_N

`ifdef BSG_ML605_FMC
  // fmc tx clk out
  ,output FCLK1_M2C_P, FCLK1_M2C_N
  // fmc tx data out [0]
  ,output F0_P, F0_N
`else
`ifdef BSG_ZEDBOARD_FMC
  // fmc tx clk out
  ,output F0_P, F0_N
  // fmc tx data out [0]
  ,output F1_P, F1_N
`endif
`endif

  // fmc tx data out [9:1]
  ,output F16_P, F16_N
  ,output F15_P, F15_N
  ,output F13_P, F13_N
  ,output F11_P, F11_N
  ,output F10_P, F10_N
  ,output F14_P, F14_N
  ,output F9_P, F9_N
  ,output F4_P, F4_N
  ,output F7_P, F7_N
  ,output F8_P, F8_N
  // fmc rx clk in
  ,input F17_P, F17_N
  // fmc rx data in
  ,input F31_P, F31_N
  ,input F33_P, F33_N
  ,input F30_P, F30_N
  ,input F32_P, F32_N
  ,input F28_P, F28_N
  ,input F25_P, F25_N
  ,input F29_P, F29_N
  ,input F26_P, F26_N
  ,input F21_P, F21_N
  ,input F27_P, F27_N
  ,input F22_P, F22_N
`ifdef SIMULATION
  );
`else
  // ports used by microblaze
  // reset
  ,input PWR_RSTN
  // voltage-rail enable
  ,output logic ASIC_CORE_EN, ASIC_IO_EN
  // current monitor
  ,output logic CUR_MON_ADDR0, CUR_MON_ADDR1
  ,inout CUR_MON_SCL, CUR_MON_SDA
  // potentiometer
  ,output logic DIG_POT_ADDR0, DIG_POT_ADDR1
  ,output logic DIG_POT_INDEP, DIG_POT_NRST
  ,inout DIG_POT_SCL, DIG_POT_SDA
  // uart
  ,input UART_RX
  ,output UART_TX
  // led
  ,output logic FPGA_LED0, FPGA_LED1);
`endif

  // clock generation

  wire mb_clk;
  wire core_clk;
  wire locked;

  bsg_gateway_clk clk
    (.clk_150_mhz_p_i(CLK_OSC_P) ,.clk_150_mhz_n_i(CLK_OSC_N)
    ,.core_clk_o(core_clk)
    ,.mb_clk_o(mb_clk)
    ,.locked_o(locked));

  // fmc

  wire fmc_reset, core_calib_done_r;

  assign core_calib_done_r = ~fmc_reset;

  wire                    core_valid;
  wire [ring_width_p-1:0] core_data;
  wire                    core_ready;

  wire                    fmc_valid;
  wire [ring_width_p-1:0] fmc_data;
  wire                    fmc_ready;

  bsg_gateway_fmc fmc
    (.clk_i(core_clk)
    // fmc reset out
    ,.fmc_reset_o(fmc_reset)
    // host reset in
    ,.host_reset_i(~core_calib_done_r)
    // data in
    ,.valid_i(core_valid)
    ,.data_i(core_data)
    ,.ready_o(core_ready)
    // data out
    ,.valid_o(fmc_valid)
    ,.data_o(fmc_data)
    ,.ready_i(fmc_ready)
    // fmc reset in
    ,.F20_P(F20_P) ,.F20_N(F20_N)
    // fmc host reset out
    ,.F23_P(F23_P) ,.F23_N(F23_N)
    // fmc tx clk in
    ,.FCLK0_M2C_P(FCLK0_M2C_P) ,.FCLK0_M2C_N(FCLK0_M2C_N)
    // fmc tx clk out
    ,.F0_P(F0_P) ,.F0_N(F0_N)
    // fmc tx data out [0]
    ,.F1_P(F1_P) ,.F1_N(F1_N)
    // fmc tx data out [9:1]
    ,.F16_P(F16_P) ,.F16_N(F16_N)
    ,.F15_P(F15_P) ,.F15_N(F15_N)
    ,.F13_P(F13_P) ,.F13_N(F13_N)
    ,.F11_P(F11_P) ,.F11_N(F11_N)
    ,.F10_P(F10_P) ,.F10_N(F10_N)
    ,.F14_P(F14_P) ,.F14_N(F14_N)
    ,.F9_P(F9_P) ,.F9_N(F9_N)
    ,.F4_P(F4_P) ,.F4_N(F4_N)
    ,.F7_P(F7_P) ,.F7_N(F7_N)
    ,.F8_P(F8_P) ,.F8_N(F8_N)
    // fmc rx clk in
    ,.F17_P(F17_P) ,.F17_N(F17_N)
    // fmc rx data in
    ,.F31_P(F31_P) ,.F31_N(F31_N)
    ,.F33_P(F33_P) ,.F33_N(F33_N)
    ,.F30_P(F30_P) ,.F30_N(F30_N)
    ,.F32_P(F32_P) ,.F32_N(F32_N)
    ,.F28_P(F28_P) ,.F28_N(F28_N)
    ,.F25_P(F25_P) ,.F25_N(F25_N)
    ,.F29_P(F29_P) ,.F29_N(F29_N)
    ,.F26_P(F26_P) ,.F26_N(F26_N)
    ,.F21_P(F21_P) ,.F21_N(F21_N)
    ,.F27_P(F27_P) ,.F27_N(F27_N)
    ,.F22_P(F22_P) ,.F22_N(F22_N));

  // fsb

  wire                    asm_valid;
  wire [ring_width_p-1:0] asm_data;
  wire                    asm_yumi;

  bsg_two_fifo #
    (.width_p(ring_width_p))
  btf
    (.clk_i(core_clk)
    ,.reset_i(fmc_reset)
    // in
    ,.v_i(fmc_valid)
    ,.data_i(fmc_data)
    ,.ready_o(fmc_ready)
    // out
    ,.v_o(asm_valid)
    ,.data_o(asm_data)
    ,.yumi_i(asm_yumi));

  // into rocket nodes (fsb data)
  wire [nodes_p-1:0]      core_node_v_A;
  wire [ring_width_p-1:0] core_node_data_A [nodes_p-1:0];
  wire [nodes_p-1:0]      core_node_ready_A;

  // into rocket nodes (fsb control)
  wire [nodes_p-1:0]      core_node_reset_r;
  wire [nodes_p-1:0]      core_node_en_r;

  // out of rocket nodes (fsb data)
  wire [nodes_p-1:0]      core_node_v_B;
  wire [ring_width_p-1:0] core_node_data_B [nodes_p-1:0];
  wire [nodes_p-1:0]      core_node_yumi_B;

  bsg_fsb #
    (.width_p(ring_width_p)
    ,.nodes_p(nodes_p)
    ,.snoop_vec_p( { nodes_p { 1'b0 } })
    // if master, enable at startup so that it can drive things
    ,.enabled_at_start_vec_p(enabled_at_start_vec_p))
  fsb
    (.clk_i(core_clk)
    ,.reset_i(fmc_reset)
    // asm in
    ,.asm_v_i(asm_valid)
    ,.asm_data_i(asm_data)
    ,.asm_yumi_o(asm_yumi)
    // asm out
    ,.asm_v_o(core_valid)
    ,.asm_data_o(core_data)
    ,.asm_ready_i(core_ready)
    // node ctrl
    ,.node_reset_r_o(core_node_reset_r)
    ,.node_en_r_o(core_node_en_r)
    // node in
    ,.node_v_i(core_node_v_B)
    ,.node_data_i(core_node_data_B)
    ,.node_yumi_o(core_node_yumi_B)
    // node out
    ,.node_v_o(core_node_v_A)
    ,.node_data_o(core_node_data_A)
    ,.node_ready_i(core_node_ready_A));

  // rocket node client

  bsg_rocket_node_client #
    (.dest_id_p(0))
  clnt
    (.clk_i(core_clk)
    ,.reset_i(core_node_reset_r[0])
    ,.en_i(core_node_en_r[0])
    // in
    ,.v_i(core_node_v_A[0])
    ,.data_i(core_node_data_A[0])
    ,.ready_o(core_node_ready_A[0])
    // out
    ,.v_o(core_node_v_B[0])
    ,.data_o(core_node_data_B[0])
    ,.yumi_i(core_node_yumi_B[0]));

  // led

  assign FPGA_LED2 = fmc_reset;
  assign FPGA_LED3 = core_calib_done_r;

`ifndef SIMULATION

  // microblaze

  wire [11:0] gpio;
  wire cpu_override_output_p;
  wire cpu_override_output_n;

  assign cpu_override_output_p = gpio[11];
  assign cpu_override_output_n = gpio[10];

  always_comb begin
    FPGA_LED0 = 1'b1;
    FPGA_LED1 = 1'b1;
    DIG_POT_INDEP = 1'b1;
    DIG_POT_NRST = 1'b1;
    DIG_POT_ADDR0 = 1'b1;
    DIG_POT_ADDR1 = 1'b1;
    CUR_MON_ADDR0 = 1'b1;
    CUR_MON_ADDR1 = 1'b1;
    ASIC_IO_EN = 1'b1;
    ASIC_CORE_EN = 1'b1;
    if (cpu_override_output_p == 1'b1 && cpu_override_output_n == 1'b0 && PWR_RSTN == 1'b1) begin
      FPGA_LED0 = gpio[0];
      FPGA_LED1 = gpio[1];
      DIG_POT_INDEP = gpio[2];
      DIG_POT_NRST = gpio[3];
      DIG_POT_ADDR0 = gpio[4];
      DIG_POT_ADDR1 = gpio[5];
      CUR_MON_ADDR0 = gpio[6];
      CUR_MON_ADDR1 = gpio[7];
      ASIC_IO_EN = gpio[8];
      ASIC_CORE_EN = gpio[9];
    end
  end

  (* BOX_TYPE = "user_black_box" *)
  board_ctrl board_ctrl_i
    (.RESET(PWR_RSTN)
    ,.CLK_50(mb_clk)
    ,.CLK_LOCKED(locked)
    ,.axi_iic_dig_pot_Gpo_pin()
    ,.axi_iic_dig_pot_Sda_pin(DIG_POT_SDA)
    ,.axi_iic_dig_pot_Scl_pin(DIG_POT_SCL)
    ,.axi_iic_cur_mon_Gpo_pin()
    ,.axi_iic_cur_mon_Sda_pin(CUR_MON_SDA)
    ,.axi_iic_cur_mon_Scl_pin(CUR_MON_SCL)
    ,.axi_gpio_0_GPIO_IO_O_pin(gpio)
    ,.axi_gpio_0_GPIO2_IO_I_pin()
    ,.axi_uartlite_0_RX_pin(UART_RX)
    ,.axi_uartlite_0_TX_pin(UART_TX));

  // chipscope

  bsg_gateway_chipscope cs
    (.clk_i(core_clk)
    ,.data_i({'0
             ,core_calib_done_r
             ,core_ready
             ,core_valid
             ,core_data
             ,fmc_ready
             ,fmc_valid
             ,fmc_data}));

`endif

endmodule
