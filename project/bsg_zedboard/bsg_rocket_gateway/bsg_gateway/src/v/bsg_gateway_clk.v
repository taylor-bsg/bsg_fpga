//------------------------------------------------------------
// University of California, San Diego - Bespoke Systems Group
//------------------------------------------------------------
// File: bsg_gateway_clk.v
//
// - PLL_ADV generates the following clocks:
//     * ext_core_clk_o
//     * int_core_clk_o
//     * mb_clk_o
//
// - DCM_CLKGEN generates the following clocks:
//     * ext_io_master_clk_o
//     * int_io_master_clk_o
//
// - Both PLL_ADV and DCM_CLKGEN uses the 150MHz clock coming
//   from the oscillator in doubletrouble.
//
// - Check pll_*_lp and dcm_*_lp local parameters for tweaking
//   clocks
//
// Author: Luis Vega - lvgutierrez@eng.ucsd.edu
//------------------------------------------------------------

module bsg_gateway_clk
  (input clk_150_mhz_p_i, clk_150_mhz_n_i
  // core clock
  ,output core_clk_o
  // microblaze clock
  ,output mb_clk_o
  // locked
  ,output locked_o);

  wire ibufgds_clk_150_mhz;

  IBUFGDS #
    (.DIFF_TERM("TRUE"))
  ibufgds
    (.I(clk_150_mhz_p_i) ,.IB(clk_150_mhz_n_i)
    ,.O(ibufgds_clk_150_mhz));

  // 150Mhz * pll_mult_lp
  // if pll_mult_lp=7 then pll-internal-clock is 1050MHz
  localparam pll_mult_lp = 7;

  // 150MHz * (pll_mult_lp/pll_core_clk_divide_lp)
  // if pll_mult_lp=7 and pll_core_clk_divide_lp=21,
  // then (int/ext) core_clk_o are 50MHz
  localparam pll_core_clk_divide_lp = 21;

  // 150Mhz*(pll_mult_lp/pll_mb_clk_divide_lp)
  // if pll_mult_lp=7 and pll_mb_clk_divide_lp=21,
  // then mb_clk_o is 50MHz
  localparam pll_mb_clk_divide_lp = 21;

  wire pll_fb;
  wire pll_core_clk;
  wire pll_mb_clk;

  PLL_ADV #
    (.BANDWIDTH("OPTIMIZED")
    ,.CLKFBOUT_MULT(pll_mult_lp)
    ,.CLKFBOUT_PHASE(0.0)
    ,.CLKIN1_PERIOD(6.667)
    ,.CLKIN2_PERIOD(6.667)
    // clk 0
    ,.CLKOUT0_DIVIDE(pll_core_clk_divide_lp)
    ,.CLKOUT0_DUTY_CYCLE(0.5)
    ,.CLKOUT0_PHASE(0.0)
    // clk 1
    ,.CLKOUT1_DIVIDE(pll_mb_clk_divide_lp)
    ,.CLKOUT1_DUTY_CYCLE(0.5)
    ,.CLKOUT1_PHASE(180.0)
    // not used
    ,.CLKOUT2_DIVIDE(1)
    ,.CLKOUT2_DUTY_CYCLE(0.5)
    ,.CLKOUT2_PHASE(0.0)
    ,.CLKOUT3_DIVIDE(1)
    ,.CLKOUT3_DUTY_CYCLE(0.5)
    ,.CLKOUT3_PHASE(0.0)
    ,.CLKOUT4_DIVIDE(1)
    ,.CLKOUT4_DUTY_CYCLE(0.5)
    ,.CLKOUT4_PHASE(0.0)
    ,.CLKOUT5_DIVIDE(1)
    ,.CLKOUT5_DUTY_CYCLE(0.5)
    ,.CLKOUT5_PHASE(0.0)
    ,.COMPENSATION("INTERNAL")
    ,.DIVCLK_DIVIDE(1)
    ,.REF_JITTER(0.100)
    ,.SIM_DEVICE("SPARTAN6"))
  pll
    (.CLKFBDCM()
    ,.CLKFBOUT(pll_fb)
    // core clk
    ,.CLKOUT0(pll_core_clk)
    // mb clk
    ,.CLKOUT1(pll_mb_clk)
    // not used
    ,.CLKOUT2()
    ,.CLKOUT3()
    ,.CLKOUT4()
    ,.CLKOUT5()
    ,.CLKOUTDCM0()
    ,.CLKOUTDCM1()
    ,.CLKOUTDCM2()
    ,.CLKOUTDCM3()
    ,.CLKOUTDCM4()
    ,.CLKOUTDCM5()
    ,.DO()
    ,.DRDY()
    ,.LOCKED(locked_o)
    ,.CLKFBIN(pll_fb)
    ,.CLKIN1(ibufgds_clk_150_mhz)
    ,.CLKIN2(1'b0)
    ,.CLKINSEL(1'b1)
    ,.DADDR(5'b00000)
    ,.DCLK(1'b0)
    ,.DEN(1'b0)
    ,.DI(16'h0000)
    ,.DWE(1'b0)
    ,.RST(1'b0)
    ,.REL(1'b0));

  // core clock

  BUFG bufg_cc
    (.I(pll_core_clk)
    ,.O(core_clk_o));

  // mb clock

  BUFG bufg_mb
    (.I(pll_mb_clk)
    ,.O(mb_clk_o));

endmodule
