### Source dependencies and versioning ###

This project support versioning, which means that uses snapshots (sha-commits)
from other bsg-repositories. The reason why this is required is for preserving
a known state for emulation efforts. The following [Makefile](https://bitbucket.org/taylor-bsg/bsg_fpga/src/master/project/bsg_zedboard/bsg_two_coyote_accum/Makefile)
defines the commits being used to build this system.

After pulling the required dependencies, a developer may change sources under
the source tree to test new functionality.

### How to build ###

Currently, bb-39 node is the one hosting Xilinx 6-series licenses. Any other
node can build 7-series designs.

1. Go to the required node for building
2. Build a bitstream for a board by running `make -C bsg_asic`
3. Since rocket-based-designs takes a bit more time, you can split bsg_asic building in
two steps: synth and bitstream, since bitstream generation flow is the only one
requiring bb-39. For example, you can:

* Go to bb-91
* Run `make -C bsg_asic synth`
* Go to bb-39
* Run `make -C bsg_asic bitstream`

### How to test ###

1. Go to testing directory
2. Run `make`

### How to run zedboard ###

1. Copy boot.bin in SDCard and attach to the Zedboard
2. Configure the computer to be connected to Zedboard as gateway (192.168.1.1)
3. Connect ethernet cable between computer and Zedboard
4. ssh to zedboard (192.168.1.5), check RISCV for more info
5. Run `fesvr-zynq pk hello`
