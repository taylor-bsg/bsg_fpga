# scripts for creating filelist and library
source $::env(BSG_TREE_DIR)/bsg_rocket/common/tcl/bsg_vcs_create_filelist_library.tcl

# zedboard rtl
source $::env(BSG_ZEDBOARD_DIR)/src/tcl/filelist.tcl
source $::env(BSG_ZEDBOARD_DIR)/src/tcl/include.tcl

# zedboard rtl filelist
bsg_create_filelist $::env(ZB_RTL_LIST) \
                    $ZB_RTL_FILES

# zedboard rtl library
bsg_create_library $::env(ZB_RTL_LIB_NAME) \
                   $::env(ZB_RTL_LIB) \
                   $ZB_RTL_FILES \
                   $ZB_RTL_INCLUDE

# gateway rtl
source $::env(BSG_GATEWAY_DIR)/src/tcl/filelist.tcl
source $::env(BSG_GATEWAY_DIR)/src/tcl/include.tcl

# gateway rtl filelist
bsg_create_filelist $::env(GW_RTL_LIST) \
                    $GW_RTL_FILES

# gateway library
bsg_create_library $::env(GW_RTL_LIB_NAME) \
                   $::env(GW_RTL_LIB) \
                   $GW_RTL_FILES \
                   $GW_RTL_INCLUDE

# xilinx ise sim
source $::env(BSG_TEST_DIR)/src/tcl/xilinx_ise_sim_filelist.tcl

# xilinx ise sim filelist
bsg_create_filelist $::env(ISE_SIM_LIST) \
                    $ISE_SIM_FILES

# xilinx ise sim library
bsg_create_library $::env(ISE_SIM_LIB_NAME) \
                   $::env(ISE_SIM_LIB) \
                   $ISE_SIM_FILES

# xilinx vivado sim
source $::env(BSG_TEST_DIR)/src/tcl/xilinx_vivado_sim_filelist.tcl

# xilinx vivado sim filelist
bsg_create_filelist $::env(VIVADO_SIM_LIST) \
                    $VIVADO_SIM_FILES

# xilinx vivado sim library
bsg_create_library $::env(VIVADO_SIM_LIB_NAME) \
                   $::env(VIVADO_SIM_LIB) \
                   $VIVADO_SIM_FILES
