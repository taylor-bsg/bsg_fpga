set xilinx_vivado_src_dir $::env(XILINX_VIVADO)/data/verilog/src
set xilinx_vivado_unisims_dir $::env(XILINX_VIVADO)/data/verilog/src/unisims
set xilinx_vivado_secure_dir $::env(XILINX_VIVADO)/data/secureip

set VIVADO_SIM_FILES [join "
  $xilinx_vivado_src_dir/glbl.v
  $xilinx_vivado_unisims_dir/MMCME2_ADV.v
  $xilinx_vivado_unisims_dir/IBUF.v
  $xilinx_vivado_unisims_dir/BUFG.v
  $xilinx_vivado_unisims_dir/ODDR.v
  $xilinx_vivado_unisims_dir/OBUFDS.v
  $xilinx_vivado_unisims_dir/IBUFDS.v
  $xilinx_vivado_unisims_dir/IDELAYCTRL.v
  $xilinx_vivado_unisims_dir/IDELAYE2.v
  $xilinx_vivado_unisims_dir/BUFIO.v
  $xilinx_vivado_unisims_dir/BUFR.v
  $xilinx_vivado_unisims_dir/OSERDESE2.v
  $xilinx_vivado_unisims_dir/ISERDESE2.v
  $xilinx_vivado_secure_dir/iserdese2/iserdese2_001.vp
  $xilinx_vivado_secure_dir/iserdese2/iserdese2_002.vp
  $xilinx_vivado_secure_dir/oserdese2/oserdese2_001.vp
  $xilinx_vivado_secure_dir/oserdese2/oserdese2_002.vp
"]
