set xilinx_ise_src_dir $::env(XILINX_ISE_DS_DIR)/ISE/verilog/src
set xilinx_ise_unisims_dir $::env(XILINX_ISE_DS_DIR)/ISE/verilog/src/unisims

set ISE_SIM_FILES [join "
  $xilinx_ise_unisims_dir/IBUFDS.v
  $xilinx_ise_unisims_dir/OBUFDS.v
  $xilinx_ise_unisims_dir/IBUFGDS.v
  $xilinx_ise_unisims_dir/IBUFDS_DIFF_OUT.v
  $xilinx_ise_unisims_dir/PLL_ADV.v
  $xilinx_ise_unisims_dir/OSERDES2.v
  $xilinx_ise_unisims_dir/ISERDES2.v
  $xilinx_ise_unisims_dir/IODELAY2.v
  $xilinx_ise_unisims_dir/BUFIO2_2CLK.v
  $xilinx_ise_unisims_dir/BUFIO2.v
  $xilinx_ise_unisims_dir/BUFG.v
"]
