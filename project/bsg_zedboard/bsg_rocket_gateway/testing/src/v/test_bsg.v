`include "test_five.vh"

`define HTIF_WIDTH 16
`define MEM_DATA_BITS 64
`define MEM_STRB_BITS 8
`define MEM_ADDR_BITS 32
`define MEM_ID_BITS 6

extern "A" void htif_fini(input reg failure);

`HTIF_TICK_DEFINITION(0, `HTIF_WIDTH)

`MEMORY_TICK_DEFINITION(0, `MEM_ADDR_BITS, `MEM_ID_BITS, `MEM_STRB_BITS, `MEM_DATA_BITS)

module test_bsg;

  // board oscillators

  wire ZB_OSC_CLK;
  bsg_nonsynth_clock_gen #(.cycle_time_p(`ZB_CLOCK_PERIOD_PS)) zb_osc (.o(ZB_OSC_CLK));

  wire GW_OSC_CLK;
  bsg_nonsynth_clock_gen #(.cycle_time_p(`GW_CLOCK_PERIOD_PS)) gw_osc (.o(GW_OSC_CLK));

  // bsg async reset

  wire zb_host_clk;
  wire host_reset;

  bsg_nonsynth_reset_gen #
    (.num_clocks_p(1)
    ,.reset_cycles_lo_p(0)
    ,.reset_cycles_hi_p(16))
  rgen
    (.clk_i(zb_host_clk)
    ,.async_reset_o(host_reset));

  logic reset = 1'b1;
  logic boot_done;

  initial begin
    wait (~boot_done);
    wait (boot_done) reset = 1'b0;
  end

  // log/waveform

  integer stderr = 32'h80000002;
  logic htif_verbose = 1'b0;
  logic mem_verbose = 1'b0;
  logic [1023:0] vcdplusfile = '0;
  initial begin
    htif_verbose = $test$plusargs("htif_verbose");
    mem_verbose = $test$plusargs("mem_verbose");
    if ($value$plusargs("vcdplusfile=%s", vcdplusfile)) begin
      $vcdplusfile(vcdplusfile);
      $vcdpluson(0);
      $vcdplusmemon(0);
    end
  end

  // host

  logic                   r2h_en = '1;

  logic                   r2h_valid;
  logic [`HTIF_WIDTH-1:0] r2h_data;
  logic                   r2h_ready;

  logic                   h2r_valid;
  logic [`HTIF_WIDTH-1:0] h2r_data;
  logic                   h2r_ready;

  logic            [31:0] h2r_exit = '0;

  always @(posedge zb_host_clk) begin
    if (reset) begin
      h2r_valid <= 1'b0;
      r2h_ready <= 1'b0;
      h2r_exit <= '0;
    end
    else begin
      htif_0_tick
        (r2h_en
        ,r2h_valid
        ,r2h_data
        ,r2h_ready
        ,h2r_valid
        ,h2r_data
        ,h2r_ready
        ,h2r_exit);
    end
  end

  always @(posedge zb_host_clk) begin
    if (htif_verbose) begin
      if (h2r_valid & h2r_ready)
        $fdisplay(stderr, "H2R: %04x", h2r_data);
      if (r2h_valid & r2h_ready)
        $fdisplay(stderr, "R2H: %04x", r2h_data);
    end
  end

  logic                      mem_aw_valid;
  logic [`MEM_ADDR_BITS-1:0] mem_aw_addr;
  logic   [`MEM_ID_BITS-1:0] mem_aw_id;
  logic                [2:0] mem_aw_size;
  logic                [7:0] mem_aw_len;
  logic                      mem_aw_ready;

  logic                      mem_w_valid;
  logic [`MEM_STRB_BITS-1:0] mem_w_strb;
  logic [`MEM_DATA_BITS-1:0] mem_w_data;
  logic                      mem_w_last;
  logic                      mem_w_ready;

  logic                      mem_b_valid;
  logic                [1:0] mem_b_resp;
  logic   [`MEM_ID_BITS-1:0] mem_b_id;
  logic                      mem_b_ready;

  logic                      mem_ar_valid;
  logic [`MEM_ADDR_BITS-1:0] mem_ar_addr;
  logic   [`MEM_ID_BITS-1:0] mem_ar_id;
  logic                [2:0] mem_ar_size;
  logic                [7:0] mem_ar_len;
  logic                      mem_ar_ready;

  logic                      mem_r_valid;
  logic                [1:0] mem_r_resp;
  logic   [`MEM_ID_BITS-1:0] mem_r_id;
  logic [`MEM_DATA_BITS-1:0] mem_r_data;
  logic                      mem_r_last;
  logic                      mem_r_ready;

  always @(posedge zb_host_clk) begin
    if (reset) begin
      mem_aw_ready <= 1'b0;
      mem_w_ready <= 1'b0;
      mem_b_valid <= 1'b0;
      mem_b_id <= 0;
      mem_b_resp <= 0;
      mem_ar_ready <= 1'b0;
      mem_r_valid <= 1'b0;
      mem_r_data <= 0;
    end
    else begin
      memory_0_tick
        (mem_aw_valid
        ,mem_aw_addr
        ,mem_aw_id
        ,mem_aw_size
        ,mem_aw_len
        ,mem_aw_ready
        ,mem_w_valid
        ,mem_w_strb
        ,mem_w_data
        ,mem_w_last
        ,mem_w_ready
        ,mem_b_valid
        ,mem_b_resp
        ,mem_b_id
        ,mem_b_ready
        ,mem_ar_valid
        ,mem_ar_addr
        ,mem_ar_id
        ,mem_ar_size
        ,mem_ar_len
        ,mem_ar_ready
        ,mem_r_valid
        ,mem_r_resp
        ,mem_r_id
        ,mem_r_data
        ,mem_r_last
        ,mem_r_ready);
    end
  end

  always @(posedge zb_host_clk) begin
    if (mem_verbose) begin

      if (mem_ar_valid && mem_ar_ready)
        $fdisplay(stderr, "MC: ar addr=%x id=%x size=%x len=%x"
        ,mem_ar_addr
        ,mem_ar_id
        ,mem_ar_size
        ,mem_ar_len);

      if (mem_aw_valid && mem_aw_ready)
        $fdisplay(stderr, "MC: aw addr=%x id=%x size=%x len=%x"
        ,mem_aw_addr
        ,mem_aw_id
        ,mem_aw_size
        ,mem_aw_len);

      if (mem_w_valid && mem_w_ready)
        $fdisplay(stderr, "MC: w data=%x", mem_w_data);

      if (mem_r_valid && mem_r_ready)
        $fdisplay(stderr, "MC: r data=%x", mem_r_data);

    end
  end

  // exit

  always @(posedge zb_host_clk) begin

    if (h2r_exit > 1) begin
      $display("\n*** FAIL");
      htif_fini(1'b1);
    end

    if (h2r_exit) begin
      $display("\n*** DONE");
      htif_fini(1'b0);
    end

  end

  // zedboard

  wire F_20_P, F_20_N;
  wire F_23_P, F_23_N;
  wire F_17_P, F_17_N;
  wire F_31_P, F_31_N;
  wire F_33_P, F_33_N;
  wire F_30_P, F_30_N;
  wire F_32_P, F_32_N;
  wire F_28_P, F_28_N;
  wire F_25_P, F_25_N;
  wire F_29_P, F_29_N;
  wire F_26_P, F_26_N;
  wire F_21_P, F_21_N;
  wire F_27_P, F_27_N;
  wire F_22_P, F_22_N;
  wire F_CLK0_P, F_CLK0_N;
  wire F_00_P, F_00_N;
  wire F_01_P, F_01_N;
  wire F_16_P, F_16_N;
  wire F_15_P, F_15_N;
  wire F_13_P, F_13_N;
  wire F_11_P, F_11_N;
  wire F_10_P, F_10_N;
  wire F_14_P, F_14_N;
  wire F_09_P, F_09_N;
  wire F_04_P, F_04_N;
  wire F_07_P, F_07_N;
  wire F_08_P, F_08_N;

  bsg_zedboard zb
    (.GCLK(ZB_OSC_CLK)
    ,.BTNC(host_reset) // push-button reset
    ,.FMC_LA20_P(F_20_P) ,.FMC_LA20_N(F_20_N)
    ,.FMC_LA23_P(F_23_P) ,.FMC_LA23_N(F_23_N)
    ,.FMC_LA17_CC_P(F_17_P) ,.FMC_LA17_CC_N(F_17_N)
    ,.FMC_LA31_P(F_31_P) ,.FMC_LA31_N(F_31_N)
    ,.FMC_LA33_P(F_33_P) ,.FMC_LA33_N(F_33_N)
    ,.FMC_LA30_P(F_30_P) ,.FMC_LA30_N(F_30_N)
    ,.FMC_LA32_P(F_32_P) ,.FMC_LA32_N(F_32_N)
    ,.FMC_LA28_P(F_28_P) ,.FMC_LA28_N(F_28_N)
    ,.FMC_LA25_P(F_25_P) ,.FMC_LA25_N(F_25_N)
    ,.FMC_LA29_P(F_29_P) ,.FMC_LA29_N(F_29_N)
    ,.FMC_LA26_P(F_26_P) ,.FMC_LA26_N(F_26_N)
    ,.FMC_LA21_P(F_21_P) ,.FMC_LA21_N(F_21_N)
    ,.FMC_LA27_P(F_27_P) ,.FMC_LA27_N(F_27_N)
    ,.FMC_LA22_P(F_22_P) ,.FMC_LA22_N(F_22_N)
    ,.FMC_CLK0_P(F_CLK0_P) ,.FMC_CLK0_N(F_CLK0_N)
    ,.FMC_LA00_CC_P(F_00_P) ,.FMC_LA00_CC_N(F_00_N)
    ,.FMC_LA01_CC_P(F_01_P) ,.FMC_LA01_CC_N(F_01_N)
    ,.FMC_LA16_P(F_16_P) ,.FMC_LA16_N(F_16_N)
    ,.FMC_LA15_P(F_15_P) ,.FMC_LA15_N(F_15_N)
    ,.FMC_LA13_P(F_13_P) ,.FMC_LA13_N(F_13_N)
    ,.FMC_LA11_P(F_11_P) ,.FMC_LA11_N(F_11_N)
    ,.FMC_LA10_P(F_10_P) ,.FMC_LA10_N(F_10_N)
    ,.FMC_LA14_P(F_14_P) ,.FMC_LA14_N(F_14_N)
    ,.FMC_LA09_P(F_09_P) ,.FMC_LA09_N(F_09_N)
    ,.FMC_LA04_P(F_04_P) ,.FMC_LA04_N(F_04_N)
    ,.FMC_LA07_P(F_07_P) ,.FMC_LA07_N(F_07_N)
    ,.FMC_LA08_P(F_08_P) ,.FMC_LA08_N(F_08_N)
`ifdef SIMULATION
    ,.boot_done_o(boot_done)
    ,.host_clk_o(zb_host_clk)
    // host in
    ,.host_valid_i(h2r_valid)
    ,.host_data_i(h2r_data)
    ,.host_ready_o(h2r_ready)
    // host out
    ,.host_valid_o(r2h_valid)
    ,.host_data_o(r2h_data)
    ,.host_ready_i(r2h_ready)
    // mem aw
    ,.mem_aw_valid_o(mem_aw_valid)
    ,.mem_aw_bits_addr_o(mem_aw_addr)
    ,.mem_aw_bits_len_o(mem_aw_len)
    ,.mem_aw_bits_size_o(mem_aw_size)
    ,.mem_aw_bits_id_o(mem_aw_id)
    ,.mem_aw_ready_i(mem_aw_ready)
    // mem w
    ,.mem_w_valid_o(mem_w_valid)
    ,.mem_w_bits_data_o(mem_w_data)
    ,.mem_w_bits_last_o(mem_w_last)
    ,.mem_w_bits_strb_o(mem_w_strb)
    ,.mem_w_ready_i(mem_w_ready)
    // mem b
    ,.mem_b_valid_i(mem_b_valid)
    ,.mem_b_bits_resp_i(mem_b_resp)
    ,.mem_b_bits_id_i(mem_b_id)
    ,.mem_b_ready_o(mem_b_ready)
    // mem ar
    ,.mem_ar_valid_o(mem_ar_valid)
    ,.mem_ar_bits_addr_o(mem_ar_addr)
    ,.mem_ar_bits_len_o(mem_ar_len)
    ,.mem_ar_bits_size_o(mem_ar_size)
    ,.mem_ar_bits_id_o(mem_ar_id)
    ,.mem_ar_ready_i(mem_ar_ready)
    // mem r
    ,.mem_r_valid_i(mem_r_valid)
    ,.mem_r_bits_resp_i(mem_r_resp)
    ,.mem_r_bits_data_i(mem_r_data)
    ,.mem_r_bits_last_i(mem_r_last)
    ,.mem_r_bits_id_i(mem_r_id)
    ,.mem_r_ready_o(mem_r_ready));
`else
    );
`endif

  // double trouble

  bsg_gateway gw
    (.CLK_OSC_P(GW_OSC_CLK) ,.CLK_OSC_N(~GW_OSC_CLK)
    ,.F20_P(F_20_N) ,.F20_N(F_20_P)
    ,.F23_P(F_23_N) ,.F23_N(F_23_P)
    ,.FCLK0_M2C_P(F_CLK0_P) ,.FCLK0_M2C_N(F_CLK0_N)
    ,.F0_P(F_00_N) ,.F0_N(F_00_P)
    ,.F1_P(F_01_N) ,.F1_N(F_01_P)
    ,.F16_P(F_16_N) ,.F16_N(F_16_P)
    ,.F15_P(F_15_N) ,.F15_N(F_15_P)
    ,.F13_P(F_13_N) ,.F13_N(F_13_P)
    ,.F11_P(F_11_N) ,.F11_N(F_11_P)
    ,.F10_P(F_10_N) ,.F10_N(F_10_P)
    ,.F14_P(F_14_N) ,.F14_N(F_14_P)
    ,.F9_P(F_09_N) ,.F9_N(F_09_P)
    ,.F4_P(F_04_N) ,.F4_N(F_04_P)
    ,.F7_P(F_07_N) ,.F7_N(F_07_P)
    ,.F8_P(F_08_N) ,.F8_N(F_08_P)
    ,.F17_P(F_17_N) ,.F17_N(F_17_P)
    ,.F31_P(F_31_N) ,.F31_N(F_31_P)
    ,.F33_P(F_33_N) ,.F33_N(F_33_P)
    ,.F30_P(F_30_N) ,.F30_N(F_30_P)
    ,.F32_P(F_32_N) ,.F32_N(F_32_P)
    ,.F28_P(F_28_N) ,.F28_N(F_28_P)
    ,.F25_P(F_25_N) ,.F25_N(F_25_P)
    ,.F29_P(F_29_N) ,.F29_N(F_29_P)
    ,.F26_P(F_26_N) ,.F26_N(F_26_P)
    ,.F21_P(F_21_N) ,.F21_N(F_21_P)
    ,.F27_P(F_27_N) ,.F27_N(F_27_P)
    ,.F22_P(F_22_N) ,.F22_N(F_22_P));

endmodule
