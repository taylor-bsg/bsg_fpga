//------------------------------------------------------------
// University of California, San Diego - Bespoke Systems Group
//------------------------------------------------------------
// File: bsg_gateway_clk.v
//
// - PLL_ADV generates the following clocks:
//     * ext_core_clk_o
//     * int_core_clk_o
//     * mb_clk_o
//
// - DCM_CLKGEN generates the following clocks:
//     * ext_io_master_clk_o
//     * int_io_master_clk_o
//
// - Both PLL_ADV and DCM_CLKGEN uses the 150MHz clock coming
//   from the oscillator in doubletrouble.
//
// - Check pll_*_lp and dcm_*_lp local parameters for tweaking
//   clocks
//
// Author: Luis Vega - lvgutierrez@eng.ucsd.edu
//------------------------------------------------------------

module bsg_gateway_clk
  (input clk_150_mhz_p_i, clk_150_mhz_n_i
  // microblaze clock
  ,output mb_clk_o
  // internal clocks
  ,output int_core_clk_o
  ,output int_io_master_clk_o
  // external clocks
  ,output ext_core_clk_o
  ,output ext_io_master_clk_o
  // locked
  ,output locked_o);

  wire ibufgds_clk_150_mhz;

  IBUFGDS #
    (.DIFF_TERM("TRUE"))
  ibufgds
    (.I(clk_150_mhz_p_i) ,.IB(clk_150_mhz_n_i)
    ,.O(ibufgds_clk_150_mhz));

  // 150Mhz * pll_mult_lp
  // if pll_mult_lp=7 then pll-internal-clock is 1050MHz
  localparam pll_mult_lp = 7;

  // 150MHz * (pll_mult_lp/pll_core_clk_divide_lp)
  // if pll_mult_lp=7 and pll_core_clk_divide_lp=21,
  // then (int/ext) core_clk_o are 50MHz
  localparam pll_core_clk_divide_lp = 21;

  // 150Mhz*(pll_mult_lp/pll_mb_clk_divide_lp)
  // if pll_mult_lp=7 and pll_mb_clk_divide_lp=21,
  // then mb_clk_o is 50MHz
  localparam pll_mb_clk_divide_lp = 21;

  wire pll_fb;
  wire pll_ext_core_clk_0_deg;
  wire pll_ext_core_clk_180_deg;
  wire pll_int_core_clk;
  wire pll_mb_clk;
  wire pll_locked;

  PLL_ADV #
    (.BANDWIDTH("OPTIMIZED")
    ,.CLKFBOUT_MULT(pll_mult_lp)
    ,.CLKFBOUT_PHASE(0.0)
    ,.CLKIN1_PERIOD(6.667)
    ,.CLKIN2_PERIOD(6.667)
    // ext core clk
    ,.CLKOUT0_DIVIDE(pll_core_clk_divide_lp)
    ,.CLKOUT0_DUTY_CYCLE(0.5)
    ,.CLKOUT0_PHASE(0.0)
    ,.CLKOUT1_DIVIDE(pll_core_clk_divide_lp)
    ,.CLKOUT1_DUTY_CYCLE(0.5)
    ,.CLKOUT1_PHASE(180.0)
    // int core clk
    ,.CLKOUT2_DIVIDE(pll_core_clk_divide_lp)
    ,.CLKOUT2_DUTY_CYCLE(0.5)
    ,.CLKOUT2_PHASE(0.0)
    // mb clk
    ,.CLKOUT3_DIVIDE(pll_mb_clk_divide_lp)
    ,.CLKOUT3_DUTY_CYCLE(0.5)
    ,.CLKOUT3_PHASE(0.0)
    // not used
    ,.CLKOUT4_DIVIDE(1)
    ,.CLKOUT4_DUTY_CYCLE(0.5)
    ,.CLKOUT4_PHASE(0.0)
    ,.CLKOUT5_DIVIDE(8)
    ,.CLKOUT5_DUTY_CYCLE(0.5)
    ,.CLKOUT5_PHASE(0.0)
    ,.COMPENSATION("INTERNAL")
    ,.DIVCLK_DIVIDE(1)
    ,.REF_JITTER(0.100)
    ,.SIM_DEVICE("SPARTAN6"))
  pll
    (.CLKFBDCM()
    ,.CLKFBOUT(pll_fb)
    // ext core clk
    ,.CLKOUT0(pll_ext_core_clk_0_deg)
    ,.CLKOUT1(pll_ext_core_clk_180_deg)
    // int core clk
    ,.CLKOUT2(pll_int_core_clk)
    // mb clk
    ,.CLKOUT3(pll_mb_clk)
    ,.CLKOUT4()
    ,.CLKOUT5()
    ,.CLKOUTDCM0()
    ,.CLKOUTDCM1()
    ,.CLKOUTDCM2()
    ,.CLKOUTDCM3()
    ,.CLKOUTDCM4()
    ,.CLKOUTDCM5()
    ,.DO()
    ,.DRDY()
    ,.LOCKED(pll_locked)
    ,.CLKFBIN(pll_fb)
    ,.CLKIN1(ibufgds_clk_150_mhz)
    ,.CLKIN2(1'b0)
    ,.CLKINSEL(1'b1)
    ,.DADDR(5'b00000)
    ,.DCLK(1'b0)
    ,.DEN(1'b0)
    ,.DI(16'h0000)
    ,.DWE(1'b0)
    ,.RST(1'b0)
    ,.REL(1'b0));

  // ext core clock

  wire bufg_ext_core_clk_0_deg;

  BUFG bufg_0
    (.I(pll_ext_core_clk_0_deg)
    ,.O(bufg_ext_core_clk_0_deg));

  wire bufg_ext_core_clk_180_deg;

  BUFG bufg_180
    (.I(pll_ext_core_clk_180_deg)
    ,.O(bufg_ext_core_clk_180_deg));

  ODDR2 core_oddr
    (.D0(1'b1)
    ,.D1(1'b0)
    ,.C0(bufg_ext_core_clk_0_deg)
    ,.C1(bufg_ext_core_clk_180_deg)
    ,.CE(1'b1)
    ,.S(1'b0)
    ,.R(1'b0)
    ,.Q(ext_core_clk_o));

  // int core clock

  BUFG bufg_core
    (.I(pll_int_core_clk)
    ,.O(int_core_clk_o));

  // mb clock

  BUFG bufg_mb
    (.I(pll_mb_clk)
    ,.O(mb_clk_o));

  // io master clk

  // 150Mhz * dcm_mult_lp
  // if dcm_mult_lp=4 then dcm-internal-clock 600MHz
  localparam dcm_mult_lp = 4;

  // 150MHz * (dcm_mult_lp/dcm_io_master_clk_divide_lp)
  // if dcm_mult_lp=7 and dcm_io_master_clk_divide_lp=9,
  // then (int/ext) io_master_clk_o are 66.67MHz
  // if dcm_io_master_clk_divide_lp = 3 (int/ext) io_master_clk are 200 mhz
  // if dcm_io_master_clk_divide_lp = 4 (int/ext) io_master_clk are 150 mhz
  // if dcm_io_master_clk_divide_lp = 6 (int/ext) io_master_clk are 100 mhz
  // if dcm_io_master_clk_divide_lp = 8 (int/ext) io_master_clk are 75 mhz
  localparam dcm_io_master_clk_divide_lp = 6;

  wire dcm_io_master_clk_0_deg;
  wire dcm_io_master_clk_180_deg;
  wire dcm_locked;

  DCM_CLKGEN #
    (.CLKFX_MULTIPLY(dcm_mult_lp)
    ,.CLKFX_DIVIDE(dcm_io_master_clk_divide_lp)
    ,.SPREAD_SPECTRUM("NONE")
    ,.STARTUP_WAIT("FALSE")
    ,.CLKIN_PERIOD(6.667))
  dcm
    (.CLKIN(ibufgds_clk_150_mhz)
    ,.RST(1'b0)
    ,.FREEZEDCM(1'b0)
    // output
    ,.CLKFX(dcm_io_master_clk_0_deg)
    ,.CLKFX180(dcm_io_master_clk_180_deg)
    ,.LOCKED(dcm_locked)
    ,.CLKFXDV()
    ,.PROGDONE()
    ,.STATUS()
    // inputs
    ,.PROGDATA()
    ,.PROGEN()
    ,.PROGCLK());

  // int io master clock

  assign int_io_master_clk_o = dcm_io_master_clk_0_deg;

  // ext io master clock

  ODDR2 io_oddr
    (.D0(1'b1)
    ,.D1(1'b0)
    ,.C0(dcm_io_master_clk_0_deg)
    ,.C1(dcm_io_master_clk_180_deg)
    ,.CE(1'b1)
    ,.S(1'b0)
    ,.R(1'b0)
    ,.Q(ext_io_master_clk_o));

  assign locked_o = pll_locked & dcm_locked;

endmodule
