// bsg_gateway has FMC-support for these two boards:
//   * Xilinx ML605 (bsg_ml605)
//   * Digilent Zedboard (bsg_zedboard)
//
// BSG_ML605_FMC macro sets pinout for ML605
// BSG_ZEDBOARD_FMC macro sets pinout for Zedboard

module bsg_gateway
# (parameter num_channels_p=4
  ,parameter channel_width_p=8
  ,parameter ring_bytes_p=10
  ,parameter ring_width_p=ring_bytes_p*channel_width_p
  ,parameter master_p=1
  ,parameter master_to_client_speedup_p=100
  ,parameter master_bypass_test_p=5'b11111)
  (input CLK_OSC_P, CLK_OSC_N

  // LED
  ,output FPGA_LED2
  ,output FPGA_LED3

  // fmc reset in
  ,input F20_P, F20_N
  // fmc host reset out
  ,output F23_P, F23_N
  // fmc tx clk in
  ,input FCLK0_M2C_P, FCLK0_M2C_N

`ifdef BSG_ML605_FMC
  // fmc tx clk out
  ,output FCLK1_M2C_P, FCLK1_M2C_N
  // fmc tx data out [0]
  ,output F0_P, F0_N
`else
`ifdef BSG_ZEDBOARD_FMC
  // fmc tx clk out
  ,output F0_P, F0_N
  // fmc tx data out [0]
  ,output F1_P, F1_N
`endif
`endif

  // fmc tx data out [9:1]
  ,output F16_P, F16_N
  ,output F15_P, F15_N
  ,output F13_P, F13_N
  ,output F11_P, F11_N
  ,output F10_P, F10_N
  ,output F14_P, F14_N
  ,output F9_P, F9_N
  ,output F4_P, F4_N
  ,output F7_P, F7_N
  ,output F8_P, F8_N
  // fmc rx clk in
  ,input F17_P, F17_N
  // fmc rx data in
  ,input F31_P, F31_N
  ,input F33_P, F33_N
  ,input F30_P, F30_N
  ,input F32_P, F32_N
  ,input F28_P, F28_N
  ,input F25_P, F25_N
  ,input F29_P, F29_N
  ,input F26_P, F26_N
  ,input F21_P, F21_N
  ,input F27_P, F27_N
  ,input F22_P, F22_N

  ,output PLL_CLK_I
  ,output MSTR_SDO_CLK
  // asic reset
  ,output AID10
  // channel clk in
  ,input AOC0, BOC0, COC0, DOC0
  // channel valid in
  ,input AOD8, BOD8, COD8, DOD8
  // channel data in
  //      A     B     C     D
  ,input AOD0, BOD0, COD0, DOD0
  ,input AOD1, BOD1, COD1, DOD1
  ,input AOD2, BOD2, COD2, DOD2
  ,input AOD3, BOD3, COD3, DOD3
  ,input AOD4, BOD4, COD4, DOD4
  ,input AOD5, BOD5, COD5, DOD5
  ,input AOD6, BOD6, COD6, DOD6
  ,input AOD7, BOD7, COD7, DOD7
  // channel token out
  ,output AOT0, BOT0, COT0, DOT0
  // channel clk out
  ,output AIC0, BIC0, CIC0, DIC0
  // channel valid out
  ,output AID8, BID8, CID8, DID8
  // channel data out
  //       A     B     C     D
  ,output AID0, BID0, CID0, DID0
  ,output AID1, BID1, CID1, DID1
  ,output AID2, BID2, CID2, DID2
  ,output AID3, BID3, CID3, DID3
  ,output AID4, BID4, CID4, DID4
  ,output AID5, BID5, CID5, DID5
  ,output AID6, BID6, CID6, DID6
  ,output AID7, BID7, CID7, DID7
  // channel token in
  ,input AIT0, BIT0, CIT0, DIT0
`ifdef SIMULATION
  );
`else
  // ports used by microblaze
  // reset
  ,input PWR_RSTN
  // voltage-rail enable
  ,output logic ASIC_CORE_EN, ASIC_IO_EN
  // current monitor
  ,output logic CUR_MON_ADDR0, CUR_MON_ADDR1
  ,inout CUR_MON_SCL, CUR_MON_SDA
  // potentiometer
  ,output logic DIG_POT_ADDR0, DIG_POT_ADDR1
  ,output logic DIG_POT_INDEP, DIG_POT_NRST
  ,inout DIG_POT_SCL, DIG_POT_SDA
  // uart
  ,input UART_RX
  ,output UART_TX
  // led
  ,output logic FPGA_LED0, FPGA_LED1);
`endif

  // clock generation

  wire mb_clk;
  wire core_clk, io_master_clk;
  wire locked;

  bsg_gateway_clk clk
    (.clk_150_mhz_p_i(CLK_OSC_P) ,.clk_150_mhz_n_i(CLK_OSC_N)
    // microblaze clock
    ,.mb_clk_o(mb_clk)
    // internal clocks
    ,.int_core_clk_o(core_clk)
    ,.int_io_master_clk_o(io_master_clk)
    // external clocks
    ,.ext_core_clk_o(MSTR_SDO_CLK)
    ,.ext_io_master_clk_o(PLL_CLK_I)
    ,.locked_o(locked));

  // fmc

  wire fmc_reset, core_calib_done_r;

  wire                    core_valid;
  wire [ring_width_p-1:0] core_data;
  wire                    core_ready;

  wire                    fmc_valid;
  wire [ring_width_p-1:0] fmc_data;
  wire                    fmc_ready;

  bsg_gateway_fmc fmc
    (.clk_i(core_clk)
    // fmc reset out
    ,.fmc_reset_o(fmc_reset)
    // host reset in
    ,.host_reset_i(~core_calib_done_r)
    // data in
    ,.valid_i(core_valid)
    ,.data_i(core_data)
    ,.ready_o(core_ready)
    // data out
    ,.valid_o(fmc_valid)
    ,.data_o(fmc_data)
    ,.ready_i(fmc_ready)
    // fmc reset in
    ,.F20_P(F20_P) ,.F20_N(F20_N)
    // fmc host reset out
    ,.F23_P(F23_P) ,.F23_N(F23_N)
    // fmc tx clk in
    ,.FCLK0_M2C_P(FCLK0_M2C_P) ,.FCLK0_M2C_N(FCLK0_M2C_N)
    // fmc tx clk out
    ,.F0_P(F0_P) ,.F0_N(F0_N)
    // fmc tx data out [0]
    ,.F1_P(F1_P) ,.F1_N(F1_N)
    // fmc tx data out [9:1]
    ,.F16_P(F16_P) ,.F16_N(F16_N)
    ,.F15_P(F15_P) ,.F15_N(F15_N)
    ,.F13_P(F13_P) ,.F13_N(F13_N)
    ,.F11_P(F11_P) ,.F11_N(F11_N)
    ,.F10_P(F10_P) ,.F10_N(F10_N)
    ,.F14_P(F14_P) ,.F14_N(F14_N)
    ,.F9_P(F9_P) ,.F9_N(F9_N)
    ,.F4_P(F4_P) ,.F4_N(F4_N)
    ,.F7_P(F7_P) ,.F7_N(F7_N)
    ,.F8_P(F8_P) ,.F8_N(F8_N)
    // fmc rx clk in
    ,.F17_P(F17_P) ,.F17_N(F17_N)
    // fmc rx data in
    ,.F31_P(F31_P) ,.F31_N(F31_N)
    ,.F33_P(F33_P) ,.F33_N(F33_N)
    ,.F30_P(F30_P) ,.F30_N(F30_N)
    ,.F32_P(F32_P) ,.F32_N(F32_N)
    ,.F28_P(F28_P) ,.F28_N(F28_N)
    ,.F25_P(F25_P) ,.F25_N(F25_N)
    ,.F29_P(F29_P) ,.F29_N(F29_N)
    ,.F26_P(F26_P) ,.F26_N(F26_N)
    ,.F21_P(F21_P) ,.F21_N(F21_N)
    ,.F27_P(F27_P) ,.F27_N(F27_N)
    ,.F22_P(F22_P) ,.F22_N(F22_N));

  // comm_link

  wire  asic_reset;

  assign AID10 = asic_reset;

  // io

  wire  [num_channels_p-1:0] io_clk;
  wire  [num_channels_p-1:0] io_valid;
  wire [channel_width_p-1:0] io_data [num_channels_p-1:0];
  wire  [num_channels_p-1:0] io_token;

  wire  [num_channels_p-1:0] im_clk;
  wire  [num_channels_p-1:0] im_valid;
  wire [channel_width_p-1:0] im_data [num_channels_p-1:0];
  wire  [num_channels_p-1:0] token_clk;

  // channel in

  assign io_clk = {DOC0, COC0, BOC0, AOC0};

  assign io_valid = {DOD8, COD8, BOD8, AOD8};

  assign io_data = {{DOD7, DOD6, DOD5, DOD4, DOD3, DOD2, DOD1, DOD0}
                   ,{COD7, COD6, COD5, COD4, COD3, COD2, COD1, COD0}
                   ,{BOD7, BOD6, BOD5, BOD4, BOD3, BOD2, BOD1, BOD0}
                   ,{AOD7, AOD6, AOD5, AOD4, AOD3, AOD2, AOD1, AOD0}};

  assign {DOT0, COT0, BOT0, AOT0} = io_token;

  // channel out

  assign {DIC0, CIC0, BIC0, AIC0} = im_clk;

  assign {DID8, CID8, BID8, AID8} = im_valid;

  assign {DID7, DID6, DID5, DID4, DID3, DID2, DID1, DID0} = im_data[3];
  assign {CID7, CID6, CID5, CID4, CID3, CID2, CID1, CID0} = im_data[2];
  assign {BID7, BID6, BID5, BID4, BID3, BID2, BID1, BID0} = im_data[1];
  assign {AID7, AID6, AID5, AID4, AID3, AID2, AID1, AID0} = im_data[0];

  assign token_clk = {DIT0, CIT0, BIT0, AIT0};

  bsg_comm_link #
    (.channel_width_p(channel_width_p)
    ,.core_channels_p(ring_bytes_p)
    ,.link_channels_p(num_channels_p)
    ,.master_p(master_p)
    ,.master_to_slave_speedup_p(master_to_client_speedup_p)
    ,.master_bypass_test_p(master_bypass_test_p))
  comm_link
    (.core_clk_i(core_clk)
    ,.io_master_clk_i(io_master_clk)

    // reset
    ,.async_reset_i(fmc_reset)

    // core ctrl
    ,.core_calib_done_r_o(core_calib_done_r)

    // core in
    ,.core_valid_i(fmc_valid)
    ,.core_data_i(fmc_data)
    ,.core_ready_o(fmc_ready)

    // core out
    ,.core_valid_o(core_valid)
    ,.core_data_o(core_data)
    ,.core_yumi_i(core_valid & core_ready)

    // in from i/o
    ,.io_clk_tline_i(io_clk)
    ,.io_valid_tline_i(io_valid)
    ,.io_data_tline_i(io_data)
    ,.io_token_clk_tline_o(io_token)

    // out to i/o
    ,.im_clk_tline_o(im_clk)
    ,.im_valid_tline_o(im_valid)
    ,.im_data_tline_o(im_data)
    ,.token_clk_tline_i(token_clk)

    ,.im_slave_reset_tline_r_o(asic_reset));

  // led

  assign FPGA_LED2 = fmc_reset;
  assign FPGA_LED3 = core_calib_done_r;

`ifndef SIMULATION

  // microblaze

  wire [11:0] gpio;
  wire cpu_override_output_p;
  wire cpu_override_output_n;

  assign cpu_override_output_p = gpio[11];
  assign cpu_override_output_n = gpio[10];

  always_comb begin
    FPGA_LED0 = 1'b1;
    FPGA_LED1 = 1'b1;
    DIG_POT_INDEP = 1'b1;
    DIG_POT_NRST = 1'b1;
    DIG_POT_ADDR0 = 1'b1;
    DIG_POT_ADDR1 = 1'b1;
    CUR_MON_ADDR0 = 1'b1;
    CUR_MON_ADDR1 = 1'b1;
    ASIC_IO_EN = 1'b1;
    ASIC_CORE_EN = 1'b1;
    if (cpu_override_output_p == 1'b1 && cpu_override_output_n == 1'b0 && PWR_RSTN == 1'b1) begin
      FPGA_LED0 = gpio[0];
      FPGA_LED1 = gpio[1];
      DIG_POT_INDEP = gpio[2];
      DIG_POT_NRST = gpio[3];
      DIG_POT_ADDR0 = gpio[4];
      DIG_POT_ADDR1 = gpio[5];
      CUR_MON_ADDR0 = gpio[6];
      CUR_MON_ADDR1 = gpio[7];
      ASIC_IO_EN = gpio[8];
      ASIC_CORE_EN = gpio[9];
    end
  end

  (* BOX_TYPE = "user_black_box" *)
  board_ctrl board_ctrl_i
    (.RESET(PWR_RSTN)
    ,.CLK_50(mb_clk)
    ,.CLK_LOCKED(locked)
    ,.axi_iic_dig_pot_Gpo_pin()
    ,.axi_iic_dig_pot_Sda_pin(DIG_POT_SDA)
    ,.axi_iic_dig_pot_Scl_pin(DIG_POT_SCL)
    ,.axi_iic_cur_mon_Gpo_pin()
    ,.axi_iic_cur_mon_Sda_pin(CUR_MON_SDA)
    ,.axi_iic_cur_mon_Scl_pin(CUR_MON_SCL)
    ,.axi_gpio_0_GPIO_IO_O_pin(gpio)
    ,.axi_gpio_0_GPIO2_IO_I_pin()
    ,.axi_uartlite_0_RX_pin(UART_RX)
    ,.axi_uartlite_0_TX_pin(UART_TX));

`endif

endmodule
