#------------------------------------------------------------
# University of California, San Diego - Bespoke Systems Group
#------------------------------------------------------------
# File: vivado_prog_debug.tcl
#
# Authors: Luis Vega - lvgutierrez@eng.ucsd.edu
#------------------------------------------------------------

# these two variables must be exported in the Makefile
set bsg_zedboard_bit $::env(BSG_ZEDBOARD_BIT)
set bsg_zedboard_ltx $::env(BSG_ZEDBOARD_LTX)

start_gui

open_hw

connect_hw_server -url localhost:3121

current_hw_target [get_hw_targets */xilinx_tcf/Digilent/210248691134]
set_property PARAM.FREQUENCY 3000000 [get_hw_targets */xilinx_tcf/Digilent/210248691134]
open_hw_target

current_hw_device [lindex [get_hw_devices] 1]
refresh_hw_device -update_hw_probes false [lindex [get_hw_devices] 1]

set_property PROBES.FILE $bsg_zedboard_ltx [lindex [get_hw_devices] 1]
set_property PROGRAM.FILE $bsg_zedboard_bit [lindex [get_hw_devices] 1]
program_hw_devices [lindex [get_hw_devices] 1]
refresh_hw_device [lindex [get_hw_devices] 1]

run_hw_ila [get_hw_ilas -of_objects [get_hw_devices xc7z020_1] -filter {CELL_NAME=~"ila_0"}] -trigger_now
wait_on_hw_ila [get_hw_ilas -of_objects [get_hw_devices xc7z020_1] -filter {CELL_NAME=~"ila_0"}]
display_hw_ila_data [upload_hw_ila_data [get_hw_ilas -of_objects [get_hw_devices xc7z020_1] -filter {CELL_NAME=~"ila_0"}]]
