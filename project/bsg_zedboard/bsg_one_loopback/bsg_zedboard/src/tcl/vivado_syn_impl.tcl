# these two variables must be exported in the Makefile
#------------------------------------------------------------
# University of California, San Diego - Bespoke Systems Group
#------------------------------------------------------------
# File: vivado_syn_impl.tcl
#
# Authors: Luis Vega - lvgutierrez@eng.ucsd.edu
#------------------------------------------------------------

# these four variables must be exported in the Makefile
set bsg_fpga_ip_dir $::env(BSG_FPGA_IP_DIR)
set bsg_ip_cores_dir $::env(BSG_IP_CORES_DIR)
set bsg_designs_dir $::env(BSG_DESIGNS_DIR)
set bsg_work_dir $::env(BSG_WORK_DIR)
set bsg_top_name $::env(BSG_TOP_NAME)

set output_dir $bsg_work_dir/out
file mkdir $output_dir

# read source files
read_verilog -sv $bsg_ip_cores_dir/bsg_misc/bsg_defines.v
read_verilog -sv $bsg_ip_cores_dir/bsg_fsb/bsg_fsb_pkg.v
read_verilog -sv $bsg_ip_cores_dir/bsg_fsb/bsg_front_side_bus_hop_in.v
read_verilog -sv $bsg_ip_cores_dir/bsg_fsb/bsg_front_side_bus_hop_out.v
read_verilog -sv $bsg_ip_cores_dir/bsg_fsb/bsg_fsb_murn_gateway.v
read_verilog -sv $bsg_ip_cores_dir/bsg_fsb/bsg_fsb.v
read_verilog -sv $bsg_ip_cores_dir/bsg_dataflow/bsg_sbox.v
read_verilog -sv $bsg_ip_cores_dir/bsg_dataflow/bsg_two_fifo.v
read_verilog -sv $bsg_ip_cores_dir/bsg_async/bsg_async_credit_counter.v
read_verilog -sv $bsg_ip_cores_dir/bsg_async/bsg_async_fifo.v
read_verilog -sv $bsg_ip_cores_dir/bsg_async/bsg_async_ptr_gray.v
read_verilog -sv $bsg_ip_cores_dir/bsg_async/bsg_sync_sync.v
read_verilog -sv $bsg_ip_cores_dir/bsg_async/bsg_launch_sync_sync.v
read_verilog -sv $bsg_ip_cores_dir/bsg_test/test_bsg_data_gen.v
read_verilog -sv $bsg_ip_cores_dir/bsg_mem/bsg_mem_1r1w.v

read_verilog -sv $bsg_fpga_ip_dir/bsg_zedboard/bsg_zedboard_fmc/v/bsg_zedboard_fmc.v
read_verilog -sv $bsg_fpga_ip_dir/bsg_zedboard/bsg_zedboard_fmc/v/bsg_zedboard_fmc_buffer.v
read_verilog -sv $bsg_fpga_ip_dir/bsg_zedboard/bsg_zedboard_fmc/v/bsg_zedboard_fmc_tx.v
read_verilog -sv $bsg_fpga_ip_dir/bsg_zedboard/bsg_zedboard_fmc/v/bsg_zedboard_fmc_tx_clk.v
read_verilog -sv $bsg_fpga_ip_dir/bsg_zedboard/bsg_zedboard_fmc/v/bsg_zedboard_fmc_tx_data.v
read_verilog -sv $bsg_fpga_ip_dir/bsg_zedboard/bsg_zedboard_fmc/v/bsg_zedboard_fmc_rx.v
read_verilog -sv $bsg_fpga_ip_dir/bsg_zedboard/bsg_zedboard_fmc/v/bsg_zedboard_fmc_rx_clk.v
read_verilog -sv $bsg_fpga_ip_dir/bsg_zedboard/bsg_zedboard_fmc/v/bsg_zedboard_fmc_rx_data.v
read_verilog -sv $bsg_fpga_ip_dir/bsg_zedboard/bsg_zedboard_fmc/v/bsg_zedboard_fmc_rx_data_bitslip_ctrl.v

read_verilog -sv $bsg_work_dir/src/v/${bsg_top_name}.v
read_verilog -sv $bsg_work_dir/src/v/bsg_zedboard_mmcm.v

read_verilog -sv $bsg_designs_dir/modules/bsg_guts/bsg_test_node.v

read_xdc $bsg_work_dir/src/xdc/${bsg_top_name}.xdc
read_xdc $bsg_fpga_ip_dir/bsg_zedboard/bsg_zedboard_fmc/xdc/bsg_zedboard_fmc.xdc

# synth
synth_design -top $bsg_top_name \
             -part xc7z020clg484-1 \
             -include_dirs $bsg_ip_cores_dir/bsg_misc

write_checkpoint -force $output_dir/post_synth_checkpoint.dcp
report_utilization -file $output_dir/post_synth_utilization.rpt
report_timing_summary -file $output_dir/post_synth_timing_summary.rpt

# ila
create_debug_core ila_0 ila
set_property C_DATA_DEPTH 1024 [get_debug_cores ila_0]
set_property C_TRIGIN_EN false [get_debug_cores ila_0]
set_property C_TRIGOUT_EN false [get_debug_cores ila_0]
set_property C_ADV_TRIGGER false [get_debug_cores ila_0]
set_property C_INPUT_PIPE_STAGES 0 [get_debug_cores ila_0]
set_property C_EN_STRG_QUAL false [get_debug_cores ila_0]
set_property ALL_PROBE_SAME_MU true [get_debug_cores ila_0]
set_property ALL_PROBE_SAME_MU_CNT 1 [get_debug_cores ila_0]
set_property port_width 1 [get_debug_ports ila_0/clk]
connect_debug_port ila_0/clk [get_nets [list clk_50_mhz_lo]]
set_property port_width 146 [get_debug_ports ila_0/probe0]
connect_debug_port ila_0/probe0 [lsort -dictionary [get_nets -hier -filter {MARK_DEBUG==1}]]

write_debug_probes -force $output_dir/${bsg_top_name}.ltx

# logic optimization
opt_design

# placement
place_design
write_checkpoint -force $output_dir/post_place_checkpoint.dcp
report_clock_utilization -file $output_dir/clock_utilization.rpt
report_utilization -file $output_dir/post_place_utilization.rpt
report_timing_summary -file $output_dir/post_place_timing_summary.rpt

# route
route_design
write_checkpoint -force $output_dir/post_route_checkpoint.dcp
report_route_status -file $output_dir/post_route_status.rpt
report_timing_summary -file $output_dir/post_route_timing_summary.rpt

# power
report_power -file $output_dir/post_route_power.rpt

# drc
report_drc -file $output_dir/post_imp_drc.rpt

# netlist
write_verilog -force $output_dir/${bsg_top_name}_mapped.v \
              -mode timesim \
              -sdf_anno true

# generate a bitstream
write_bitstream -force $output_dir/${bsg_top_name}.bit
