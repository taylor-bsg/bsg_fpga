//------------------------------------------------------------
// University of California, San Diego - Bespoke Systems Group
//------------------------------------------------------------
// File: test_bsg.v
//
// Authors: Luis Vega - lvgutierrez@eng.ucsd.edu
//------------------------------------------------------------

module test_bsg;

  logic GCLK;

  initial GCLK = 1'b0;

  always #(5000) GCLK = ~GCLK;

  logic BTNC;

  initial begin
    BTNC = 1'b0;
    #100_000;
    BTNC = 1'b1;
    $display("TIME:%08d ZEDBOARD RESET HIGH", $time);
    #150_000;
    BTNC = 1'b0;
    $display("TIME:%08d ZEDBOARD RESET LOW", $time);
  end

  initial begin
    #2_650_000_000;
    $finish;
  end

  // ZEDBOARD

  logic LD0;
  logic LD1;
  logic LD2;
  logic LD3;
  logic LD4;
  logic LD5;
  logic LD6;
  logic LD7;

  logic F_20_P, F_20_N;
  logic F_23_P, F_23_N;
  logic F_17_P, F_17_N;

  logic F_31_P, F_31_N;
  logic F_33_P, F_33_N;
  logic F_30_P, F_30_N;
  logic F_32_P, F_32_N;
  logic F_28_P, F_28_N;
  logic F_25_P, F_25_N;
  logic F_29_P, F_29_N;
  logic F_26_P, F_26_N;
  logic F_21_P, F_21_N;
  logic F_27_P, F_27_N;
  logic F_22_P, F_22_N;

  logic F_CLK0_P, F_CLK0_N;
  logic F_00_P, F_00_N;

  logic F_01_P, F_01_N;
  logic F_16_P, F_16_N;
  logic F_15_P, F_15_N;
  logic F_13_P, F_13_N;
  logic F_11_P, F_11_N;
  logic F_10_P, F_10_N;
  logic F_14_P, F_14_N;
  logic F_09_P, F_09_N;
  logic F_04_P, F_04_N;
  logic F_07_P, F_07_N;
  logic F_08_P, F_08_N;

  bsg_zedboard zedboard
    (.GCLK(GCLK)
    ,.BTNC(BTNC)
    // led
    ,.LD0(LD0)
    ,.LD1(LD1)
    ,.LD2(LD2)
    ,.LD3(LD3)
    ,.LD4(LD4)
    ,.LD5(LD5)
    ,.LD6(LD6)
    ,.LD7(LD7)
    // fmc gateway reset out
    ,.FMC_LA20_P(F_20_P) ,.FMC_LA20_N(F_20_N)
    // fmc zedboard reset in
    ,.FMC_LA23_P(F_23_P) ,.FMC_LA23_N(F_23_N)
    // fmc tx clk out
    ,.FMC_LA17_CC_P(F_17_P) ,.FMC_LA17_CC_N(F_17_N)
    // fmc tx data out
    ,.FMC_LA31_P(F_31_P) ,.FMC_LA31_N(F_31_N)
    ,.FMC_LA33_P(F_33_P) ,.FMC_LA33_N(F_33_N)
    ,.FMC_LA30_P(F_30_P) ,.FMC_LA30_N(F_30_N)
    ,.FMC_LA32_P(F_32_P) ,.FMC_LA32_N(F_32_N)
    ,.FMC_LA28_P(F_28_P) ,.FMC_LA28_N(F_28_N)
    ,.FMC_LA25_P(F_25_P) ,.FMC_LA25_N(F_25_N)
    ,.FMC_LA29_P(F_29_P) ,.FMC_LA29_N(F_29_N)
    ,.FMC_LA26_P(F_26_P) ,.FMC_LA26_N(F_26_N)
    ,.FMC_LA21_P(F_21_P) ,.FMC_LA21_N(F_21_N)
    ,.FMC_LA27_P(F_27_P) ,.FMC_LA27_N(F_27_N)
    ,.FMC_LA22_P(F_22_P) ,.FMC_LA22_N(F_22_N)
    // fmc rx clk out
    ,.FMC_CLK0_P(F_CLK0_P) ,.FMC_CLK0_N(F_CLK0_N)
    // fmc rx clk in
    ,.FMC_LA00_CC_P(F_00_P) ,.FMC_LA00_CC_N(F_00_N)
    // fmc rx data in
    ,.FMC_LA01_CC_P(F_01_P) ,.FMC_LA01_CC_N(F_01_N)
    ,.FMC_LA16_P(F_16_P) ,.FMC_LA16_N(F_16_N)
    ,.FMC_LA15_P(F_15_P) ,.FMC_LA15_N(F_15_N)
    ,.FMC_LA13_P(F_13_P) ,.FMC_LA13_N(F_13_N)
    ,.FMC_LA11_P(F_11_P) ,.FMC_LA11_N(F_11_N)
    ,.FMC_LA10_P(F_10_P) ,.FMC_LA10_N(F_10_N)
    ,.FMC_LA14_P(F_14_P) ,.FMC_LA14_N(F_14_N)
    ,.FMC_LA09_P(F_09_P) ,.FMC_LA09_N(F_09_N)
    ,.FMC_LA04_P(F_04_P) ,.FMC_LA04_N(F_04_N)
    ,.FMC_LA07_P(F_07_P) ,.FMC_LA07_N(F_07_N)
    ,.FMC_LA08_P(F_08_P) ,.FMC_LA08_N(F_08_N));

  logic [7:0] LED;

  assign LED = {LD7
              ,LD6
              ,LD5
              ,LD4
              ,LD3
              ,LD2
              ,LD1
              ,LD0};

  initial $monitor("ZEDBOARD LED = %08x", LED);

  // GATEWAY

  logic CLK_OSC_P, CLK_OSC_N;

  initial CLK_OSC_P = 1'b0;

  always #(3_335) CLK_OSC_P = ~CLK_OSC_P;

  assign CLK_OSC_N = ~CLK_OSC_P;

  logic FPGA_LED2, FPGA_LED3;

  // asic clk in
  logic CLK0;
  logic MSTR_SDO_CLK;

  // asic reset in
  logic AID10;

  // asic channel clk out
  logic AOC0, BOC0, COC0, DOC0;
  // asic channel valid out
  logic AOD8, BOD8, COD8, DOD8;
  // asic channel data out
  //      A     B     C     D
  logic AOD0, BOD0, COD0, DOD0;
  logic AOD1, BOD1, COD1, DOD1;
  logic AOD2, BOD2, COD2, DOD2;
  logic AOD3, BOD3, COD3, DOD3;
  logic AOD4, BOD4, COD4, DOD4;
  logic AOD5, BOD5, COD5, DOD5;
  logic AOD6, BOD6, COD6, DOD6;
  logic AOD7, BOD7, COD7, DOD7;
  // asic channel token in
  logic AOT0, BOT0, COT0, DOT0;

  // asic channel clk in
  logic AIC0, BIC0, CIC0, DIC0;
  // asic channel valid in
  logic AID8, BID8, CID8, DID8;
  // asic channel data in
  //     A     B     C     D
  logic AID0, BID0, CID0, DID0;
  logic AID1, BID1, CID1, DID1;
  logic AID2, BID2, CID2, DID2;
  logic AID3, BID3, CID3, DID3;
  logic AID4, BID4, CID4, DID4;
  logic AID5, BID5, CID5, DID5;
  logic AID6, BID6, CID6, DID6;
  logic AID7, BID7, CID7, DID7;
  // asic channel token out
  logic AIT0, BIT0, CIT0, DIT0;

  bsg_gateway gateway
    (.CLK_OSC_P(CLK_OSC_P) ,.CLK_OSC_N(CLK_OSC_N)
    ,.FPGA_LED2(FPGA_LED2) ,.FPGA_LED3(FPGA_LED3)

    // --------------------- FMC ------------------------

    // fmc gateway reset in
    ,.F20_P(F_20_N) ,.F20_N(F_20_P)
    // fmc zedboard reset out
    ,.F23_P(F_23_N) ,.F23_N(F_23_P)
    // fmc tx clk in
    ,.FCLK0_M2C_P(F_CLK0_P) ,.FCLK0_M2C_N(F_CLK0_N)
    // fmc tx clk out
    ,.F0_P(F_00_N) ,.F0_N(F_00_P)
    // fmc tx data out
    ,.F1_P(F_01_N) ,.F1_N(F_01_P)
    ,.F16_P(F_16_N) ,.F16_N(F_16_P)
    ,.F15_P(F_15_N) ,.F15_N(F_15_P)
    ,.F13_P(F_13_N) ,.F13_N(F_13_P)
    ,.F11_P(F_11_N) ,.F11_N(F_11_P)
    ,.F10_P(F_10_N) ,.F10_N(F_10_P)
    ,.F14_P(F_14_N) ,.F14_N(F_14_P)
    ,.F9_P(F_09_N) ,.F9_N(F_09_P)
    ,.F4_P(F_04_N) ,.F4_N(F_04_P)
    ,.F7_P(F_07_N) ,.F7_N(F_07_P)
    ,.F8_P(F_08_N) ,.F8_N(F_08_P)
    // fmc rx clk in
    ,.F17_P(F_17_N) ,.F17_N(F_17_P)
    // fmc rx data in
    ,.F31_P(F_31_N) ,.F31_N(F_31_P)
    ,.F33_P(F_33_N) ,.F33_N(F_33_P)
    ,.F30_P(F_30_N) ,.F30_N(F_30_P)
    ,.F32_P(F_32_N) ,.F32_N(F_32_P)
    ,.F28_P(F_28_N) ,.F28_N(F_28_P)
    ,.F25_P(F_25_N) ,.F25_N(F_25_P)
    ,.F29_P(F_29_N) ,.F29_N(F_29_P)
    ,.F26_P(F_26_N) ,.F26_N(F_26_P)
    ,.F21_P(F_21_N) ,.F21_N(F_21_P)
    ,.F27_P(F_27_N) ,.F27_N(F_27_P)
    ,.F22_P(F_22_N) ,.F22_N(F_22_P)

    // --------------------- ASIC ------------------------

    // clk
    ,.CLK0(CLK0)
    ,.MSTR_SDO_CLK(MSTR_SDO_CLK)
    // asic reset out
    ,.AID10(AID10)

    // channel in

    // channel clk in
    ,.AOC0(AOC0)  ,.BOC0(BOC0) ,.COC0(COC0) ,.DOC0(DOC0)
    // channel valid in
    ,.AOD8(AOD8) ,.BOD8(BOD8) ,.COD8(COD8) ,.DOD8(DOD8)
    // channel data in
    //    A            B            C            D
    ,.AOD0(AOD0) ,.BOD0(BOD0) ,.COD0(COD0) ,.DOD0(DOD0)
    ,.AOD1(AOD1) ,.BOD1(BOD1) ,.COD1(COD1) ,.DOD1(DOD1)
    ,.AOD2(AOD2) ,.BOD2(BOD2) ,.COD2(COD2) ,.DOD2(DOD2)
    ,.AOD3(AOD3) ,.BOD3(BOD3) ,.COD3(COD3) ,.DOD3(DOD3)
    ,.AOD4(AOD4) ,.BOD4(BOD4) ,.COD4(COD4) ,.DOD4(DOD4)
    ,.AOD5(AOD5) ,.BOD5(BOD5) ,.COD5(COD5) ,.DOD5(DOD5)
    ,.AOD6(AOD6) ,.BOD6(BOD6) ,.COD6(COD6) ,.DOD6(DOD6)
    ,.AOD7(AOD7) ,.BOD7(BOD7) ,.COD7(COD7) ,.DOD7(DOD7)
    // channel token out
    ,.AOT0(AOT0) ,.BOT0(BOT0) ,.COT0(COT0) ,.DOT0(DOT0)

    // channel out

    // channel clk out
    ,.AIC0(AIC0) ,.BIC0(BIC0) ,.CIC0(CIC0) ,.DIC0(DIC0)
    // channel valid out
    ,.AID8(AID8) ,.BID8(BID8) ,.CID8(CID8) ,.DID8(DID8)
    // channel data out
    //    A            B            C            D
    ,.AID0(AID0) ,.BID0(BID0) ,.CID0(CID0) ,.DID0(DID0)
    ,.AID1(AID1) ,.BID1(BID1) ,.CID1(CID1) ,.DID1(DID1)
    ,.AID2(AID2) ,.BID2(BID2) ,.CID2(CID2) ,.DID2(DID2)
    ,.AID3(AID3) ,.BID3(BID3) ,.CID3(CID3) ,.DID3(DID3)
    ,.AID4(AID4) ,.BID4(BID4) ,.CID4(CID4) ,.DID4(DID4)
    ,.AID5(AID5) ,.BID5(BID5) ,.CID5(CID5) ,.DID5(DID5)
    ,.AID6(AID6) ,.BID6(BID6) ,.CID6(CID6) ,.DID6(DID6)
    ,.AID7(AID7) ,.BID7(BID7) ,.CID7(CID7) ,.DID7(DID7)
    // channel token in
    ,.AIT0(AIT0) ,.BIT0(BIT0) ,.CIT0(CIT0) ,.DIT0(DIT0));

  // ASIC

  logic ASIC_LED0, ASIC_LED1;

  bsg_asic asic
    // --------------------- GATEWAY ------------------------
    // clk
    (.CLK0(CLK0)
    ,.MSTR_SDO_CLK(MSTR_SDO_CLK)
    // reset from asic
    ,.AID10(AID10)
    // led
    ,.ASIC_LED0(ASIC_LED0) ,.ASIC_LED1(ASIC_LED1)

    // channel out

    // channel clk out
    ,.AOC0(AOC0)  ,.BOC0(BOC0) ,.COC0(COC0) ,.DOC0(DOC0)
    // channel valid out
    ,.AOD8(AOD8) ,.BOD8(BOD8) ,.COD8(COD8) ,.DOD8(DOD8)
    // channel data out
    //    A            B            C            D
    ,.AOD0(AOD0) ,.BOD0(BOD0) ,.COD0(COD0) ,.DOD0(DOD0)
    ,.AOD1(AOD1) ,.BOD1(BOD1) ,.COD1(COD1) ,.DOD1(DOD1)
    ,.AOD2(AOD2) ,.BOD2(BOD2) ,.COD2(COD2) ,.DOD2(DOD2)
    ,.AOD3(AOD3) ,.BOD3(BOD3) ,.COD3(COD3) ,.DOD3(DOD3)
    ,.AOD4(AOD4) ,.BOD4(BOD4) ,.COD4(COD4) ,.DOD4(DOD4)
    ,.AOD5(AOD5) ,.BOD5(BOD5) ,.COD5(COD5) ,.DOD5(DOD5)
    ,.AOD6(AOD6) ,.BOD6(BOD6) ,.COD6(COD6) ,.DOD6(DOD6)
    ,.AOD7(AOD7) ,.BOD7(BOD7) ,.COD7(COD7) ,.DOD7(DOD7)
    // channel token in
    ,.AOT0(AOT0) ,.BOT0(BOT0) ,.COT0(COT0) ,.DOT0(DOT0)

    // channel in

    // channel clk in
    ,.AIC0(AIC0) ,.BIC0(BIC0) ,.CIC0(CIC0) ,.DIC0(DIC0)
    // channel valid in
    ,.AID8(AID8) ,.BID8(BID8) ,.CID8(CID8) ,.DID8(DID8)
    // channel data in
    //    A            B            C            D
    ,.AID0(AID0) ,.BID0(BID0) ,.CID0(CID0) ,.DID0(DID0)
    ,.AID1(AID1) ,.BID1(BID1) ,.CID1(CID1) ,.DID1(DID1)
    ,.AID2(AID2) ,.BID2(BID2) ,.CID2(CID2) ,.DID2(DID2)
    ,.AID3(AID3) ,.BID3(BID3) ,.CID3(CID3) ,.DID3(DID3)
    ,.AID4(AID4) ,.BID4(BID4) ,.CID4(CID4) ,.DID4(DID4)
    ,.AID5(AID5) ,.BID5(BID5) ,.CID5(CID5) ,.DID5(DID5)
    ,.AID6(AID6) ,.BID6(BID6) ,.CID6(CID6) ,.DID6(DID6)
    ,.AID7(AID7) ,.BID7(BID7) ,.CID7(CID7) ,.DID7(DID7)
    // channel token out
    ,.AIT0(AIT0) ,.BIT0(BIT0) ,.CIT0(CIT0) ,.DIT0(DIT0));

endmodule
