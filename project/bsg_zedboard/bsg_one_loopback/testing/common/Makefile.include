#------------------------------------------------------------
# University of California, San Diego - Bespoke Systems Group
#------------------------------------------------------------
# File: Makefile.include
#
# Authors: Luis Vega - lvgutierrez@eng.ucsd.edu
#------------------------------------------------------------

# vcs common options

# compiler directives
VCS_OPTIONS += +define+SIMULATION
VCS_OPTIONS += +define+BSG_ZEDBOARD_FMC

# other options
VCS_OPTIONS += -full64
VCS_OPTIONS += +v2k
VCS_OPTIONS += +vc
VCS_OPTIONS += -sverilog
VCS_OPTIONS += -debug_pp
VCS_OPTIONS += +vcs+lic+wait
VCS_OPTIONS += +multisource_int_delays
VCS_OPTIONS += +neg_tchk
VCS_OPTIONS += +libext+.v+.vlib+.vh
VCS_OPTIONS += +notimingcheck
VCS_OPTIONS += -timescale=1ps/1ps

# vcs libraries for ml605 and gateway
VCS_OPTIONS += -y $(BSG_DESIGNS_DIR)/modules/bsg_guts
VCS_OPTIONS += -y $(BSG_ZEDBOARD_DIR)/src/v
VCS_OPTIONS += -y $(BSG_FPGA_IP_DIR)/bsg_zedboard/bsg_zedboard_fmc/v
VCS_OPTIONS += -y $(BSG_FPGA_IP_DIR)/bsg_gateway/bsg_gateway/v
VCS_OPTIONS += -y $(BSG_FPGA_IP_DIR)/bsg_asic/v
VCS_OPTIONS += -y $(BSG_IP_CORES_DIR)/bsg_comm_link
VCS_OPTIONS += -y $(BSG_IP_CORES_DIR)/bsg_fsb
VCS_OPTIONS += -y $(BSG_IP_CORES_DIR)/bsg_misc
VCS_OPTIONS += -y $(BSG_IP_CORES_DIR)/bsg_async
VCS_OPTIONS += -y $(BSG_IP_CORES_DIR)/bsg_mem
VCS_OPTIONS += -y $(BSG_IP_CORES_DIR)/bsg_dataflow
VCS_OPTIONS += -y $(BSG_IP_CORES_DIR)/bsg_test
VCS_OPTIONS += -y $(XILINX_VIVADO)/data/verilog/src/unisims

# xilinx file list
VCS_OPTIONS += -f $(XILINX_VIVADO)/data/secureip/iserdese2/iserdese2_cell.list.f
VCS_OPTIONS += -f $(XILINX_VIVADO)/data/secureip/oserdese2/oserdese2_cell.list.f

# for encrypted ip such as ISERDESE2/OSERDESE2
VCS_OPTIONS += -lca

# xilinx verilog models
XILINX_SRC = $(XILINX_ISE_DS_DIR)/ISE/verilog/src

BSG_TESTING_FILES = \
$(BSG_TESTING_DIR)/common/test_bsg.v \
$(XILINX_SRC)/glbl.v \
$(XILINX_VIVADO)/ids_lite/ISE/verilog/src/unisims/IBUFG.v \
$(XILINX_VIVADO)/ids_lite/ISE/verilog/src/unisims/MUXCY_L.v \
$(XILINX_VIVADO)/ids_lite/ISE/verilog/src/unisims/FD.v \
$(XILINX_VIVADO)/ids_lite/ISE/verilog/src/unisims/FD_1.v \
$(XILINX_VIVADO)/ids_lite/ISE/verilog/src/unisims/FDE.v \
$(XILINX_VIVADO)/ids_lite/ISE/verilog/src/unisims/FDR.v \
$(XILINX_VIVADO)/ids_lite/ISE/verilog/src/unisims/FDR_1.v \
$(XILINX_VIVADO)/ids_lite/ISE/verilog/src/unisims/FDS.v \
$(XILINX_VIVADO)/ids_lite/ISE/verilog/src/unisims/FDS_1.v \
$(XILINX_VIVADO)/ids_lite/ISE/verilog/src/unisims/LUT1_L.v \
$(XILINX_VIVADO)/ids_lite/ISE/verilog/src/unisims/LUT2_L.v \
$(XILINX_VIVADO)/ids_lite/ISE/verilog/src/unisims/LUT3_L.v \
$(XILINX_VIVADO)/ids_lite/ISE/verilog/src/unisims/LUT4_L.v \
$(XILINX_VIVADO)/ids_lite/ISE/verilog/src/unisims/LUT5_L.v \
$(XILINX_VIVADO)/ids_lite/ISE/verilog/src/unisims/LUT6_L.v \
$(XILINX_VIVADO)/ids_lite/ISE/verilog/src/unisims/BUFIO2.v \
$(XILINX_VIVADO)/ids_lite/ISE/verilog/src/unisims/PLL_ADV.v \
$(XILINX_VIVADO)/ids_lite/ISE/verilog/src/unisims/ODDR2.v \
$(XILINX_VIVADO)/ids_lite/ISE/verilog/src/unisims/DCM_CLKGEN.v \
$(XILINX_VIVADO)/ids_lite/ISE/verilog/src/unisims/IODELAY2.v \
$(XILINX_VIVADO)/ids_lite/ISE/verilog/src/unisims/BUFIO2_2CLK.v \
$(XILINX_VIVADO)/ids_lite/ISE/verilog/src/unisims/ISERDES2.v \
$(XILINX_VIVADO)/ids_lite/ISE/verilog/src/unisims/OSERDES2.v \
$(XILINX_VIVADO)/ids_lite/ISE/verilog/src/unisims/IBUFGDS.v
