`include "bsg_rocket_pkg.vh"
`include "bsg_fsb_pkg.v"

module bsg_rocket_baseline
  import bsg_fsb_pkg::*;
  import bsg_rocket_pkg::*;
# (parameter nodes_p=1
  ,parameter num_channels_p=4
  ,parameter channel_width_p=8
  ,parameter enabled_at_start_vec_p=1'b0
  ,parameter master_p=0
  ,parameter master_to_client_speedup_p=100
  ,parameter master_bypass_test_p=5'b00000)
  (input                        core_clk_i
  ,input                        io_master_clk_i
  ,input                        async_reset_i
  // input from i/o
  ,input   [num_channels_p-1:0] io_clk_tline_i
  ,input   [num_channels_p-1:0] io_valid_tline_i
  ,input  [channel_width_p-1:0] io_data_tline_i [num_channels_p-1:0]
  ,output  [num_channels_p-1:0] io_token_clk_tline_o
  // out to i/o
  ,output  [num_channels_p-1:0] im_clk_tline_o
  ,output  [num_channels_p-1:0] im_valid_tline_o
  ,output [channel_width_p-1:0] im_data_tline_o [num_channels_p-1:0]
  ,input   [num_channels_p-1:0] token_clk_tline_i
  // note: generate by the master (FPGA) and sent to the slave (ASIC)
  // not used by slave (ASIC).
  ,output reg                   im_slave_reset_tline_r_o
  // this signal is the post-calibration reset signal
  // synchronous to the core clock
  ,output                       core_reset_o);

  localparam ring_bytes_lp = 10;
  localparam ring_width_lp = ring_bytes_lp*channel_width_p;

  // into rocket nodes (fsb data)
  wire [nodes_p-1:0]       core_node_v_A;
  wire [ring_width_lp-1:0] core_node_data_A [nodes_p-1:0];
  wire [nodes_p-1:0]       core_node_ready_A;

  // into rocket nodes (fsb control)
  wire [nodes_p-1:0]       core_node_reset_r;
  wire [nodes_p-1:0]       core_node_en_r;

  // out of rocket nodes (fsb data)
  wire [nodes_p-1:0]       core_node_v_B;
  wire [ring_width_lp-1:0] core_node_data_B [nodes_p-1:0];
  wire [nodes_p-1:0]       core_node_yumi_B;

  // rocket node client

  bsg_rocket_node_client #
    (.dest_id_p(0))
  clnt
    (.clk_i(core_clk_i)
    ,.reset_i(core_node_reset_r[0])
    ,.en_i(core_node_en_r[0])
    // in
    ,.v_i(core_node_v_A[0])
    ,.data_i(core_node_data_A[0])
    ,.ready_o(core_node_ready_A[0])
    // out
    ,.v_o(core_node_v_B[0])
    ,.data_o(core_node_data_B[0])
    ,.yumi_i(core_node_yumi_B[0]));

  // fsb

  // fsb reset
  wire core_calib_done_r;

  // fsb in
  wire                     core_cl_valid;
  wire [ring_width_lp-1:0] core_cl_data;
  wire                     core_fsb_yumi;

  // fsb out
  wire                     core_fsb_valid;
  wire [ring_width_lp-1:0] core_fsb_data;
  wire                     core_cl_ready;

  bsg_fsb #
    (.width_p(ring_width_lp)
    ,.nodes_p(nodes_p)
    ,.snoop_vec_p( { nodes_p { 1'b0 } })
    // if master, enable at startup so that it can drive things
    ,.enabled_at_start_vec_p(enabled_at_start_vec_p))
  fsb
    (.clk_i(core_clk_i)
    ,.reset_i(~core_calib_done_r)

    // -------- fsb node --------

    // node ctrl
    ,.node_reset_r_o(core_node_reset_r)
    ,.node_en_r_o(core_node_en_r)

    // node in
    ,.node_v_i(core_node_v_B)
    ,.node_data_i(core_node_data_B)
    ,.node_yumi_o(core_node_yumi_B)

    // node out
    ,.node_v_o(core_node_v_A)
    ,.node_data_o(core_node_data_A)
    ,.node_ready_i(core_node_ready_A)

    // -------- bsg comm link --------

    // asm in
    ,.asm_v_i(core_cl_valid)
    ,.asm_data_i(core_cl_data)
    ,.asm_yumi_o(core_fsb_yumi)

    // asm out
    ,.asm_v_o(core_fsb_valid)
    ,.asm_data_o(core_fsb_data)
    ,.asm_ready_i(core_cl_ready));

  // comm_link

  bsg_comm_link #
    (.channel_width_p(channel_width_p)
    ,.core_channels_p(ring_bytes_lp)
    ,.link_channels_p(num_channels_p)
    ,.master_p(master_p)
    ,.master_to_slave_speedup_p(master_to_client_speedup_p)
    ,.master_bypass_test_p(master_bypass_test_p))
  comm_link
    (.core_clk_i(core_clk_i)
    ,.io_master_clk_i(io_master_clk_i)

    // reset
    ,.async_reset_i(async_reset_i)

    // core ctrl
    ,.core_calib_done_r_o(core_calib_done_r)

    // core in
    ,.core_valid_i(core_fsb_valid)
    ,.core_data_i(core_fsb_data)
    ,.core_ready_o(core_cl_ready)

    // core out
    ,.core_valid_o(core_cl_valid)
    ,.core_data_o(core_cl_data)
    ,.core_yumi_i(core_fsb_yumi)

    // in from i/o
    ,.io_valid_tline_i(io_valid_tline_i)
    ,.io_data_tline_i(io_data_tline_i)
    ,.io_clk_tline_i(io_clk_tline_i)              // clk
    ,.io_token_clk_tline_o(io_token_clk_tline_o)  // clk

    // out to i/o
    ,.im_valid_tline_o(im_valid_tline_o)
    ,.im_data_tline_o(im_data_tline_o)
    ,.im_clk_tline_o(im_clk_tline_o)       // clk
    ,.token_clk_tline_i(token_clk_tline_i) // clk

    ,.im_slave_reset_tline_r_o(im_slave_reset_tline_r_o));

  assign core_reset_o = ~core_calib_done_r;

endmodule
