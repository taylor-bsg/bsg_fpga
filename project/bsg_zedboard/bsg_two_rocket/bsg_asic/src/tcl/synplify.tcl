source $::env(BSG_ASIC_DIR)/src/tcl/common.tcl

project -new $bsg_syn_dir

# source
source $::env(BSG_ASIC_DIR)/src/tcl/filelist.tcl
foreach f $SVERILOG_SOURCE_FILES { add_file -verilog $f }

# include
source $::env(BSG_ASIC_DIR)/src/tcl/include.tcl
foreach f $SVERILOG_INCLUDE_PATHS {set_option -include_path $f}

add_file -constraint $::env(BSG_ASIC_DIR)/src/fdc/$bsg_fpga_top_name.fdc

# options
set_option -technology $device_tech
set_option -part $device_name
set_option -package $device_package
set_option -speed_grade $device_speed_grade
set_option -top_module $bsg_fpga_top_name
set_option -symbolic_fsm_compiler 1
set_option -frequency auto
set_option -vlog_std sysv
set_option -enable64bit 1
set_option -resource_sharing 1
set_option -pipe 1
set_option -write_verilog 1
set_option -maxfan 1000

# project
project -result_format "edif"
project -result_file $bsg_syn_dir/$bsg_fpga_top_name.edn
project -run
project -save $bsg_syn_dir/$bsg_fpga_top_name.prj
