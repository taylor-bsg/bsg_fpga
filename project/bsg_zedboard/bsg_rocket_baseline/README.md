### Source dependencies and versioning ###

This project support versioning, which means that uses snapshots (sha-commits)
from other bsg-repositories. The reason why this is required is for preserving
a known state for emulation efforts. The following [Makefile](https://bitbucket.org/taylor-bsg/bsg_fpga/src/master/project/bsg_zedboard/bsg_rocket_baseline/Makefile)
defines the commits being used to build this system.

After pulling the required dependencies, a developer may change sources under
the source tree to test new functionality.

### How to build ###

1. Run `make`

### How to test ###

1. Go to testing directory
2. Run `make`

### How to run zedboard ###

1. Copy boot.bin in SDCard and attach to the Zedboard
2. Configure the computer to be connected to Zedboard as gateway (192.168.1.1)
3. Connect ethernet cable between computer and Zedboard
4. ssh to zedboard (192.168.1.5), check RISCV for more info
5. Run `fesvr-zynq pk hello`
