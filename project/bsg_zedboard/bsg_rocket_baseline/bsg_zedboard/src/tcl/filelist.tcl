set bsg_fpga_ip_dir $::env(BSG_FPGA_IP_DIR)
set bsg_zedboard_dir $::env(BSG_ZEDBOARD_DIR)
set bsg_rocket_dir $bsg_zedboard_dir/out/bsg_rocket

set ZB_RTL_FILES [join "
  $bsg_fpga_ip_dir/bsg_zedboard/bsg_zedboard_rocket/v/host_axi_converter.v
  $bsg_zedboard_dir/src/v/Top.DefaultFPGAConfig.v
  $bsg_zedboard_dir/src/v/sram.v
  $bsg_zedboard_dir/src/v/bsg_zedboard_clk.v
  $bsg_zedboard_dir/src/v/bsg_zedboard.v
"]
