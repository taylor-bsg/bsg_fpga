`include "test_five.vh"

extern "A" void htif_fini(input reg failure);

`HTIF_TICK_DEFINITION(0, `HTIF_WIDTH)

`MEMORY_TICK_DEFINITION(0, `MEM_ADDR_BITS, `MEM_ID_BITS, `MEM_STRB_BITS, `MEM_DATA_BITS)

module test_bsg;

  // zedboard oscillator

  wire zb_osc_clk;
  bsg_nonsynth_clock_gen #(.cycle_time_p(`ZB_CLOCK_PERIOD_PS)) cc (.o(zb_osc_clk));

  wire host_clk; // generated inside zedboard by MMCM

  // bsg async reset

  localparam reset_cycles_hi_lp = 1000;
  localparam reset_cycles_lo_lp = 0;

  wire reset;

  bsg_nonsynth_reset_gen #
    (.num_clocks_p(1)
    ,.reset_cycles_lo_p(reset_cycles_lo_lp)
    ,.reset_cycles_hi_p(reset_cycles_hi_lp))
  reset_gen
    (.clk_i(host_clk)
    ,.async_reset_o(reset));

  // log/waveform

  integer stderr = 32'h80000002;
  logic htif_verbose = 1'b0;
  logic mem_verbose = 1'b0;
  logic [1023:0] vcdplusfile = '0;
  initial begin
    htif_verbose = $test$plusargs("htif_verbose");
    mem_verbose = $test$plusargs("mem_verbose");
    if ($value$plusargs("vcdplusfile=%s", vcdplusfile)) begin
      $vcdplusfile(vcdplusfile);
      $vcdpluson(0);
      $vcdplusmemon(0);
    end
  end

  // host

  logic                   r2h_en = '1;

  logic                   r2h_valid;
  logic [`HTIF_WIDTH-1:0] r2h_data;
  logic                   r2h_ready;

  logic                   h2r_valid;
  logic [`HTIF_WIDTH-1:0] h2r_data;
  logic                   h2r_ready;

  logic            [31:0] h2r_exit = '0;

  always @(posedge host_clk) begin
    if (reset) begin
      h2r_valid <= 1'b0;
      r2h_ready <= 1'b0;
      h2r_exit <= '0;
    end
    else begin
      htif_0_tick
        (r2h_en
        ,r2h_valid
        ,r2h_data
        ,r2h_ready
        ,h2r_valid
        ,h2r_data
        ,h2r_ready
        ,h2r_exit);
    end
  end

  always @(posedge host_clk) begin
    if (htif_verbose) begin
      if (h2r_valid & h2r_ready)
        $fdisplay(stderr, "H2R: %04x", h2r_data);
      if (r2h_valid & r2h_ready)
        $fdisplay(stderr, "R2H: %04x", r2h_data);
    end
  end

  logic                      mem_aw_valid;
  logic [`MEM_ADDR_BITS-1:0] mem_aw_addr;
  logic   [`MEM_ID_BITS-1:0] mem_aw_id;
  logic                [2:0] mem_aw_size;
  logic                [7:0] mem_aw_len;
  logic                      mem_aw_ready;

  logic                      mem_w_valid;
  logic [`MEM_STRB_BITS-1:0] mem_w_strb;
  logic [`MEM_DATA_BITS-1:0] mem_w_data;
  logic                      mem_w_last;
  logic                      mem_w_ready;

  logic                      mem_b_valid;
  logic                [1:0] mem_b_resp;
  logic   [`MEM_ID_BITS-1:0] mem_b_id;
  logic                      mem_b_ready;

  logic                      mem_ar_valid;
  logic [`MEM_ADDR_BITS-1:0] mem_ar_addr;
  logic   [`MEM_ID_BITS-1:0] mem_ar_id;
  logic                [2:0] mem_ar_size;
  logic                [7:0] mem_ar_len;
  logic                      mem_ar_ready;

  logic                       mem_r_valid;
  logic                 [1:0] mem_r_resp;
  logic    [`MEM_ID_BITS-1:0] mem_r_id;
  logic  [`MEM_DATA_BITS-1:0] mem_r_data;
  logic                       mem_r_last;
  logic                       mem_r_ready;

  always @(posedge host_clk) begin
    if (reset) begin
      mem_aw_ready <= 1'b0;
      mem_w_ready <= 1'b0;
      mem_b_valid <= 1'b0;
      mem_b_id <= 0;
      mem_b_resp <= 0;
      mem_ar_ready <= 1'b0;
      mem_r_valid <= 1'b0;
      mem_r_data <= 0;
    end
    else begin
      memory_0_tick
        (mem_aw_valid
        ,mem_aw_addr
        ,mem_aw_id
        ,mem_aw_size
        ,mem_aw_len
        ,mem_aw_ready
        ,mem_w_valid
        ,mem_w_strb
        ,mem_w_data
        ,mem_w_last
        ,mem_w_ready
        ,mem_b_valid
        ,mem_b_resp
        ,mem_b_id
        ,mem_b_ready
        ,mem_ar_valid
        ,mem_ar_addr
        ,mem_ar_id
        ,mem_ar_size
        ,mem_ar_len
        ,mem_ar_ready
        ,mem_r_valid
        ,mem_r_resp
        ,mem_r_id
        ,mem_r_data
        ,mem_r_last
        ,mem_r_ready);
    end
  end

  always @(posedge host_clk) begin
    if (mem_verbose) begin

      if (mem_ar_valid && mem_ar_ready)
        $fdisplay(stderr, "MC: ar addr=%x id=%x size=%x len=%x"
        ,mem_ar_addr
        ,mem_ar_id
        ,mem_ar_size
        ,mem_ar_len);

      if (mem_aw_valid && mem_aw_ready)
        $fdisplay(stderr, "MC: aw addr=%x id=%x size=%x len=%x"
        ,mem_aw_addr
        ,mem_aw_id
        ,mem_aw_size
        ,mem_aw_len);

      if (mem_w_valid && mem_w_ready)
        $fdisplay(stderr, "MC: w data=%x", mem_w_data);

      if (mem_r_valid && mem_r_ready)
        $fdisplay(stderr, "MC: r data=%x", mem_r_data);

    end
  end

  // exit

  always @(posedge host_clk) begin

    if (h2r_exit > 1) begin
      $display("\n*** FAIL");
      htif_fini(1'b1);
    end

    if (h2r_exit) begin
      $display("\n*** DONE");
      htif_fini(1'b0);
    end

  end

  // zedboard

  bsg_zedboard zb
    (.GCLK(zb_osc_clk)
`ifdef SIMULATION
    ,.reset_i(reset)
    ,.host_clk_o(host_clk)

    ,.host_valid_i(h2r_valid)
    ,.host_data_i(h2r_data)
    ,.host_ready_o(h2r_ready)

    ,.host_valid_o(r2h_valid)
    ,.host_data_o(r2h_data)
    ,.host_ready_i(r2h_ready)

    ,.mem_aw_valid_o(mem_aw_valid)
    ,.mem_aw_bits_addr_o(mem_aw_addr)
    ,.mem_aw_bits_len_o(mem_aw_len)
    ,.mem_aw_bits_size_o(mem_aw_size)
    ,.mem_aw_bits_id_o(mem_aw_id)
    ,.mem_aw_ready_i(mem_aw_ready)

    ,.mem_w_valid_o(mem_w_valid)
    ,.mem_w_bits_data_o(mem_w_data)
    ,.mem_w_bits_last_o(mem_w_last)
    ,.mem_w_bits_strb_o(mem_w_strb)
    ,.mem_w_ready_i(mem_w_ready)

    ,.mem_b_valid_i(mem_b_valid)
    ,.mem_b_bits_resp_i(mem_b_resp)
    ,.mem_b_bits_id_i(mem_b_id)
    ,.mem_b_ready_o(mem_b_ready)

    ,.mem_ar_valid_o(mem_ar_valid)
    ,.mem_ar_bits_addr_o(mem_ar_addr)
    ,.mem_ar_bits_len_o(mem_ar_len)
    ,.mem_ar_bits_size_o(mem_ar_size)
    ,.mem_ar_bits_id_o(mem_ar_id)
    ,.mem_ar_ready_i(mem_ar_ready)

    ,.mem_r_valid_i(mem_r_valid)
    ,.mem_r_bits_resp_i(mem_r_resp)
    ,.mem_r_bits_data_i(mem_r_data)
    ,.mem_r_bits_last_i(mem_r_last)
    ,.mem_r_bits_id_i(mem_r_id)
    ,.mem_r_ready_o(mem_r_ready)
`endif
    );

endmodule
