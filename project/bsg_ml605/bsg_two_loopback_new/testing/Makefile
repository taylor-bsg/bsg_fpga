# environment
include ../../../Makefile.include

# bsg_two_loopback_new variables
include ../Makefile.include

export BSG_TEST_DIR:=$(abspath .)
export BSG_OUT_DIR:=$(BSG_TEST_DIR)/out

trace_out = $(BSG_OUT_DIR)/trace.out
waveform_vpd = $(BSG_OUT_DIR)/waveform.vpd

default: clean $(waveform_vpd)

$(BSG_OUT_DIR):
	mkdir -p $@
	make -C $(BSG_BASE_DIR) setup
	make -C $(BSG_ASIC_DIR) setup
	make -C $(BSG_GATEWAY_DIR) setup
	make -C $(BSG_ML605_DIR) setup

export ML_RTL_LIB_NAME = ml_rtl
export ML_RTL_LIB = $(BSG_OUT_DIR)/$(ML_RTL_LIB_NAME).lib
export ML_RTL_LIST = $(BSG_OUT_DIR)/$(ML_RTL_LIB_NAME).f

export GW_RTL_LIB_NAME = gw_rtl
export GW_RTL_LIB = $(BSG_OUT_DIR)/$(GW_RTL_LIB_NAME).lib
export GW_RTL_LIST = $(BSG_OUT_DIR)/$(GW_RTL_LIB_NAME).f

export AC_RTL_LIB_NAME = ac_rtl
export AC_RTL_LIB = $(BSG_OUT_DIR)/$(AC_RTL_LIB_NAME).lib
export AC_RTL_LIST = $(BSG_OUT_DIR)/$(AC_RTL_LIB_NAME).f

export ISE_SIM_LIB_NAME = ise_sim
export ISE_SIM_LIB = $(BSG_OUT_DIR)/$(ISE_SIM_LIB_NAME).lib
export ISE_SIM_LIST = $(BSG_OUT_DIR)/$(ISE_SIM_LIB_NAME).f

sim_vsrc += $(BSG_TREE_DIR)/bsg_ip_cores/bsg_test/bsg_nonsynth_clock_gen.v
sim_vsrc += $(BSG_TREE_DIR)/bsg_ip_cores/bsg_test/bsg_nonsynth_reset_gen.v
sim_vsrc += $(BSG_TEST_DIR)/src/v/test_bsg.v
sim_vsrc += $(BSG_TEST_DIR)/src/v/cfg_rtl.v

VCS_OPT += -notice -line +lint=all,noVCDE,noONGS,noUI -error=PCWM-L -quiet
VCS_OPT += -full64 +rad +v2k +vcs+lic+wait +vc+list +libext+.v -debug_pp
VCS_OPT += -sverilog
VCS_OPT += -timescale=1ps/1ps -diag timescale

VCS_OPT += +define+ML_CLOCK_PERIOD_PS=5000
VCS_OPT += +define+GW_CLOCK_PERIOD_PS=6670
VCS_OPT += +define+SIMULATION

VCS_OPT += +define+ML_RTL_LIB_NAME=$(ML_RTL_LIB_NAME)
VCS_OPT += +define+GW_RTL_LIB_NAME=$(GW_RTL_LIB_NAME)
VCS_OPT += +define+AC_RTL_LIB_NAME=$(AC_RTL_LIB_NAME)
VCS_OPT += +define+ISE_SIM_LIB_NAME=$(ISE_SIM_LIB_NAME)
VCS_OPT += +define+BSG_ML605_FMC

VCS_OPT += $(sim_vsrc)

VCS_OPT += +vcs+finish+400us

VCS_OPT += -top cfg_rtl -top glbl
VCS_OPT += -f $(ML_RTL_LIST)
VCS_OPT += -f $(GW_RTL_LIST)
VCS_OPT += -f $(AC_RTL_LIST)
VCS_OPT += -f $(ISE_SIM_LIST)
VCS_OPT += -libmap $(ML_RTL_LIB)
VCS_OPT += -libmap $(GW_RTL_LIB)
VCS_OPT += -libmap $(AC_RTL_LIB)
VCS_OPT += -libmap $(ISE_SIM_LIB)
VCS_OPT += -lca
VCS_OPT += -l $(BSG_OUT_DIR)/vcs.log

CFG_DEP += $(ML_RTL_LIST) $(ML_RTL_LIB)
CFG_DEP += $(GW_RTL_LIST) $(GW_RTL_LIB)
CFG_DEP += $(AC_RTL_LIST) $(AC_RTL_LIB)
CFG_DEP += $(ISE_SIM_LIST) $(ISE_SIM_LIB)

$(CFG_DEP): | $(BSG_OUT_DIR)
	tclsh $(BSG_TEST_DIR)/src/tcl/cfg_vcs.tcl

VCS_DEP += $(BSG_OUT_DIR)
VCS_DEP += $(CFG_DEP)

simv = $(BSG_OUT_DIR)/simv

$(simv): $(VCS_DEP)
	$(VCS) $(VCS_OPT) -o $@

SIM_OPT += -q +ntb_random_seed_automatic

$(waveform_vpd): $(simv)
	$< $(SIM_OPT) 2>&1 > $(trace_out)

view:
	$(DVE_BIN)/dve -full64 -vpd $(waveform_vpd) &

junk = ucli.key csrc $(BSG_OUT_DIR) DVEfiles dramsim2_ini vc_hdrs.h

clean:
	-rm -rf $(junk)
	make -C $(BSG_RISCV_DIR)/rocket-chip/dramsim2 clean
	make -C $(BSG_ASIC_DIR) clean
	make -C $(BSG_GATEWAY_DIR) clean
	make -C $(BSG_ML605_DIR) clean
