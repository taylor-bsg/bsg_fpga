set xilinx_ise_src_dir $::env(XILINX_ISE_DS_DIR)/ISE/verilog/src
set xilinx_ise_unisims_dir $xilinx_ise_src_dir/unisims

set ISE_SIM_FILES [join "
  $xilinx_ise_src_dir/glbl.v
  $xilinx_ise_unisims_dir/IBUFGDS.v
  $xilinx_ise_unisims_dir/PLL_ADV.v
  $xilinx_ise_unisims_dir/DCM_CLKGEN.v
  $xilinx_ise_unisims_dir/ODDR2.v
  $xilinx_ise_unisims_dir/IBUFDS.v
  $xilinx_ise_unisims_dir/IBUFDS_DIFF_OUT.v
  $xilinx_ise_unisims_dir/OBUFDS.v
  $xilinx_ise_unisims_dir/IDELAYCTRL.v
  $xilinx_ise_unisims_dir/IODELAYE1.v
  $xilinx_ise_unisims_dir/IODELAY2.v
  $xilinx_ise_unisims_dir/BUFIO2_2CLK.v
  $xilinx_ise_unisims_dir/OSERDESE1.v
  $xilinx_ise_unisims_dir/ISERDESE1.v
  $xilinx_ise_unisims_dir/OSERDES2.v
  $xilinx_ise_unisims_dir/ISERDES2.v
  $xilinx_ise_unisims_dir/BUFG.v
  $xilinx_ise_unisims_dir/BUFIO2.v
  $xilinx_ise_unisims_dir/BUFIO.v
  $xilinx_ise_unisims_dir/BUFPLL.v
  $xilinx_ise_unisims_dir/BUFR.v
  $xilinx_ise_unisims_dir/ODDR.v
  $xilinx_ise_unisims_dir/MMCM_ADV.v
"]
