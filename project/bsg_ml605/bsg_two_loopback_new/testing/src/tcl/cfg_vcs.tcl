# scripts for creating filelist and library
source $::env(BSG_TREE_DIR)/bsg_rocket/common/tcl/bsg_vcs_create_filelist_library.tcl

# ml605 rtl
source $::env(BSG_ML605_DIR)/src/tcl/filelist.tcl
source $::env(BSG_ML605_DIR)/src/tcl/include.tcl

# ml605 rtl filelist
bsg_create_filelist $::env(ML_RTL_LIST) \
                    $ML_RTL_FILES

# ml605 rtl library
bsg_create_library $::env(ML_RTL_LIB_NAME) \
                   $::env(ML_RTL_LIB) \
                   $ML_RTL_FILES \
                   $ML_RTL_INCLUDE

# gateway rtl
source $::env(BSG_FPGA_IP_DIR)/bsg_gateway/$::env(BSG_GATEWAY_TARGET)/tcl/filelist.tcl
source $::env(BSG_FPGA_IP_DIR)/bsg_gateway/$::env(BSG_GATEWAY_TARGET)/tcl/include.tcl

# gateway rtl filelist
bsg_create_filelist $::env(GW_RTL_LIST) \
                    $GW_RTL_FILES

# gateway library
bsg_create_library $::env(GW_RTL_LIB_NAME) \
                   $::env(GW_RTL_LIB) \
                   $GW_RTL_FILES \
                   $GW_RTL_INCLUDE

# chip rtl
source $::env(BSG_TREE_DIR)/bsg_designs/toplevels/$::env(BSG_ASIC_TOP_NAME)/tcl/filelist.tcl
source $::env(BSG_TREE_DIR)/bsg_designs/toplevels/$::env(BSG_ASIC_TOP_NAME)/tcl/include.tcl

# chip rtl filelist
bsg_create_filelist $::env(AC_RTL_LIST) \
                    $AC_RTL_FILES

# chip rtl library
bsg_create_library $::env(AC_RTL_LIB_NAME) \
                   $::env(AC_RTL_LIB) \
                   $AC_RTL_FILES \
                   $AC_RTL_INCLUDE

# xilinx ise sim
source $::env(BSG_TEST_DIR)/src/tcl/xilinx_ise_sim_filelist.tcl

# xilinx ise sim filelist
bsg_create_filelist $::env(ISE_SIM_LIST) \
                    $ISE_SIM_FILES

# xilinx ise sim library
bsg_create_library $::env(ISE_SIM_LIB_NAME) \
                   $::env(ISE_SIM_LIB) \
                   $ISE_SIM_FILES
