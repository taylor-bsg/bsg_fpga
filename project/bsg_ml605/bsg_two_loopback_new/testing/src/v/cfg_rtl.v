config cfg_rtl;
  design test_bsg;
  instance test_bsg.ml liblist `ML_RTL_LIB_NAME `ISE_SIM_LIB_NAME;
  instance test_bsg.gw liblist `GW_RTL_LIB_NAME `ISE_SIM_LIB_NAME;
  instance test_bsg.ac liblist `AC_RTL_LIB_NAME `ISE_SIM_LIB_NAME;
endconfig
