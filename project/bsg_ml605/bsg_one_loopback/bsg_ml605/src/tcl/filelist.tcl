#------------------------------------------------------------
# University of California, San Diego - Bespoke Systems Group
#------------------------------------------------------------
# File: filelist.tcl
#
# Author: Luis Vega - lvgutierrez@eng.ucsd.edu
#------------------------------------------------------------

set SVERILOG_SOURCE_FILES [join "
  $bsg_out_ip_cores_dir/bsg_misc/bsg_defines.v
  $bsg_out_ip_cores_dir/bsg_fsb/bsg_fsb_pkg.v
  $bsg_out_designs_dir/modules/bsg_guts/bsg_test_node.v
  $bsg_out_ip_cores_dir/bsg_test/test_bsg_data_gen.v
  $bsg_out_ip_cores_dir/bsg_fsb/bsg_front_side_bus_hop_in.v
  $bsg_out_ip_cores_dir/bsg_fsb/bsg_front_side_bus_hop_out.v
  $bsg_out_ip_cores_dir/bsg_fsb/bsg_fsb_murn_gateway.v
  $bsg_out_ip_cores_dir/bsg_fsb/bsg_fsb_node_trace_replay.v
  $bsg_out_ip_cores_dir/bsg_fsb/bsg_fsb.v
  $bsg_out_ip_cores_dir/bsg_mem/bsg_mem_1r1w.v
  $bsg_out_ip_cores_dir/bsg_dataflow/bsg_two_fifo.v
  $bsg_out_ip_cores_dir/bsg_async/bsg_async_fifo.v
  $bsg_out_ip_cores_dir/bsg_async/bsg_launch_sync_sync.v
  $bsg_out_ip_cores_dir/bsg_async/bsg_async_ptr_gray.v
  $bsg_fpga_ip_dir/bsg_ml605/bsg_ml605_fmc/v/bsg_ml605_fmc_buffer.v
  $bsg_fpga_ip_dir/bsg_ml605/bsg_ml605_fmc/v/bsg_ml605_fmc_rx_clk.v
  $bsg_fpga_ip_dir/bsg_ml605/bsg_ml605_fmc/v/bsg_ml605_fmc_rx_data_bitslip_ctrl.v
  $bsg_fpga_ip_dir/bsg_ml605/bsg_ml605_fmc/v/bsg_ml605_fmc_rx_data.v
  $bsg_fpga_ip_dir/bsg_ml605/bsg_ml605_fmc/v/bsg_ml605_fmc_rx.v
  $bsg_fpga_ip_dir/bsg_ml605/bsg_ml605_fmc/v/bsg_ml605_fmc_tx_clk.v
  $bsg_fpga_ip_dir/bsg_ml605/bsg_ml605_fmc/v/bsg_ml605_fmc_tx_data.v
  $bsg_fpga_ip_dir/bsg_ml605/bsg_ml605_fmc/v/bsg_ml605_fmc_tx.v
  $bsg_fpga_ip_dir/bsg_ml605/bsg_ml605_fmc/v/bsg_ml605_fmc.v
  $bsg_src_dir/v/bsg_ml605_mmcm.v
  $bsg_src_dir/v/bsg_ml605.v
"]
