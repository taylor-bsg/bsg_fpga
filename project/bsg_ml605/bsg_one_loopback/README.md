Project description
===================

Running **bsg_one_loopback** on DoubleTrouble and Xilinx ML605 boards.

If you want to get a high-level idea of this project, please go to our
[wiki](https://bitbucket.org/taylor-bsg/bsg_fpga/wiki/Home) and over
there you will find a high-level system architecture diagram.

How to setup
============

* Remember to define variables in [Makefile.include](https://bitbucket.org/taylor-bsg/bsg_fpga/src/master/project/Makefile.include)

How to simulate
===============

The simulation shows sent and received values

* Run `make rtl_sim` for rtl simulation
* Run `make post_synth_sim` for post_synth simulation

How to create bitstreams
========================

* Build all three bitstream by:
    * Run `make`

Where are the bitstreams
========================

* After successfully building each bitstream, there should be located at:
    * `bsg_fpga/project/bsg_ml605/bsg_one_loopback/bsg_ml605/out/ise/bsg_ml605.bit`
    * `bsg_fpga/project/bsg_ml605/bsg_one_loopback/bsg_gateway/out/bsg_gateway_elf.bit`
    * `bsg_fpga/project/bsg_ml605/bsg_one_loopback/bsg_asic/out/ise/bsg_asic.bit`

How to program
==============

* You will need:
    * The Xilinx ML605 board
    * The DoubleTrouble board
    * Xilinx Platform Cable USB II
    * Power supply (12V) with molex connector
    * Xilinx Impact

How to test on the fpga
=======================

* Connect the usb cable from the JTAG-port in the ML605 to the host computer
* Open Xilinx chipscope
* Open the chipscope project located in ./testing/chipscope/bsg_ml605.cpj
* Run `open_cable` in chipscope
* Click on play-symbol or arm the trigger
* You will see 1024 samples, sent and received as simulation
