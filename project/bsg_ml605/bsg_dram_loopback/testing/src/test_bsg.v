//------------------------------------------------------------
// University of California, San Diego - Bespoke Systems Group
//------------------------------------------------------------
// File: test_bsg.v
//
// Authors: Luis Vega - lvgutierrez@eng.ucsd.edu
//------------------------------------------------------------

module test_bsg;

  logic SYSCLK_P, SYSCLK_N;

  initial SYSCLK_P = 1'b0;

  always #(2_500) SYSCLK_P = ~SYSCLK_P;

  assign SYSCLK_N = ~SYSCLK_P;

  logic CPU_RESET;

  initial begin
    CPU_RESET = 1'b0;
    #100_000;
    CPU_RESET = 1'b1;
    $display("TIME:%08d ML605 RESET HIGH", $time);
    #150_000;
    CPU_RESET = 1'b0;
    $display("TIME:%08d ML605 RESET LOW", $time);
  end

  // in case there is an issue with sim,
  // kill it after 100_000_000 ps
  initial begin
    #100_000_000;
    $finish;
  end

  // ML605 FPGA

  logic [7:0] GPIO_LED;

  logic GPIO_LED_0;
  logic GPIO_LED_1;
  logic GPIO_LED_2;
  logic GPIO_LED_3;
  logic GPIO_LED_4;
  logic GPIO_LED_5;
  logic GPIO_LED_6;
  logic GPIO_LED_7;

  wire DDR3_RESET_N;
  wire [63:0] DDR3_DQ;
  wire [14:0] DDR3_ADDR;
  wire [2:0] DDR3_BA;
  wire DDR3_RAS_N;
  wire DDR3_CAS_N;
  wire DDR3_WE_N;
  wire [0:0] DDR3_CS_N;
  wire [0:0] DDR3_ODT;
  wire [0:0] DDR3_CKE;
  wire [7:0] DDR3_DM;
  wire [7:0] DDR3_DQS_P;
  wire [7:0] DDR3_DQS_N;
  wire [0:0] DDR3_CK_P;
  wire [0:0] DDR3_CK_N;

  bsg_ml605 #
    (.SIM_BYPASS_INIT_CAL("FAST"))
  ml605_inst
    (.SYSCLK_P(SYSCLK_P) ,.SYSCLK_N(SYSCLK_N)
    ,.CPU_RESET(CPU_RESET)
    // led
    ,.GPIO_LED_0(GPIO_LED_0)
    ,.GPIO_LED_1(GPIO_LED_1)
    ,.GPIO_LED_2(GPIO_LED_2)
    ,.GPIO_LED_3(GPIO_LED_3)
    ,.GPIO_LED_4(GPIO_LED_4)
    ,.GPIO_LED_5(GPIO_LED_5)
    ,.GPIO_LED_6(GPIO_LED_6)
    ,.GPIO_LED_7(GPIO_LED_7)
    // dram
    ,.DDR3_DQ(DDR3_DQ)
    ,.DDR3_DQS_P(DDR3_DQS_P)
    ,.DDR3_DQS_N(DDR3_DQS_N)
    ,.DDR3_RESET_N(DDR3_RESET_N)
    ,.DDR3_CK_P(DDR3_CK_P)
    ,.DDR3_CK_N(DDR3_CK_N)
    ,.DDR3_CKE(DDR3_CKE)
    ,.DDR3_CS_N(DDR3_CS_N)
    ,.DDR3_RAS_N(DDR3_RAS_N)
    ,.DDR3_CAS_N(DDR3_CAS_N)
    ,.DDR3_WE_N(DDR3_WE_N)
    ,.DDR3_DM(DDR3_DM)
    ,.DDR3_BA(DDR3_BA)
    ,.DDR3_ADDR(DDR3_ADDR)
    ,.DDR3_ODT(DDR3_ODT));

  assign GPIO_LED = {GPIO_LED_7
                    ,GPIO_LED_6
                    ,GPIO_LED_5
                    ,GPIO_LED_4
                    ,GPIO_LED_3
                    ,GPIO_LED_2
                    ,GPIO_LED_1
                    ,GPIO_LED_0};

  initial $monitor("GPIO_LED:%08x", GPIO_LED);

  // ML605 DRAM DDR3 MODEL

  `define x1Gb
  `define sg187E
  `define x16

  bsg_ml605_ddr3_model ddr3_model_inst
    (.RESET(CPU_RESET)
    ,.DDR3_DQ(DDR3_DQ)
    ,.DDR3_DQS_P(DDR3_DQS_P)
    ,.DDR3_DQS_N(DDR3_DQS_N)
    ,.DDR3_RESET_N(DDR3_RESET_N)
    ,.DDR3_CK_P(DDR3_CK_P)
    ,.DDR3_CK_N(DDR3_CK_N)
    ,.DDR3_CKE(DDR3_CKE)
    ,.DDR3_CS_N(DDR3_CS_N)
    ,.DDR3_RAS_N(DDR3_RAS_N)
    ,.DDR3_CAS_N(DDR3_CAS_N)
    ,.DDR3_WE_N(DDR3_WE_N)
    ,.DDR3_DM(DDR3_DM)
    ,.DDR3_BA(DDR3_BA)
    ,.DDR3_ADDR(DDR3_ADDR)
    ,.DDR3_ODT(DDR3_ODT));

endmodule
