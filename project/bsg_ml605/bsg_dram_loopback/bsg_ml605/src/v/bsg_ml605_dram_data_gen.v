//------------------------------------------------------------
// University of California, San Diego - Bespoke Systems Group
//------------------------------------------------------------
// File: bsg_ml605_dram_data_gen.v
//
// Authors: Luis Vega - lvgutierrez@eng.ucsd.edu
//------------------------------------------------------------

module bsg_ml605_dram_data_gen
  (input clk_i
  ,input reset_i
  // ctrl
  ,input phy_init_done_i
  // status
  ,output done_o
  // in
  ,input valid_i
  ,input [31:0] data_i
  ,output thanks_o
  // out
  ,output valid_o
  ,output [31:0] data_o
  ,input thanks_i);

  typedef struct packed {bit [2:0] fbits;
                         bit [4:0] length;
                         bit [3:0] user;
                         bit [4:0] origY;
                         bit [4:0] origX;
                         bit [4:0] absY;
                         bit [4:0] absX;} raw_dynamic_packet_s;

  raw_dynamic_packet_s read_hdr, write_hdr;

  localparam kCacheLineSizeInWords          = 8;
  localparam kUserFieldCacheLineWrite       = 4'b0100;
  localparam kUserFieldCacheLineRead        = 4'b0000;
  localparam kUserFieldDmaReadNorth         = 4'b0001;
  localparam kUserFieldDmaReadWest          = 4'b0010;
  localparam kUserFieldEscape               = 4'b0101;
  localparam kUserFieldCacheLineWriteMask   = 4'b0111;
  localparam kUserFieldDmaTaggedReadReply   = 4'b1000;
  localparam kUserFieldMDNRelay	            = 4'b1101;
  localparam kUserFieldSystemMonitorService = 4'b1110;

  localparam num_words_lp = 483;

  typedef enum logic [6:0] {sStart       = 7'b0000001
                           ,sHeader      = 7'b0000010
                           ,sAddress     = 7'b0000100
                           ,sWriteData   = 7'b0001000
                           ,sWriteGetTag = 7'b0010000
                           ,sReadData    = 7'b0100000
                           ,sDone        = 7'b1000000} state_t;

  logic [31:0] read_addr_r, read_addr_n;

  always_ff @(posedge clk_i)
    if (reset_i == 1'b1)
      read_addr_r <= 32'b0;
    else
      read_addr_r <= read_addr_n;

  logic [31:0] write_addr_r, write_addr_n;

  always_ff @(posedge clk_i)
    if (reset_i == 1'b1)
      write_addr_r <= 32'b0;
    else
      write_addr_r <= write_addr_n;

  logic [31:0] write_data_r, write_data_n;

  always_ff @(posedge clk_i)
    if (reset_i == 1'b1)
      write_data_r <= 32'b0;
    else
      write_data_r <= write_data_n;

  logic write_r;
  logic write_change_en;
  logic [3:0] write_counter;

  always @(posedge clk_i)
    if (reset_i == 1'b1)
      write_counter <= 4'b0;
    else if (write_change_en == 1'b1)
      write_counter <= write_counter + 1'b1;

  always @(posedge clk_i)
    if (reset_i == 1'b1 || phy_init_done_i == 1'b0)
      write_r <= 1'b1;
    else if (write_counter == 4'd15 && write_change_en == 1'b1)
      write_r <= ~write_r;

  logic [$clog2(kCacheLineSizeInWords)-1:0] write_counter_r, write_counter_n;
  logic [$clog2(kCacheLineSizeInWords)-1:0] read_counter_r, read_counter_n;

  always @(posedge clk_i)
    if (reset_i == 1'b1)
      write_counter_r <= {$clog2(kCacheLineSizeInWords){1'b0}};
    else
      write_counter_r <= write_counter_n;

  always @(posedge clk_i)
    if (reset_i == 1'b1)
      read_counter_r <= {$clog2(kCacheLineSizeInWords){1'b0}};
    else
      read_counter_r <= read_counter_n;

  logic rd_done_r, rd_done_n;

  always_ff @(posedge clk_i)
    if (reset_i == 1'b1)
      rd_done_r  <= 1'b0;
    else
      rd_done_r  <= rd_done_n;

  state_t sender_state_r, sender_state_n;

  always @(posedge clk_i)
    if (reset_i == 1'b1)
      sender_state_r <= sStart;
    else
      sender_state_r <= sender_state_n;

  state_t receiver_state_r, receiver_state_n;

  always @(posedge clk_i)
    if (reset_i == 1'b1)
      receiver_state_r <= sStart;
    else
      receiver_state_r <= receiver_state_n;

  logic dram_credit_available_lo;
  logic valid_lo;
  logic [31:0] data_lo;

  always_comb begin

    sender_state_n = sender_state_r;
    write_addr_n = write_addr_r;
    read_addr_n =  read_addr_r;
    write_data_n = write_data_r;
    write_change_en = 1'b0;
    write_counter_n = write_counter_r;

    data_lo = 32'b0;
    valid_lo = 1'b0;

    unique case (sender_state_r)

      sStart:
        if (phy_init_done_i == 1'b1) begin
          if (receiver_state_r == sDone)
            sender_state_n = sDone;
          else
            sender_state_n = sHeader;
        end

      sHeader:
        if (dram_credit_available_lo == 1'b1) begin
          sender_state_n = sAddress;
          valid_lo = 1'b1;
          if (write_r == 1'b1)
            data_lo = write_hdr;
          else
            data_lo = read_hdr;
        end

      sAddress:
        if (dram_credit_available_lo == 1'b1) begin
          valid_lo = 1'b1;
          data_lo = write_r ? write_addr_r : read_addr_r;
          sender_state_n = write_r ? sWriteData : sWriteGetTag;
          if (write_r == 1'b1)
            write_addr_n = write_addr_r + 4*kCacheLineSizeInWords;
          else
            read_addr_n = read_addr_r + 4*kCacheLineSizeInWords;
        end

      sWriteGetTag:
        if (dram_credit_available_lo == 1'b1) begin
          valid_lo = 1'b1;
          data_lo  = 32'hf0f00f0f;
          sender_state_n = sHeader;
          write_change_en = 1'b1;
        end

      sWriteData:
        if (dram_credit_available_lo == 1'b1) begin
          valid_lo = 1'b1;
          data_lo = write_data_r;
          write_data_n = write_data_r + 1'b1;
          if (write_counter_r == (kCacheLineSizeInWords - 1)) begin
            sender_state_n = sHeader;
            write_change_en = 1'b1;
            write_counter_n = {$clog2(kCacheLineSizeInWords){1'b0}};
          end
          else
            write_counter_n = write_counter_r + 1'b1;
        end

      sDone: begin
        sender_state_n = sDone;
      end

    endcase
  end

  logic fifo_valid_lo;
  logic [31:0] fifo_data_lo;
  logic fifo_deq_li;

  // fifo

  bsg_fifo_1r1w_large #
    (.width_p(32)
    ,.els_p(8))
  fifo_inst
    (.clk_i(clk_i)
    ,.reset_i(reset_i)
    // in
    ,.v_i(valid_i)
    ,.data_i(data_i)
    ,.ready_o()
    // out
    ,.v_o(fifo_valid_lo)
    ,.data_o(fifo_data_lo)
    ,.yumi_i(fifo_deq_li));

  logic [31:0] read_data_r, read_data_n;

  always_ff @(posedge clk_i)
    if (reset_i == 1'b1)
      read_data_r <= 32'b0;
    else
      read_data_r <= read_data_n;

  always_comb begin

    rd_done_n = rd_done_r;
    receiver_state_n = receiver_state_r;
    read_data_n = read_data_r;
    read_counter_n = read_counter_r;
    fifo_deq_li = 1'b0;

    unique case (receiver_state_r)

      sStart:
        if (phy_init_done_i == 1'b1) begin
          receiver_state_n = sHeader;

        `ifdef SIMULATION
          $display("PHY INIT DONE\n");
        `endif

        end

      sHeader:
        if (fifo_valid_lo == 1'b1) begin
          receiver_state_n = sWriteGetTag;
          fifo_deq_li = 1'b1;
        end

      sWriteGetTag:
        if (fifo_valid_lo == 1'b1) begin
          receiver_state_n = sReadData;
          fifo_deq_li = 1'b1;
        end

      sReadData:
        if (fifo_valid_lo == 1'b1) begin

          if (fifo_data_lo == num_words_lp)
            receiver_state_n = sDone;

          fifo_deq_li = 1'b1;
          read_data_n = read_data_r + 1'b1;

          if (read_counter_r == (kCacheLineSizeInWords-1)) begin
            receiver_state_n = sHeader;
            read_counter_n = {$clog2(kCacheLineSizeInWords){1'b0}};
          end
          else
            read_counter_n = read_counter_r + 1'b1;

        end

      sDone: begin

        receiver_state_n = sDone;
        rd_done_n = 1'b1;

      end

    endcase

  end

  assign read_hdr.user = kUserFieldDmaReadNorth;
  assign write_hdr.user = kUserFieldCacheLineWrite;
  assign write_hdr.length = 8;

  // track credits going to DRAM
  // max_val_p -> number of credits
  // init_val_p -> start with max credits

  logic [2:0] credit_cnt_lo;

  bsg_counter_up_down #
    (.max_val_p(4)
    ,.init_val_p(4))
  credit_cnt_inst
    (.clk_i(clk_i)
    ,.reset_i(reset_i)
    ,.up_i(thanks_i)
    ,.down_i(valid_lo)
    ,.count_o(credit_cnt_lo));

  assign dram_credit_available_lo = (| credit_cnt_lo);

  assign thanks_o = fifo_deq_li;
  assign valid_o = valid_lo;
  assign data_o = data_lo;
  assign done_o = rd_done_r;

`ifndef SIMULATION

  // chipscope

  bsg_ml605_chipscope cs_inst
    (.clk_i(clk_i)
    ,.data_i({{215{1'b0}}
            ,reset_i
            ,fifo_valid_lo
            ,fifo_data_lo
            ,receiver_state_r}));

`else

  always @(posedge clk_i)
    if (receiver_state_r === sReadData)
      if (read_data_r === fifo_data_lo)
        $display("### PASS:0x%08x", fifo_data_lo);
      else
        $display("### FAIL R:0x%08x E:0x%08x\n",fifo_data_lo, read_data_r);

  always @(posedge clk_i)
    if (receiver_state_r === sDone) begin
      $display("### DONE\n");
      $finish;
    end

`endif

endmodule
