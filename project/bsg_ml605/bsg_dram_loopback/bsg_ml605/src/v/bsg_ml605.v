//------------------------------------------------------------
// University of California, San Diego - Bespoke Systems Group
//------------------------------------------------------------
// File: bsg_ml605.v
//
// Authors: Luis Vega - lvgutierrez@eng.ucsd.edu
//------------------------------------------------------------

`include "bsg_defines.v"

module bsg_ml605 #
  (parameter SIM_BYPASS_INIT_CAL = "OFF")
  (input SYSCLK_P, SYSCLK_N
  ,input CPU_RESET
  // led
  ,output GPIO_LED_0
  ,output GPIO_LED_1
  ,output GPIO_LED_2
  ,output GPIO_LED_3
  ,output GPIO_LED_4
  ,output GPIO_LED_5
  ,output GPIO_LED_6
  ,output GPIO_LED_7
  // dram
  ,inout [63:0] DDR3_DQ
  ,inout [7:0] DDR3_DQS_P
  ,inout [7:0] DDR3_DQS_N
  ,output DDR3_RESET_N
  ,output [0:0] DDR3_CK_P
  ,output [0:0] DDR3_CK_N
  ,output [0:0] DDR3_CKE
  ,output [0:0] DDR3_CS_N
  ,output DDR3_RAS_N
  ,output DDR3_CAS_N
  ,output DDR3_WE_N
  ,output [7:0] DDR3_DM
  ,output [2:0] DDR3_BA
  ,output [14:0] DDR3_ADDR
  ,output [0:0] DDR3_ODT);

  // clock

  logic clk_50_mhz_lo;
  logic clk_200_mhz_lo;

  bsg_ml605_mmcm mmcm_inst
    (.clk_200_mhz_p_i(SYSCLK_P) ,.clk_200_mhz_n_i(SYSCLK_N)
    ,.clk_50_mhz_o(clk_50_mhz_lo)
    ,.clk_200_mhz_o(clk_200_mhz_lo)
    ,.locked_o());

  // dram data gen

  logic dram_phy_init_done_lo;

  logic gen_done_lo;

  logic dram_valid_lo;
  logic [31:0] dram_data_lo;
  logic gen_thanks_lo;

  logic gen_valid_lo;
  logic [31:0] gen_data_lo;
  logic dram_thanks_lo;

  bsg_ml605_dram_data_gen gen_inst
    (.clk_i(clk_50_mhz_lo)
    ,.reset_i(CPU_RESET)
    // ctrl
    ,.phy_init_done_i(dram_phy_init_done_lo)
    // status
    ,.done_o(gen_done_lo)
    // in
    ,.valid_i(dram_valid_lo)
    ,.data_i(dram_data_lo)
    ,.thanks_o(gen_thanks_lo)
    // out
    ,.valid_o(gen_valid_lo)
    ,.data_o(gen_data_lo)
    ,.thanks_i(dram_thanks_lo));

  // dram ddr3 ctrl

  logic dram_pll_lock_lo;

  bsg_ml605_dram #
    (.SIM_BYPASS_INIT_CAL(SIM_BYPASS_INIT_CAL))
  dram_inst
    (.clk_i(clk_50_mhz_lo)
    ,.clk_200_mhz_i(clk_200_mhz_lo)
    ,.reset_i(CPU_RESET)
    // phy
    ,.phy_init_done_o(dram_phy_init_done_lo)
    ,.pll_lock_o(dram_pll_lock_lo)
    // in
    ,.valid_i(gen_valid_lo)
    ,.data_i(gen_data_lo)
    ,.thanks_o(dram_thanks_lo)
    // out
    ,.valid_o(dram_valid_lo)
    ,.data_o(dram_data_lo)
    ,.thanks_i(gen_thanks_lo)
    // ddr3
    ,.DDR3_CK_P(DDR3_CK_P)
    ,.DDR3_CK_N(DDR3_CK_N)
    ,.DDR3_ADDR(DDR3_ADDR)
    ,.DDR3_BA(DDR3_BA)
    ,.DDR3_RAS_N(DDR3_RAS_N)
    ,.DDR3_CAS_N(DDR3_CAS_N)
    ,.DDR3_WE_N(DDR3_WE_N)
    ,.DDR3_CS_N(DDR3_CS_N)
    ,.DDR3_CKE(DDR3_CKE)
    ,.DDR3_ODT(DDR3_ODT)
    ,.DDR3_RESET_N(DDR3_RESET_N)
    ,.DDR3_DM(DDR3_DM)
    ,.DDR3_DQ(DDR3_DQ)
    ,.DDR3_DQS_P(DDR3_DQS_P)
    ,.DDR3_DQS_N(DDR3_DQS_N));

  // led

  assign {GPIO_LED_7
         ,GPIO_LED_6
         ,GPIO_LED_5
         ,GPIO_LED_4
         ,GPIO_LED_3
         ,GPIO_LED_2
         ,GPIO_LED_1
         ,GPIO_LED_0} = ( CPU_RESET == 1'b0
                       && dram_phy_init_done_lo == 1'b1
                       && dram_pll_lock_lo == 1'b1
                       && gen_done_lo == 1'b1)? 8'hAA : 8'hFF;

endmodule
