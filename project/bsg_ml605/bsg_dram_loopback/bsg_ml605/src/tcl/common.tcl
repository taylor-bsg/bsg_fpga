#------------------------------------------------------------
# University of California, San Diego - Bespoke Systems Group
#------------------------------------------------------------
# File: common.tcl
#
# Authors: Luis Vega - lvgutierrez@eng.ucsd.edu
#------------------------------------------------------------

# common variable for synplify and xilinx

set bsg_top_name $::env(BSG_TOP_NAME)

set dc_dir $::env(DC_HOME)

set bsg_ip_cores_dir $::env(BSG_IP_CORES_DIR)
set bsg_fpga_ip_dir $::env(BSG_FPGA_IP_DIR)

set bsg_output_syn_dir $::env(BSG_OUTPUT_SYN_DIR)
set bsg_output_ise_dir $::env(BSG_OUTPUT_ISE_DIR)

set device_tech virtex6
set device_name xc6vlx240t
set device_package ff1156
set device_speed_grade -1
