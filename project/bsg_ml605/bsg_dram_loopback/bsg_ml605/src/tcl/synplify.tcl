#------------------------------------------------------------
# University of California, San Diego - Bespoke Systems Group
#------------------------------------------------------------
# File: synplify.tcl
#
# Authors: Luis Vega - lvgutierrez@eng.ucsd.edu
#------------------------------------------------------------

# synplify flow

source $::env(BSG_WORK_DIR)/src/tcl/common.tcl

set bsg_src_dir $::env(BSG_WORK_DIR)/src

project -new $bsg_output_syn_dir

# top file
add_file -verilog $bsg_src_dir/v/$bsg_top_name.v

# chipscope
add_file -edif $bsg_fpga_ip_dir/bsg_ml605/bsg_ml605_chipscope/ndf/bsg_ml605_chipscope_icon.ndf
add_file -edif $bsg_fpga_ip_dir/bsg_ml605/bsg_ml605_chipscope/ndf/bsg_ml605_chipscope_ila.ndf

# constraints
add_file -constraint $bsg_fpga_ip_dir/bsg_ml605/bsg_ml605_dram/fdc/bsg_ml605_dram.fdc
add_file -constraint $bsg_src_dir/fdc/bsg_ml605.fdc

# source dir
set_option -library_path $bsg_ip_cores_dir/bsg_async
set_option -library_path $bsg_ip_cores_dir/bsg_mem
set_option -library_path $bsg_ip_cores_dir/bsg_dataflow
set_option -library_path $bsg_ip_cores_dir/bsg_misc
set_option -library_path $bsg_src_dir/v
set_option -library_path $bsg_fpga_ip_dir/bsg_ml605/bsg_ml605_dram/v
set_option -library_path $bsg_fpga_ip_dir/bsg_ml605/bsg_ml605_chipscope/v
set_option -library_path $bsg_fpga_ip_dir/bsg_ml605/bsg_ml605_chipscope/ndf

# include dirs
set_option -include_path $bsg_ip_cores_dir/bsg_misc

# designware
set_option -dc_root $dc_dir
set_option -dw_library {dw_foundation}
set_option -enable_DesignWare 0

# standard options

# board specs
set_option -technology $device_tech
set_option -part $device_name
set_option -package $device_package
set_option -speed_grade $device_speed_grade
set_option -top_module $bsg_top_name

set_option -use_fsm_explorer 0
set_option -symbolic_fsm_compiler 1
set_option -compiler_compatible 0
set_option -resource_sharing 1

set_option -frequency auto
set_option -write_verilog 1
set_option -run_prop_extract 1
set_option -maxfan 10000
set_option -disable_io_insertion 0
set_option -pipe 1
set_option -update_models_cp 0
set_option -retiming 0
set_option -no_sequential_opt 0
set_option -fixgatedclocks 3
set_option -fixgeneratedclocks 3

set_option -enable_prepacking 1
set_option -write_apr_constraint 1
set_option -vlog_std sysv
set_option -project_relative_includes 1

project -result_format "edif"
project -result_file $bsg_output_syn_dir/$bsg_top_name.edn
project -run
project -save $bsg_output_syn_dir/$bsg_top_name.prj
