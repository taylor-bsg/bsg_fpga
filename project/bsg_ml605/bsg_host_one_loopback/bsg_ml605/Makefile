#------------------------------------------------------------
# University of California, San Diego - Bespoke Systems Group
#------------------------------------------------------------
# File: Makefile
#
# Author: Luis Vega - lvgutierrez@eng.ucsd.edu
#------------------------------------------------------------

# include licence and path variables
include ../../../Makefile.include

# include version numbers
include ../Makefile.version

export BSG_TOP_NAME=bsg_ml605
export BSG_WORK_DIR=$(abspath .)
export BSG_FPGA_IP_DIR=$(abspath ../../../../ip)
export BSG_OUT_DIR=$(BSG_WORK_DIR)/out
export BSG_OUT_VCS_DIR=$(BSG_OUT_DIR)/vcs
export BSG_OUT_SYN_DIR=$(BSG_OUT_DIR)/syn
export BSG_OUT_ISE_DIR=$(BSG_OUT_DIR)/ise
export BSG_OUT_IP_CORES_DIR=$(BSG_OUT_DIR)/bsg_ip_cores

BSG_LOCAL_IP_CORES = $(HOME)/raw/bsg_ip_cores

# input files
VCS_TCL = $(BSG_WORK_DIR)/src/tcl/vcs.tcl
SYNPLIFY_TCL = $(BSG_WORK_DIR)/src/tcl/synplify.tcl
XILINX_TCL = $(BSG_WORK_DIR)/src/tcl/xilinx.tcl

# logs
XILINX_LOG = $(BSG_OUT_DIR)/xilinx.log
SYNPLIFY_LOG = $(BSG_OUT_DIR)/synplify.log

# output
BSG_OUT_ML605_LIBRARY = $(BSG_OUT_VCS_DIR)/bsg_ml605.library
BSG_OUT_ML605_FILELIST = $(BSG_OUT_VCS_DIR)/bsg_ml605.filelist
BIT_FILE = $(BSG_OUT_ISE_DIR)/$(BSG_TOP_NAME).bit
SYNPLICITY_UCF = $(BSG_OUT_SYN_DIR)/synplicity.ucf
SYN_TOP_NCF = $(BSG_OUT_SYN_DIR)/$(BSG_TOP_NAME).ncf
SYN_TOP_EDN = $(BSG_OUT_SYN_DIR)/$(BSG_TOP_NAME).edn

default:
	. $(XILINX_ISE_DS_DIR)/settings64.sh; make $(BIT_FILE)

$(BSG_OUT_IP_CORES_DIR):
	mkdir -p $(BSG_OUT_DIR)
	test -d $(BSG_LOCAL_IP_CORES) && \
	git clone $(BSG_LOCAL_IP_CORES) $@ || \
	git clone git@bitbucket.org:taylor-bsg/bsg_ip_cores.git $@
	git -C $@ checkout -b testing $(BSG_ML605_IP_CORES_VERSION)

# VCS dir
$(BSG_OUT_VCS_DIR): $(BSG_OUT_IP_CORES_DIR)
	mkdir -p $@

# VCS library
$(BSG_OUT_ML605_LIBRARY) : $(BSG_OUT_VCS_DIR)
	tclsh $(VCS_TCL) library > $@

# VCS filelist
$(BSG_OUT_ML605_FILELIST) : $(BSG_OUT_ML605_LIBRARY)
	tclsh $(VCS_TCL) filelist > $@

# checkout source code
setup: $(BSG_OUT_ML605_FILELIST)

# synplify flow
$(SYN_TOP_EDN) $(SYN_TOP_NCF) $(SYNPLICITY_UCF): $(BSG_OUT_ML605_FILELIST)
	mkdir -p $(BSG_OUT_SYN_DIR)
	$(SYNPLIFY_BIN) -batch \
	                -licensetype synplifypremierdp \
		        -tcl $(SYNPLIFY_TCL) 2>&1 | tee $(SYNPLIFY_LOG)

# xilinx flow
$(BIT_FILE): $(SYN_TOP_EDN) $(SYN_TOP_NCF) $(SYNPLICITY_UCF)
	mkdir -p $(BSG_OUT_ISE_DIR)
	xtclsh $(XILINX_TCL) 2>&1 | tee $(XILINX_LOG)

clean:
	-rm -rf $(BSG_OUT_DIR) *.log synlog.tcl licbug.txt \
		xlnx_auto_0_xdb *.xrpt *.xml *.html _xmsgs \
		*.twr *.twx
