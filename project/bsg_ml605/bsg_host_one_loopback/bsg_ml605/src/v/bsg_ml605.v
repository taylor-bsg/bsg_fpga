//------------------------------------------------------------
// University of California, San Diego - Bespoke Systems Group
//------------------------------------------------------------
// File: bsg_ml605.v
//
// Author: Luis Vega - lvgutierrez@eng.ucsd.edu
//------------------------------------------------------------

`include "bsg_defines.v"

module bsg_ml605
  (input SYSCLK_P, SYSCLK_N
  // led
  ,output GPIO_LED_0
  ,output GPIO_LED_1
  ,output GPIO_LED_2
  ,output GPIO_LED_3
  ,output GPIO_LED_4
  ,output GPIO_LED_5
  ,output GPIO_LED_6
  ,output GPIO_LED_7
  // pcie clk
  ,input PCIE_250M_MGT1_P, PCIE_250M_MGT1_N
  // pcie reset (active low)
  ,input PCIE_PERST_B_LS
  // pcie data in
  ,input PCIE_RX0_P, PCIE_RX0_N
  ,input PCIE_RX1_P, PCIE_RX1_N
  ,input PCIE_RX2_P, PCIE_RX2_N
  ,input PCIE_RX3_P, PCIE_RX3_N
  // pcie data out
  ,output PCIE_TX0_P, PCIE_TX0_N
  ,output PCIE_TX1_P, PCIE_TX1_N
  ,output PCIE_TX2_P, PCIE_TX2_N
  ,output PCIE_TX3_P, PCIE_TX3_N
  // fmc gateway reset out
  ,output FMC_LPC_LA20_P, FMC_LPC_LA20_N
  // fmc ml605 reset in
  ,input FMC_LPC_LA23_P, FMC_LPC_LA23_N
  // fmc tx clk out
  ,output FMC_LPC_LA17_CC_P, FMC_LPC_LA17_CC_N
  // fmc tx data out
  ,output FMC_LPC_LA31_P, FMC_LPC_LA31_N
  ,output FMC_LPC_LA33_P, FMC_LPC_LA33_N
  ,output FMC_LPC_LA30_P, FMC_LPC_LA30_N
  ,output FMC_LPC_LA32_P, FMC_LPC_LA32_N
  ,output FMC_LPC_LA28_P, FMC_LPC_LA28_N
  ,output FMC_LPC_LA25_P, FMC_LPC_LA25_N
  ,output FMC_LPC_LA29_P, FMC_LPC_LA29_N
  ,output FMC_LPC_LA26_P, FMC_LPC_LA26_N
  ,output FMC_LPC_LA21_P, FMC_LPC_LA21_N
  ,output FMC_LPC_LA27_P, FMC_LPC_LA27_N
  ,output FMC_LPC_LA22_P, FMC_LPC_LA22_N
  // fmc rx clk out
  ,output FMC_LPC_CLK0_M2C_P, FMC_LPC_CLK0_M2C_N
  // fmc rx clk in
  ,input FMC_LPC_CLK1_M2C_P, FMC_LPC_CLK1_M2C_N
  // fmc rx data in
  ,input FMC_LPC_LA00_CC_P, FMC_LPC_LA00_CC_N
  ,input FMC_LPC_LA16_P, FMC_LPC_LA16_N
  ,input FMC_LPC_LA15_P, FMC_LPC_LA15_N
  ,input FMC_LPC_LA13_P, FMC_LPC_LA13_N
  ,input FMC_LPC_LA11_P, FMC_LPC_LA11_N
  ,input FMC_LPC_LA10_P, FMC_LPC_LA10_N
  ,input FMC_LPC_LA14_P, FMC_LPC_LA14_N
  ,input FMC_LPC_LA09_P, FMC_LPC_LA09_N
  ,input FMC_LPC_LA04_P, FMC_LPC_LA04_N
  ,input FMC_LPC_LA07_P, FMC_LPC_LA07_N
  ,input FMC_LPC_LA08_P, FMC_LPC_LA08_N);

  // mmcm

  logic clk_50_mhz_lo;
  logic clk_200_mhz_lo;

  bsg_ml605_mmcm mmcm_inst
    (.clk_200_mhz_p_i(SYSCLK_P) ,.clk_200_mhz_n_i(SYSCLK_N)
    ,.clk_50_mhz_o(clk_50_mhz_lo)
    ,.clk_200_mhz_o(clk_200_mhz_lo)
    ,.locked_o());

  // pcie

  localparam channel_lp = 3;

  logic pcie_reset_lo;
  logic fmc_ml605_reset_lo;

  logic fsb_node_reset_lo;
  logic fsb_node_en_lo;

  logic [31:0] pcie_status_r;

  always_ff @(posedge clk_50_mhz_lo)
    if (pcie_reset_lo == 1'b1)
      pcie_status_r <= 32'h00000000;
    else if ( fmc_ml605_reset_lo == 1'b0
           && fsb_node_reset_lo == 1'b0
           && fsb_node_en_lo == 1'b1)
      pcie_status_r <= 32'hA0A0CCBB;

  logic fsb_node_valid_lo;
  logic [79:0] fsb_node_data_lo [0:0];
  logic [79:0] fsb_node_unpacked_data_lo;

  assign fsb_node_unpacked_data_lo = fsb_node_data_lo[0];

  logic [channel_lp - 1 : 0] pcie_valid_li;
  logic [31:0] pcie_data_li [channel_lp - 1 : 0];
  logic [channel_lp - 1 : 0] pcie_ready_lo;

  assign pcie_valid_li = {channel_lp{((& pcie_ready_lo) & fsb_node_valid_lo)}};
  assign pcie_data_li[0] = fsb_node_unpacked_data_lo[31:00];
  assign pcie_data_li[1] = fsb_node_unpacked_data_lo[63:32];
  assign pcie_data_li[2] = {16'd0, fsb_node_unpacked_data_lo[79:64]};

  logic [channel_lp - 1 : 0] pcie_valid_lo;
  logic [31:0] pcie_data_lo [channel_lp - 1 : 0];
  logic [channel_lp - 1 : 0] pcie_yumi_li;

  logic fsb_node_yumi_lo;

  assign pcie_yumi_li = {channel_lp{fsb_node_yumi_lo}};

  bsg_ml605_pcie #
    (.channel_p(channel_lp))
  pcie_inst
    // clk
    (.clk_i(clk_50_mhz_lo)
    // ctrl
    ,.reset_o(pcie_reset_lo)
    // status register
    ,.status_register_i(pcie_status_r)
    // data in
    ,.valid_i(pcie_valid_li)
    ,.data_i(pcie_data_li)
    ,.ready_o(pcie_ready_lo)
    // data out
    ,.valid_o(pcie_valid_lo)
    ,.data_o(pcie_data_lo)
    ,.yumi_i(pcie_yumi_li)
    // pcie clk
    ,.PCIE_250M_MGT1_P(PCIE_250M_MGT1_P) ,.PCIE_250M_MGT1_N(PCIE_250M_MGT1_N)
    // pcie reset
    ,.PCIE_PERST_B_LS(PCIE_PERST_B_LS)
    // pcie data in
    ,.PCIE_RX0_P(PCIE_RX0_P) ,.PCIE_RX0_N(PCIE_RX0_N)
    ,.PCIE_RX1_P(PCIE_RX1_P) ,.PCIE_RX1_N(PCIE_RX1_N)
    ,.PCIE_RX2_P(PCIE_RX2_P) ,.PCIE_RX2_N(PCIE_RX2_N)
    ,.PCIE_RX3_P(PCIE_RX3_P) ,.PCIE_RX3_N(PCIE_RX3_N)
    // pcie data out
    ,.PCIE_TX0_P(PCIE_TX0_P) ,.PCIE_TX0_N(PCIE_TX0_N)
    ,.PCIE_TX1_P(PCIE_TX1_P) ,.PCIE_TX1_N(PCIE_TX1_N)
    ,.PCIE_TX2_P(PCIE_TX2_P) ,.PCIE_TX2_N(PCIE_TX2_N)
    ,.PCIE_TX3_P(PCIE_TX3_P) ,.PCIE_TX3_N(PCIE_TX3_N));

  // fsb

  logic fsb_node_valid_li;

  assign fsb_node_valid_li = (& pcie_valid_lo);

  logic [32*channel_lp - 1 : 0] pcie_unpacked_data_lo;

  assign pcie_unpacked_data_lo = {pcie_data_lo[2], pcie_data_lo[1], pcie_data_lo[0]};

  logic [79:0] fsb_node_data_li [0:0];

  assign fsb_node_data_li[0] = pcie_unpacked_data_lo[79:0];

  logic fsb_node_ready_li;

  assign fsb_node_ready_li = (& pcie_ready_lo);

  logic btf_valid_lo;
  logic [79:0] btf_data_lo;
  logic fsb_asm_yumi_lo;

  logic fsb_asm_valid_lo;
  logic [79:0] fsb_asm_data_lo;
  logic fmc_ready_lo;

  bsg_fsb #
    (.width_p(80)
    ,.nodes_p(1)
    ,.enabled_at_start_vec_p(1'b1)
    ,.snoop_vec_p(1'b0))
  fsb_inst
    (.clk_i(clk_50_mhz_lo)
    ,.reset_i(fmc_ml605_reset_lo)
    // node ctrl
    ,.node_reset_r_o(fsb_node_reset_lo)
    ,.node_en_r_o(fsb_node_en_lo)
    // node in
    ,.node_v_i(fsb_node_valid_li)
    ,.node_data_i(fsb_node_data_li)
    ,.node_yumi_o(fsb_node_yumi_lo)
    // node out
    ,.node_v_o(fsb_node_valid_lo)
    ,.node_data_o(fsb_node_data_lo)
    ,.node_ready_i(fsb_node_ready_li)
    // asm in
    ,.asm_v_i(btf_valid_lo)
    ,.asm_data_i(btf_data_lo)
    ,.asm_yumi_o(fsb_asm_yumi_lo)
    // asm out
    ,.asm_v_o(fsb_asm_valid_lo)
    ,.asm_data_o(fsb_asm_data_lo)
    ,.asm_ready_i(fmc_ready_lo));

  logic fmc_valid_lo;
  logic [79:0] fmc_data_lo;
  logic btf_ready_lo;

  bsg_two_fifo #
    (.width_p(80))
  btf_inst
    (.clk_i(clk_50_mhz_lo)
    ,.reset_i(fmc_ml605_reset_lo)
    // in
    ,.v_i(fmc_valid_lo)
    ,.data_i(fmc_data_lo)
    ,.ready_o(btf_ready_lo)
    // out
    ,.v_o(btf_valid_lo)
    ,.data_o(btf_data_lo)
    ,.yumi_i(fsb_asm_yumi_lo));

  // fmc

  bsg_ml605_fmc fmc_inst
    (.clk_i(clk_50_mhz_lo)
    // data in
    ,.valid_i(fsb_asm_valid_lo)
    ,.data_i(fsb_asm_data_lo)
    ,.ready_o(fmc_ready_lo)
    // data out
    ,.valid_o(fmc_valid_lo)
    ,.data_o(fmc_data_lo)
    ,.ready_i(btf_ready_lo)
    // board reset in
    ,.board_reset_i(pcie_reset_lo)
    // ml605 reset out
    ,.ml605_reset_o(fmc_ml605_reset_lo)
    // fmc clk for ml605 and gateway
    ,.fmc_clk_i(clk_200_mhz_lo)
    ,.fmc_clk_div_i(clk_50_mhz_lo)
    ,.fmc_clk_200_mhz_i(clk_200_mhz_lo)
    // fmc gateway reset out
    ,.FMC_LPC_LA20_P(FMC_LPC_LA20_P) ,.FMC_LPC_LA20_N(FMC_LPC_LA20_N)
    // fmc ml605 reset in
    ,.FMC_LPC_LA23_P(FMC_LPC_LA23_P) ,.FMC_LPC_LA23_N(FMC_LPC_LA23_N)
    // fmc tx clk out
    ,.FMC_LPC_LA17_CC_P(FMC_LPC_LA17_CC_P) ,.FMC_LPC_LA17_CC_N(FMC_LPC_LA17_CC_N)
    // fmc tx data out
    ,.FMC_LPC_LA31_P(FMC_LPC_LA31_P) ,.FMC_LPC_LA31_N(FMC_LPC_LA31_N)
    ,.FMC_LPC_LA33_P(FMC_LPC_LA33_P) ,.FMC_LPC_LA33_N(FMC_LPC_LA33_N)
    ,.FMC_LPC_LA30_P(FMC_LPC_LA30_P) ,.FMC_LPC_LA30_N(FMC_LPC_LA30_N)
    ,.FMC_LPC_LA32_P(FMC_LPC_LA32_P) ,.FMC_LPC_LA32_N(FMC_LPC_LA32_N)
    ,.FMC_LPC_LA28_P(FMC_LPC_LA28_P) ,.FMC_LPC_LA28_N(FMC_LPC_LA28_N)
    ,.FMC_LPC_LA25_P(FMC_LPC_LA25_P) ,.FMC_LPC_LA25_N(FMC_LPC_LA25_N)
    ,.FMC_LPC_LA29_P(FMC_LPC_LA29_P) ,.FMC_LPC_LA29_N(FMC_LPC_LA29_N)
    ,.FMC_LPC_LA26_P(FMC_LPC_LA26_P) ,.FMC_LPC_LA26_N(FMC_LPC_LA26_N)
    ,.FMC_LPC_LA21_P(FMC_LPC_LA21_P) ,.FMC_LPC_LA21_N(FMC_LPC_LA21_N)
    ,.FMC_LPC_LA27_P(FMC_LPC_LA27_P) ,.FMC_LPC_LA27_N(FMC_LPC_LA27_N)
    ,.FMC_LPC_LA22_P(FMC_LPC_LA22_P) ,.FMC_LPC_LA22_N(FMC_LPC_LA22_N)
    // fmc rx clk out
    ,.FMC_LPC_CLK0_M2C_P(FMC_LPC_CLK0_M2C_P) ,.FMC_LPC_CLK0_M2C_N(FMC_LPC_CLK0_M2C_N)
    // fmc rx clk in
    ,.FMC_LPC_CLK1_M2C_P(FMC_LPC_CLK1_M2C_P) ,.FMC_LPC_CLK1_M2C_N(FMC_LPC_CLK1_M2C_N)
    // fmc rx data in
    ,.FMC_LPC_LA00_CC_P(FMC_LPC_LA00_CC_P) ,.FMC_LPC_LA00_CC_N(FMC_LPC_LA00_CC_N)
    ,.FMC_LPC_LA16_P(FMC_LPC_LA16_P) ,.FMC_LPC_LA16_N(FMC_LPC_LA16_N)
    ,.FMC_LPC_LA15_P(FMC_LPC_LA15_P) ,.FMC_LPC_LA15_N(FMC_LPC_LA15_N)
    ,.FMC_LPC_LA13_P(FMC_LPC_LA13_P) ,.FMC_LPC_LA13_N(FMC_LPC_LA13_N)
    ,.FMC_LPC_LA11_P(FMC_LPC_LA11_P) ,.FMC_LPC_LA11_N(FMC_LPC_LA11_N)
    ,.FMC_LPC_LA10_P(FMC_LPC_LA10_P) ,.FMC_LPC_LA10_N(FMC_LPC_LA10_N)
    ,.FMC_LPC_LA14_P(FMC_LPC_LA14_P) ,.FMC_LPC_LA14_N(FMC_LPC_LA14_N)
    ,.FMC_LPC_LA09_P(FMC_LPC_LA09_P) ,.FMC_LPC_LA09_N(FMC_LPC_LA09_N)
    ,.FMC_LPC_LA04_P(FMC_LPC_LA04_P) ,.FMC_LPC_LA04_N(FMC_LPC_LA04_N)
    ,.FMC_LPC_LA07_P(FMC_LPC_LA07_P) ,.FMC_LPC_LA07_N(FMC_LPC_LA07_N)
    ,.FMC_LPC_LA08_P(FMC_LPC_LA08_P) ,.FMC_LPC_LA08_N(FMC_LPC_LA08_N));

  // led

  assign {GPIO_LED_7
         ,GPIO_LED_6
         ,GPIO_LED_5
         ,GPIO_LED_4
         ,GPIO_LED_3
         ,GPIO_LED_2
         ,GPIO_LED_1
         ,GPIO_LED_0} = pcie_status_r[7:0];

endmodule
