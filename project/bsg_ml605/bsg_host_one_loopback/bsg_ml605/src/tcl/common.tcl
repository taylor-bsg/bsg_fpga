#------------------------------------------------------------
# University of California, San Diego - Bespoke Systems Group
#------------------------------------------------------------
# File: common.tcl
#
# common variable for synplify and xilinx
#
# Author: Luis Vega - lvgutierrez@eng.ucsd.edu
#------------------------------------------------------------

set bsg_top_name $::env(BSG_TOP_NAME)

set bsg_fpga_ip_dir $::env(BSG_FPGA_IP_DIR)
set bsg_src_dir $::env(BSG_WORK_DIR)/src

set bsg_out_ip_cores_dir $::env(BSG_OUT_IP_CORES_DIR)
set bsg_out_syn_dir $::env(BSG_OUT_SYN_DIR)
set bsg_out_ise_dir $::env(BSG_OUT_ISE_DIR)

set device_tech virtex6
set device_name xc6vlx240t
set device_package ff1156
set device_speed_grade -1
