#------------------------------------------------------------
# University of California, San Diego - Bespoke Systems Group
#------------------------------------------------------------
# File: include.tcl
#
# Author: Luis Vega - lvgutierrez@eng.ucsd.edu
#------------------------------------------------------------

set SVERILOG_INCLUDE_PATHS [join "
  $bsg_out_ip_cores_dir/bsg_misc
"]
