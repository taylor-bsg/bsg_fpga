//------------------------------------------------------------
// University of California, San Diego - Bespoke Systems Group
//------------------------------------------------------------
// File: bsg_test.c
//
// Authors: Luis Vega - lvgutierrez@eng.ucsd.edu
//------------------------------------------------------------

#include "bsg_ml605_pcie.h"

int main() {

    bsg_ml605_pcie_device pcie;

    unsigned int d0[10];
    unsigned int d1[10];
    unsigned int d2[10];

    // reset

    d2[0] = 0x00000086; d1[0] = 0x00000000; d0[0] = 0x00000000;

    // enable

    d2[1] = 0x00000082; d1[1] = 0x00000000; d0[1] = 0x00000000;

    // data

    d2[2] = 0x00000000; d1[2] = 0x268711c1; d0[2] = 0xbb5539ea;
    d2[3] = 0x00000000; d1[3] = 0x4c66acad; d0[3] = 0xdc46d79e;
    d2[4] = 0x00000000; d1[4] = 0xc71b26b4; d0[4] = 0x4a377e7d;
    d2[5] = 0x00000000; d1[5] = 0x864ad765; d0[5] = 0xed7313e7;
    d2[6] = 0x00000000; d1[6] = 0x73202f0d; d0[6] = 0xf2a59b2b;
    d2[7] = 0x00000000; d1[7] = 0x39caec7a; d0[7] = 0xae3041e1;
    d2[8] = 0x00000000; d1[8] = 0x9fe6bead; d0[8] = 0xab09348c;
    d2[9] = 0x00000000; d1[9] = 0x313ee6c7; d0[9] = 0xda39a469;

    // status register

    unsigned int status_register_value;

    status_register_value = pcie.get_status_register_value();

    // status register stores 0xA0A0CCBB when GATEWAY and ASIC link is up

    while(1) {
        if(status_register_value != 0xA0A0CCBB)
            sleep(1);
        else
            break;
    }

    printf("\n[STATUS]: %08x\n", status_register_value);

    // reset

    pcie.write_packet_async_if_available(0, d0[0]);
    pcie.write_packet_async_if_available(1, d1[0]);
    pcie.write_packet_async_if_available(2, d2[0]);

    printf("\n[RESET]: %08x %08x %08x\n", d2[0], d1[0], d0[0]);

    // enable

    pcie.write_packet_async_if_available(0, d0[1]);
    pcie.write_packet_async_if_available(1, d1[1]);
    pcie.write_packet_async_if_available(2, d2[1]);

    printf("\n[ENABLE]: %08x %08x %08x\n", d2[1], d1[1], d0[1]);

    // data

    int i;
    unsigned int t2, t1, t0;
    int pass = 1;

    printf("\n");

    for (i = 2; i < 10; i++) {

        pcie.write_packet_async_if_available(0, d0[i]);
        pcie.write_packet_async_if_available(1, d1[i]);
        pcie.write_packet_async_if_available(2, d2[i]);

        printf("[WRITE]: %08x %08x %08x\n", d2[i], d1[i], d0[i]);

    }

    // test


    printf("\n");

    for (i = 2; i < 10; i++) {

        pcie.read_packet_blocking(0, t0);
        pcie.read_packet_blocking(1, t1);
        pcie.read_packet_blocking(2, t2);

        if (t2 == d2[i] && t1 == d1[i] && t0 == d0[i]) {
            printf("[READ]: %08x %08x %08x\n", t2, t1, t0);
        }
        else {
            printf("\n[FAIL]: %08x %08x %08x\n", t2, t1, t0);
            pass = 0;
        }

    }

    if (pass == 1) {
        printf("\n[PASS] TEST DONE\n");
    }
    else {
        printf("\n[FAIL] TEST DONE\n");
    }

    return 0;
}
