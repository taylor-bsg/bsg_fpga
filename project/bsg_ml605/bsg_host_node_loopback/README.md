How to setup
============

1. Remember to define variables in [Makefile.include](https://bitbucket.org/taylor-bsg/bsg_fpga/src/master/project/Makefile.include)
2. Setup kernel driver (one time, dell/gg00 machines have it already)
3. Create and download all three bitstreams
4. Reboot machine (bsg_host)
5. Go to [bsg_host](https://bitbucket.org/taylor-bsg/bsg_fpga/src/master/project/bsg_ml605_pcie_node_loopback/bsg_host)
6. Run `make`
7. `./bsg_test`


How to create bitstreams
========================

1. Run `make`

How to setup kernel driver
==========================

1. This driver has been tested on Centos 6.7 and Kernel 2.6.32-573.12.1.el6.x86_64
2. Go to [driver](https://bitbucket.org/taylor-bsg/bsg_fpga/src/master/ip/bsg_ml605/bsg_ml605_pcie/c/driver)
3. Download bitstreams and reboot the machine
4. Load module `/sbin/insmod bsg_ml605_pcie.ko`
5. `chmod 666 /dev/bsg_ml605_pcie`
6. Step 4-5 can be automated with a bash-script on `/etc/sysconfig/modules` or via udev-rules
