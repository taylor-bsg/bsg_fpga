#------------------------------------------------------------
# University of California, San Diego - Bespoke Systems Group
#------------------------------------------------------------
# File: synplify.tcl
#
# Authors: Luis Vega - lvgutierrez@eng.ucsd.edu
#------------------------------------------------------------

# synplify flow

source $::env(BSG_WORK_DIR)/src/tcl/common.tcl

set bsg_src_dir $::env(BSG_WORK_DIR)/src
set bsg_designs_dir $::env(BSG_DESIGNS_DIR)

project -new $bsg_output_syn_dir

# top file
add_file -verilog $bsg_src_dir/v/$bsg_top_name.v

# bsg_fsb_pkg
add_file -verilog $bsg_ip_cores_dir/bsg_fsb/bsg_fsb_pkg.v

# chipscope
add_file -edif $bsg_fpga_ip_dir/bsg_ml605/bsg_ml605_chipscope/ndf/bsg_ml605_chipscope_icon.ndf
add_file -edif $bsg_fpga_ip_dir/bsg_ml605/bsg_ml605_chipscope/ndf/bsg_ml605_chipscope_ila.ndf

# constraints
add_file -constraint $bsg_fpga_ip_dir/bsg_ml605/bsg_ml605_pcie/fdc/bsg_ml605_pcie.fdc
add_file -constraint $bsg_src_dir/fdc/$bsg_top_name.fdc

# source dir
set_option -library_path $bsg_src_dir/v
set_option -library_path $bsg_designs_dir/modules/bsg_guts
set_option -library_path $bsg_fpga_ip_dir/bsg_ml605/bsg_ml605_pcie/v
set_option -library_path $bsg_fpga_ip_dir/bsg_ml605/bsg_ml605_chipscope/v
set_option -library_path $bsg_ip_cores_dir/bsg_fsb
set_option -library_path $bsg_ip_cores_dir/bsg_dataflow
set_option -library_path $bsg_ip_cores_dir/bsg_misc
set_option -library_path $bsg_ip_cores_dir/bsg_async
set_option -library_path $bsg_ip_cores_dir/bsg_mem

# include dirs
set_option -include_path $bsg_ip_cores_dir/bsg_misc

# options
set_option -technology $device_tech
set_option -part $device_name
set_option -package $device_package
set_option -speed_grade $device_speed_grade
set_option -top_module $bsg_top_name
set_option -include_path $bsg_ip_cores_dir/bsg_misc
set_option -symbolic_fsm_compiler 1
set_option -frequency auto
set_option -vlog_std sysv
set_option -enable64bit 1
set_option -resource_sharing 1
set_option -pipe 1
set_option -write_verilog 1
set_option -maxfan 1000

# project
project -result_format "edif"
project -result_file $bsg_output_syn_dir/$bsg_top_name.edn
project -run
project -save $bsg_output_syn_dir/$bsg_top_name.prj
