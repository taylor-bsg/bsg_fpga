//------------------------------------------------------------
// University of California, San Diego - Bespoke Systems Group
//------------------------------------------------------------
// File: bsg_ml605.v
//
// Author: Luis Vega - lvgutierrez@eng.ucsd.edu
//------------------------------------------------------------

`include "bsg_defines.v"

module bsg_ml605
  (input SYSCLK_P, SYSCLK_N
  // led
  ,output GPIO_LED_0
  ,output GPIO_LED_1
  ,output GPIO_LED_2
  ,output GPIO_LED_3
  ,output GPIO_LED_4
  ,output GPIO_LED_5
  ,output GPIO_LED_6
  ,output GPIO_LED_7
  // pcie clk
  ,input PCIE_250M_MGT1_P, PCIE_250M_MGT1_N
  // pcie reset (active low)
  ,input PCIE_PERST_B_LS
  // pcie data in
  ,input PCIE_RX0_P, PCIE_RX0_N
  ,input PCIE_RX1_P, PCIE_RX1_N
  ,input PCIE_RX2_P, PCIE_RX2_N
  ,input PCIE_RX3_P, PCIE_RX3_N
  // pcie data out
  ,output PCIE_TX0_P, PCIE_TX0_N
  ,output PCIE_TX1_P, PCIE_TX1_N
  ,output PCIE_TX2_P, PCIE_TX2_N
  ,output PCIE_TX3_P, PCIE_TX3_N);

  // mmcm

  logic clk_50_mhz_lo;

  bsg_ml605_mmcm mmcm_inst
    (.clk_200_mhz_p_i(SYSCLK_P) ,.clk_200_mhz_n_i(SYSCLK_N)
    ,.clk_50_mhz_o(clk_50_mhz_lo)
    ,.locked_o());

  // pcie

  localparam channel_lp = 3;

  logic pcie_reset_lo;

  logic [31:0] pcie_status_r;

  always_ff @(posedge clk_50_mhz_lo)
    if (pcie_reset_lo == 1'b1)
      pcie_status_r <= 32'h00000000;
    else
      pcie_status_r <= 32'hA0A0CCBB;

  logic fsb_asm_valid_lo;
  logic [79:0] fsb_asm_data_lo;

  logic [channel_lp - 1 : 0] pcie_valid_li;
  logic [31:0] pcie_data_li [channel_lp - 1 : 0];
  logic [channel_lp - 1 : 0] pcie_ready_lo;

  assign pcie_valid_li = {channel_lp{((& pcie_ready_lo) & fsb_asm_valid_lo)}};
  assign pcie_data_li[0] = fsb_asm_data_lo[31:00];
  assign pcie_data_li[1] = fsb_asm_data_lo[63:32];
  assign pcie_data_li[2] = {16'd0, fsb_asm_data_lo[79:64]};

  logic [channel_lp - 1 : 0] pcie_valid_lo;
  logic [31:0] pcie_data_lo [channel_lp - 1 : 0];
  logic [channel_lp - 1 : 0] pcie_yumi_li;

  logic fsb_asm_yumi_lo;

  assign pcie_yumi_li = {channel_lp{fsb_asm_yumi_lo}};

  bsg_ml605_pcie #
    (.channel_p(channel_lp))
  pcie_inst
    // clk
    (.clk_i(clk_50_mhz_lo)
    // ctrl
    ,.reset_o(pcie_reset_lo)
    // status register
    ,.status_register_i(pcie_status_r)
    // data in
    ,.valid_i(pcie_valid_li)
    ,.data_i(pcie_data_li)
    ,.ready_o(pcie_ready_lo)
    // data out
    ,.valid_o(pcie_valid_lo)
    ,.data_o(pcie_data_lo)
    ,.yumi_i(pcie_yumi_li)
    // pcie clk
    ,.PCIE_250M_MGT1_P(PCIE_250M_MGT1_P) ,.PCIE_250M_MGT1_N(PCIE_250M_MGT1_N)
    // pcie reset
    ,.PCIE_PERST_B_LS(PCIE_PERST_B_LS)
    // pcie data in
    ,.PCIE_RX0_P(PCIE_RX0_P) ,.PCIE_RX0_N(PCIE_RX0_N)
    ,.PCIE_RX1_P(PCIE_RX1_P) ,.PCIE_RX1_N(PCIE_RX1_N)
    ,.PCIE_RX2_P(PCIE_RX2_P) ,.PCIE_RX2_N(PCIE_RX2_N)
    ,.PCIE_RX3_P(PCIE_RX3_P) ,.PCIE_RX3_N(PCIE_RX3_N)
    // pcie data out
    ,.PCIE_TX0_P(PCIE_TX0_P) ,.PCIE_TX0_N(PCIE_TX0_N)
    ,.PCIE_TX1_P(PCIE_TX1_P) ,.PCIE_TX1_N(PCIE_TX1_N)
    ,.PCIE_TX2_P(PCIE_TX2_P) ,.PCIE_TX2_N(PCIE_TX2_N)
    ,.PCIE_TX3_P(PCIE_TX3_P) ,.PCIE_TX3_N(PCIE_TX3_N));

  // chipscope

  bsg_ml605_chipscope cs_inst
    (.clk_i(clk_50_mhz_lo)
    ,.data_i({{52{1'b0}}
            ,pcie_reset_lo
            // pcie in
            ,pcie_ready_lo
            ,pcie_valid_li
            ,pcie_data_li
            // pcie out
            ,pcie_yumi_li
            ,pcie_valid_lo
            ,pcie_data_lo}));

  // fsb

  logic [32*channel_lp - 1 : 0] fsb_asm_data_li;

  assign fsb_asm_data_li = {pcie_data_lo[2], pcie_data_lo[1], pcie_data_lo[0]};

  logic fsb_node_reset_lo;
  logic fsb_node_en_lo;

  logic node_valid_lo;
  logic [79:0] node_data_lo [0:0];
  logic fsb_node_yumi_lo;

  logic fsb_node_valid_lo;
  logic [79:0] fsb_node_data_lo [0:0];
  logic node_ready_lo;

  bsg_fsb #
    (.width_p(80)
    ,.nodes_p(1)
    ,.enabled_at_start_vec_p(1'b0)
    ,.snoop_vec_p(1'b0))
  fsb_inst
    (.clk_i(clk_50_mhz_lo)
    ,.reset_i(pcie_reset_lo)
    // asm in
    ,.asm_v_i((& pcie_valid_lo))
    ,.asm_data_i(fsb_asm_data_li)
    ,.asm_yumi_o(fsb_asm_yumi_lo)
    // asm out
    ,.asm_v_o(fsb_asm_valid_lo)
    ,.asm_data_o(fsb_asm_data_lo)
    ,.asm_ready_i((& pcie_ready_lo))
    // node ctrl
    ,.node_reset_r_o(fsb_node_reset_lo)
    ,.node_en_r_o(fsb_node_en_lo)
    // node in
    ,.node_v_i(node_valid_lo)
    ,.node_data_i(node_data_lo)
    ,.node_yumi_o(fsb_node_yumi_lo)
    // node out
    ,.node_v_o(fsb_node_valid_lo)
    ,.node_data_o(fsb_node_data_lo)
    ,.node_ready_i(node_ready_lo));

  // node

  bsg_test_node #
    (.ring_width_p(80)
    ,.master_p(0)
    ,.master_id_p(0)
    ,.slave_id_p(0))
  ml605_node
    (.clk_i(clk_50_mhz_lo)
    // ctrl
    ,.reset_i(fsb_node_reset_lo)
    ,.en_i(fsb_node_en_lo)
    // in
    ,.v_i(fsb_node_valid_lo)
    ,.data_i(fsb_node_data_lo[0])
    ,.ready_o(node_ready_lo)
    // out
    ,.v_o(node_valid_lo)
    ,.data_o(node_data_lo[0])
    ,.yumi_i(fsb_node_yumi_lo));

  // led

  assign {GPIO_LED_7
         ,GPIO_LED_6
         ,GPIO_LED_5
         ,GPIO_LED_4
         ,GPIO_LED_3
         ,GPIO_LED_2
         ,GPIO_LED_1
         ,GPIO_LED_0} = (pcie_reset_lo == 1'b0)? 8'hAA : 8'hFF;

endmodule
