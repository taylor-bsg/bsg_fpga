//------------------------------------------------------------
// University of California, San Diego - Bespoke Systems Group
//------------------------------------------------------------
// File: bsg_ml605_trace_replay.v
//
// Author: Luis Vega - lvgutierrez@eng.ucsd.edu
//------------------------------------------------------------

module bsg_ml605_trace_replay
  (input clk_i
  // ctrl
  ,input reset_i
  ,input en_i
  ,output done_o
  ,output error_o
  // in
  ,input valid_i
  ,input [79:0] data_i
  ,output ready_o
  // out
  ,output valid_o
  ,output [79:0] data_o
  ,input yumi_i);

  logic tr_ready_lo;

  logic tr_valid_lo;
  logic [79:0] tr_data_lo;

  // this is enough for rom.trace (18 ops)
  localparam rom_addr_width_lp = 5;

  logic [rom_addr_width_lp - 1 : 0] tr_rom_addr_lo;
  logic [83:0] rom_data_lo;

  bsg_fsb_node_trace_replay #
    (.ring_width_p(80)
    ,.master_id_p(0)
    ,.slave_id_p(0)
    ,.rom_addr_width_p(rom_addr_width_lp))
  tr
    (.clk_i(clk_i)
    // ctrl
    ,.reset_i(reset_i)
    ,.en_i(en_i)
    // in
    ,.v_i(valid_i)
    ,.data_i(data_i)
    ,.ready_o(tr_ready_lo)
    // out
    ,.v_o(tr_valid_lo)
    ,.data_o(tr_data_lo)
    ,.yumi_i(yumi_i)
    // rom
    ,.rom_addr_o(tr_rom_addr_lo)
    ,.rom_data_i(rom_data_lo)
    // trace ctrl
    ,.done_o(done_o)
    ,.error_o(error_o));

  assign ready_o = tr_ready_lo;

  assign valid_o = tr_valid_lo;
  assign data_o = tr_data_lo;

  // since this module is being generated in the makefile,
  // the module name is set by a compiler directive
  `BSG_ML605_ROM_NAME #
    (.width_p(84)
    ,.addr_width_p(rom_addr_width_lp))
  rom
    (.addr_i(tr_rom_addr_lo)
    ,.data_o(rom_data_lo));

`ifndef SIMULATION

  // chipscope

  bsg_ml605_chipscope cs
    (.clk_i(clk_i)
    ,.data_i({'0
             ,reset_i
             ,tr_ready_lo
             ,valid_i
             ,data_i
             ,tr_valid_lo
             ,tr_data_lo}));

`endif

endmodule
