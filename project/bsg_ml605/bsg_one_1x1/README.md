Project description
===================

Create a basic **greendroid_node** system, where the **bsg_ml605** sends
some packets to *wake up* the **greendroid_node** on **bsg_asic** and
received back a finish packet in **bsg_ml605**

If you want to get a high-level idea of this project, please go to our
[wiki](https://bitbucket.org/taylor-bsg/bsg_fpga/wiki/Home) and over there you
will find a high-level system architecture diagram

How to setup
============

* Remember to define variables in [Makefile.include](https://bitbucket.org/taylor-bsg/bsg_fpga/src/master/project/Makefile.include)
* The required **raw** repositories (crudo, io_master and murn) are checked out
automatically with the flow. The cvs root folder is defined in
[Makefile.include](https://bitbucket.org/taylor-bsg/bsg_fpga/src/master/project/Makefile.include)

How to simulate
===============

* Run `make rtl_sim` will run RTL-simulation [bsg_ml605(rtl) <-> bsg_gateway(rtl) <-> bsg_asic(rtl)]
* Run `make post_synth_sim` will run RTL/post-fpga-synth-netlist simulation [bsg_ml605(rtl) <-> bsg_gateway(rtl) <-> bsg_asic(post-synth-fpga-netlist)]
* In any case, sim or netsim, you should get the following message:

```
############################################################################
###### DONE (trace finished; CALLING $finish) (test_bsg.ml605_inst.tr_inst.tr_inst)
############################################################################
```

How to create bitstreams
========================

* Run `make`

Where are the bitstreams
========================

* After successfully building each bitstream, there should be located at:
    * `bsg_fpga/project/bsg_ml605/bsg_one_1x1/bsg_ml605/out/ise/bsg_ml605.bit`
    * `bsg_fpga/project/bsg_ml605/bsg_one_1x1/bsg_gateway/out/bsg_gateway_elf.bit`
    * `bsg_fpga/project/bsg_ml605/bsg_one_1x1/bsg_asic/out/ise/bsg_asic.bit`

How to program
==============

* You will need:
    * The Xilinx ML605 board
    * The DoubleTrouble board
    * Xilinx Platform Cable USB II
    * Power supply (12V) with molex connector
    * Xilinx Impact

How to debug
============

* Debugging bsg_ml605
    * There is a chipscope module [bsg_ml605_chipscope.v](https://bitbucket.org/taylor-bsg/bsg_fpga/src/master/ip/bsg_ml605/bsg_ml605_chipscope/v/bsg_ml605_chipscope.v)
    instantiated in [bsg_ml605_trace_replay](https://bitbucket.org/taylor-bsg/bsg_fpga/src/master/project/bsg_ml605_gateway_asic_one_1x1/flow/bsg_ml605/src/v/bsg_ml605_trace_replay.v)
    * You can open Xilinx chipscope tool `analyzer` and capture sent and received values

* Debugging bsg_gateway
    * There is a chipscope module [bsg_gateway_chipscope.v](https://bitbucket.org/taylor-bsg/bsg_fpga/src/master/ip/bsg_gateway/bsg_gateway_chipscope/v/bsg_gateway_chipscope.v)
    instantiated in [bsg_gateway.v](https://bitbucket.org/taylor-bsg/bsg_fpga/src/master/ip/bsg_gateway/bsg_gateway/v/bsg_gateway.v)
    * You can open Xilinx chipscope tool `analyzer` and capture values between `bsg_ml605` and `bsg_asic`
