#------------------------------------------------------------
# University of California, San Diego - Bespoke Systems Group
#------------------------------------------------------------
# File: Makefile
#
# Author: Luis Vega - lvgutierrez@eng.ucsd.edu
#
# The test being executed here is:
#
# bsg_ml605(rtl)<->bsg_gateway(rtl)<->bsg_asic(post_synth)
#------------------------------------------------------------

# include licence, tool and path variables
include ../../../../Makefile.include
# include common options
include ../common/Makefile.include

BSG_ML605_DIR = $(abspath ../../bsg_ml605)
BSG_GATEWAY_DIR = $(abspath ../../bsg_gateway)
BSG_ASIC_DIR = $(abspath ../../bsg_asic)

BSG_OUT_DIR = $(abspath ./out)
BSG_OUT_SIM = $(BSG_OUT_DIR)/sim

BSG_ASIC_NETLIST = $(BSG_ASIC_DIR)/out/syn/bsg_asic.vm
BSG_ASIC_LIB = $(BSG_OUT_DIR)/bsg_asic.library

VCS_OPTIONS += +define+SYSCLK_HALF_PERIOD=2500
VCS_OPTIONS += +define+CLK_OSC_HALF_PERIOD=3335
VCS_OPTIONS += +define+CPU_RESET_DEADTIME=100000
VCS_OPTIONS += +define+CPU_RESET_HIGHTIME=150000
VCS_OPTIONS += +define+FINISH_SIMULATION_TIME=2650000000

VCS_OPTIONS += -top test_bsg_post_synth
VCS_OPTIONS += -top glbl

# verilog config related options
VCS_OPTIONS += -libmap $(BSG_ML605_DIR)/out/vcs/bsg_ml605.library
VCS_OPTIONS += -libmap $(BSG_GATEWAY_DIR)/out/vcs/bsg_gateway.library
VCS_OPTIONS += -libmap $(BSG_ASIC_LIB)
VCS_OPTIONS += -f $(BSG_ML605_DIR)/out/vcs/bsg_ml605.filelist
VCS_OPTIONS += -f $(BSG_GATEWAY_DIR)/out/vcs/bsg_gateway.filelist
VCS_OPTIONS += -v $(BSG_ASIC_NETLIST)

VCS_OPTIONS += -o $(BSG_OUT_SIM)

default: $(BSG_OUT_SIM)
	$<

setup: clean_all
	make -C $(BSG_ML605_DIR) setup
	make -C $(BSG_GATEWAY_DIR) setup

$(BSG_ASIC_NETLIST): setup
	make -C $(BSG_ASIC_DIR) syn

$(BSG_OUT_DIR): $(BSG_ASIC_NETLIST)
	mkdir -p $@

$(BSG_ASIC_LIB): $(BSG_OUT_DIR)
	@echo "library bsg_asic_post_synth" > $@
	@echo "$(BSG_ASIC_NETLIST);" >> $@

$(BSG_OUT_SIM): $(BSG_ASIC_LIB)
	$(VCS) $(VCS_OPTIONS) $(BSG_TESTING_FILES) $(BSG_ASIC_NETLIST)

clean:
	-rm -rf $(BSG_OUT_DIR) csrc ucli.key vcdplus.vpd

clean_all: clean
	make -C $(BSG_ML605_DIR) clean
	make -C $(BSG_GATEWAY_DIR) clean
	make -C $(BSG_ASIC_DIR) clean
