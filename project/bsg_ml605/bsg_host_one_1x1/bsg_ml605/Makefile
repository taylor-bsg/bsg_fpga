#------------------------------------------------------------
# University of California, San Diego - Bespoke Systems Group
#------------------------------------------------------------
# File: Makefile
#
# Author: Luis Vega - lvgutierrez@eng.ucsd.edu
#------------------------------------------------------------

# include licence and path variables
include ../../../Makefile.include

export BSG_TOP_NAME=bsg_ml605
export BSG_WORK_DIR=$(abspath .)
export BSG_FPGA_IP_DIR=$(abspath ../../../../ip)
export BSG_OUT_RAW_DIR=$(BSG_OUT_DIR)/raw
export BSG_OUT_SYN_DIR=$(BSG_OUT_DIR)/syn
export BSG_OUT_ISE_DIR=$(BSG_OUT_DIR)/ise
export BSG_OUT_V_DIR=$(BSG_OUT_DIR)/v
export BSG_ML605_ROM_NAME=bsg_ml605_chipset_rom
export BSG_ML605_ROM_V=$(BSG_OUT_V_DIR)/$(BSG_ML605_ROM_NAME).v

BSG_OUT_DIR = $(BSG_WORK_DIR)/out

# input files
SYNPLIFY_TCL = $(BSG_WORK_DIR)/src/tcl/synplify.tcl
XILINX_TCL = $(BSG_WORK_DIR)/src/tcl/xilinx.tcl

# raw
RAW_REPOSITORIES = crudo io_master rawboards

# output directories and files
SYNPLIFY_LOG = $(BSG_OUT_SYN_DIR)/synplify.log
XILINX_LOG = $(BSG_OUT_ISE_DIR)/xilinx.log

RAW_REPOSITORIES_DIR = $(patsubst %,$(BSG_OUT_RAW_DIR)/%,$(RAW_REPOSITORIES))
BIT_FILE = $(BSG_OUT_ISE_DIR)/$(BSG_TOP_NAME).bit
SYNPLICITY_UCF = $(BSG_OUT_SYN_DIR)/synplicity.ucf
SYN_TOP_NCF = $(BSG_OUT_SYN_DIR)/$(BSG_TOP_NAME).ncf
SYN_TOP_EDN = $(BSG_OUT_SYN_DIR)/$(BSG_TOP_NAME).edn

default:
	. $(XILINX_ISE_DS_DIR)/settings64.sh; make $(BIT_FILE)

# raw source code
$(RAW_REPOSITORIES_DIR):
	mkdir -p $(BSG_OUT_RAW_DIR)
	cd $(BSG_OUT_RAW_DIR) && cvs co $(RAW_REPOSITORIES)

# generate bsg_ml605_chipset_rom with the corresponding trace
$(BSG_ML605_ROM_V): $(RAW_REPOSITORIES_DIR)
	mkdir -p $(BSG_OUT_V_DIR)
	$(BSG_IP_CORES_DIR)/bsg_mem/bsg_ascii_to_rom.py \
	$(BSG_FPGA_IP_DIR)/bsg_ml605/bsg_ml605_chipset/trace/boot.trace \
	$(BSG_ML605_ROM_NAME) > $@

# synplify flow
$(SYN_TOP_EDN) $(SYN_TOP_NCF) $(SYNPLICITY_UCF): $(BSG_ML605_ROM_V)
	mkdir -p $(BSG_OUT_SYN_DIR)
	$(SYNPLIFY_2014_BIN) -batch \
	                      -licensetype synplifypremierdp \
			      -tcl $(SYNPLIFY_TCL) 2>&1 | tee $(SYNPLIFY_LOG)

# xilinx flow
$(BIT_FILE): $(SYN_TOP_EDN) $(SYN_TOP_NCF) $(SYNPLICITY_UCF)
	mkdir -p $(BSG_OUT_ISE_DIR)
	xtclsh $(XILINX_TCL) 2>&1 | tee $(XILINX_LOG)

clean:
	-rm -rf $(BSG_OUT_DIR) *.log synlog.tcl licbug.txt \
		xlnx_auto_0_xdb *.xrpt *.xml *.html _xmsgs \
		*.twr *.twx
