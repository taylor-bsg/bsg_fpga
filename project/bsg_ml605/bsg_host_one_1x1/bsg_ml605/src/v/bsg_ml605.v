//------------------------------------------------------------
// University of California, San Diego - Bespoke Systems Group
//------------------------------------------------------------
// File: bsg_ml605.v
//
// Author: Luis Vega - lvgutierrez@eng.ucsd.edu
//------------------------------------------------------------

`include "bsg_defines.v"

module bsg_ml605 #
  (parameter SIM_BYPASS_INIT_CAL = "OFF")
  (input SYSCLK_P, SYSCLK_N
  // led
  ,output GPIO_LED_0
  ,output GPIO_LED_1
  ,output GPIO_LED_2
  ,output GPIO_LED_3
  ,output GPIO_LED_4
  ,output GPIO_LED_5
  ,output GPIO_LED_6
  ,output GPIO_LED_7
  // pcie clk
  ,input PCIE_250M_MGT1_P, PCIE_250M_MGT1_N
  // pcie reset (active low)
  ,input PCIE_PERST_B_LS
  // pcie data in
  ,input PCIE_RX0_P, PCIE_RX0_N
  ,input PCIE_RX1_P, PCIE_RX1_N
  ,input PCIE_RX2_P, PCIE_RX2_N
  ,input PCIE_RX3_P, PCIE_RX3_N
  // pcie data out
  ,output PCIE_TX0_P, PCIE_TX0_N
  ,output PCIE_TX1_P, PCIE_TX1_N
  ,output PCIE_TX2_P, PCIE_TX2_N
  ,output PCIE_TX3_P, PCIE_TX3_N
  // dram
  ,inout [63:0] DDR3_DQ
  ,inout [7:0] DDR3_DQS_P
  ,inout [7:0] DDR3_DQS_N
  ,output DDR3_RESET_N
  ,output [0:0] DDR3_CK_P
  ,output [0:0] DDR3_CK_N
  ,output [0:0] DDR3_CKE
  ,output [0:0] DDR3_CS_N
  ,output DDR3_RAS_N
  ,output DDR3_CAS_N
  ,output DDR3_WE_N
  ,output [7:0] DDR3_DM
  ,output [2:0] DDR3_BA
  ,output [14:0] DDR3_ADDR
  ,output [0:0] DDR3_ODT
  // fmc
  // fmc gateway reset out
  ,output FMC_LPC_LA20_P, FMC_LPC_LA20_N
  // fmc ml605 reset in
  ,input FMC_LPC_LA23_P, FMC_LPC_LA23_N
  // fmc tx clk out
  ,output FMC_LPC_LA17_CC_P, FMC_LPC_LA17_CC_N
  // fmc tx data out
  ,output FMC_LPC_LA31_P, FMC_LPC_LA31_N
  ,output FMC_LPC_LA33_P, FMC_LPC_LA33_N
  ,output FMC_LPC_LA30_P, FMC_LPC_LA30_N
  ,output FMC_LPC_LA32_P, FMC_LPC_LA32_N
  ,output FMC_LPC_LA28_P, FMC_LPC_LA28_N
  ,output FMC_LPC_LA25_P, FMC_LPC_LA25_N
  ,output FMC_LPC_LA29_P, FMC_LPC_LA29_N
  ,output FMC_LPC_LA26_P, FMC_LPC_LA26_N
  ,output FMC_LPC_LA21_P, FMC_LPC_LA21_N
  ,output FMC_LPC_LA27_P, FMC_LPC_LA27_N
  ,output FMC_LPC_LA22_P, FMC_LPC_LA22_N
  // fmc rx clk out
  ,output FMC_LPC_CLK0_M2C_P, FMC_LPC_CLK0_M2C_N
  // fmc rx clk in
  ,input FMC_LPC_CLK1_M2C_P, FMC_LPC_CLK1_M2C_N
  // fmc rx data in
  ,input FMC_LPC_LA00_CC_P, FMC_LPC_LA00_CC_N
  ,input FMC_LPC_LA16_P, FMC_LPC_LA16_N
  ,input FMC_LPC_LA15_P, FMC_LPC_LA15_N
  ,input FMC_LPC_LA13_P, FMC_LPC_LA13_N
  ,input FMC_LPC_LA11_P, FMC_LPC_LA11_N
  ,input FMC_LPC_LA10_P, FMC_LPC_LA10_N
  ,input FMC_LPC_LA14_P, FMC_LPC_LA14_N
  ,input FMC_LPC_LA09_P, FMC_LPC_LA09_N
  ,input FMC_LPC_LA04_P, FMC_LPC_LA04_N
  ,input FMC_LPC_LA07_P, FMC_LPC_LA07_N
  ,input FMC_LPC_LA08_P, FMC_LPC_LA08_N);

  // mmcm

  logic clk_50_mhz_lo;
  logic clk_200_mhz_lo;

  bsg_ml605_mmcm mmcm_inst
    (.clk_200_mhz_p_i(SYSCLK_P) ,.clk_200_mhz_n_i(SYSCLK_N)
    ,.clk_50_mhz_o(clk_50_mhz_lo)
    ,.clk_200_mhz_o(clk_200_mhz_lo)
    ,.locked_o());

  assign clk_o = clk_50_mhz_lo;

  // pcie

  logic boot_done_lo;
  logic dram_phy_init_done_lo;
  logic pcie_reset_lo;

  // recore software code waits until
  // pcie-status-register becomes 0x0000ffff
  // for test to begin

  logic [31:0] pcie_status_r;

  always_ff @(posedge clk_50_mhz_lo)
    if (pcie_reset_lo == 1'b1)
      pcie_status_r <= 32'h00000000;
    else
      pcie_status_r <= {16'h0000
                       ,{14{1'b1}}
                       ,boot_done_lo
                       ,dram_phy_init_done_lo};

  localparam channel_lp = 6;

  logic [channel_lp - 1 : 0] cs_valid_lo;
  logic [31:0] cs_data_lo [channel_lp - 1 : 0];
  logic [channel_lp - 1 : 0] pcie_ready_lo;

  logic [channel_lp - 1 : 0] pcie_valid_lo;
  logic [31:0] pcie_data_lo [channel_lp - 1 : 0];
  logic [channel_lp - 1 : 0] cs_yumi_lo;

  bsg_ml605_pcie #
    (.channel_p(channel_lp))
  pcie_inst
    // clk
    (.clk_i(clk_50_mhz_lo)
    // ctrl
    ,.reset_o(pcie_reset_lo)
    // status register
    ,.status_register_i(pcie_status_r)
    // data in
    ,.valid_i(cs_valid_lo)
    ,.data_i(cs_data_lo)
    ,.ready_o(pcie_ready_lo)
    // data out
    ,.valid_o(pcie_valid_lo)
    ,.data_o(pcie_data_lo)
    ,.yumi_i(cs_yumi_lo)
    // pcie clk
    ,.PCIE_250M_MGT1_P(PCIE_250M_MGT1_P) ,.PCIE_250M_MGT1_N(PCIE_250M_MGT1_N)
    // pcie reset
    ,.PCIE_PERST_B_LS(PCIE_PERST_B_LS)
    // pcie data in
    ,.PCIE_RX0_P(PCIE_RX0_P) ,.PCIE_RX0_N(PCIE_RX0_N)
    ,.PCIE_RX1_P(PCIE_RX1_P) ,.PCIE_RX1_N(PCIE_RX1_N)
    ,.PCIE_RX2_P(PCIE_RX2_P) ,.PCIE_RX2_N(PCIE_RX2_N)
    ,.PCIE_RX3_P(PCIE_RX3_P) ,.PCIE_RX3_N(PCIE_RX3_N)
    // pcie data out
    ,.PCIE_TX0_P(PCIE_TX0_P) ,.PCIE_TX0_N(PCIE_TX0_N)
    ,.PCIE_TX1_P(PCIE_TX1_P) ,.PCIE_TX1_N(PCIE_TX1_N)
    ,.PCIE_TX2_P(PCIE_TX2_P) ,.PCIE_TX2_N(PCIE_TX2_N)
    ,.PCIE_TX3_P(PCIE_TX3_P) ,.PCIE_TX3_N(PCIE_TX3_N));

  // chipset

  logic uncore_reset_lo;
  logic fmc_ml605_reset_lo;

  logic dram_valid_lo;
  logic [31:0] dram_data_lo;
  logic cs_dram_thanks_lo;

  logic cs_dram_valid_lo;
  logic [31:0] cs_dram_data_lo;
  logic dram_thanks_lo;

  logic fmc_valid_lo;
  logic [79:0] fmc_data_lo;
  logic cs_fmc_ready_lo;

  logic cs_fmc_valid_lo;
  logic [79:0] cs_fmc_data_lo;
  logic fmc_ready_lo;

  bsg_ml605_chipset cs_inst
    (.clk_i(clk_50_mhz_lo)
    // resets
    ,.reset_i(fmc_ml605_reset_lo)
    ,.uncore_reset_o(uncore_reset_lo)
    // ctrl
    ,.boot_done_o(boot_done_lo)
    // pcie in
    ,.pcie_valid_i(pcie_valid_lo)
    ,.pcie_data_i(pcie_data_lo)
    ,.pcie_yumi_o(cs_yumi_lo)
    // pcie out
    ,.pcie_valid_o(cs_valid_lo)
    ,.pcie_data_o(cs_data_lo)
    ,.pcie_ready_i(pcie_ready_lo)
    // dram in
    ,.dram_valid_i(dram_valid_lo)
    ,.dram_data_i(dram_data_lo)
    ,.dram_thanks_o(cs_dram_thanks_lo)
    // dram out
    ,.dram_valid_o(cs_dram_valid_lo)
    ,.dram_data_o(cs_dram_data_lo)
    ,.dram_thanks_i(dram_thanks_lo)
    // fmc in
    ,.fmc_valid_i(fmc_valid_lo)
    ,.fmc_data_i(fmc_data_lo)
    ,.fmc_ready_o(cs_fmc_ready_lo)
    // fmc out
    ,.fmc_valid_o(cs_fmc_valid_lo)
    ,.fmc_data_o(cs_fmc_data_lo)
    ,.fmc_ready_i(fmc_ready_lo));

  // dram

  logic dram_pll_lock_lo;

  bsg_ml605_dram #
    (.SIM_BYPASS_INIT_CAL(SIM_BYPASS_INIT_CAL))
  dram_inst
    (.clk_i(clk_50_mhz_lo)
    ,.clk_200_mhz_i(clk_200_mhz_lo)
    // reset
    ,.reset_i(uncore_reset_lo)
    // phy
    ,.phy_init_done_o(dram_phy_init_done_lo)
    ,.pll_lock_o(dram_pll_lock_lo)
    // in
    ,.valid_i(cs_dram_valid_lo)
    ,.data_i(cs_dram_data_lo)
    ,.thanks_o(dram_thanks_lo)
    // out
    ,.valid_o(dram_valid_lo)
    ,.data_o(dram_data_lo)
    ,.thanks_i(cs_dram_thanks_lo)
    // ddr3
    ,.DDR3_CK_P(DDR3_CK_P)
    ,.DDR3_CK_N(DDR3_CK_N)
    ,.DDR3_ADDR(DDR3_ADDR)
    ,.DDR3_BA(DDR3_BA)
    ,.DDR3_RAS_N(DDR3_RAS_N)
    ,.DDR3_CAS_N(DDR3_CAS_N)
    ,.DDR3_WE_N(DDR3_WE_N)
    ,.DDR3_CS_N(DDR3_CS_N)
    ,.DDR3_CKE(DDR3_CKE)
    ,.DDR3_ODT(DDR3_ODT)
    ,.DDR3_RESET_N(DDR3_RESET_N)
    ,.DDR3_DM(DDR3_DM)
    ,.DDR3_DQ(DDR3_DQ)
    ,.DDR3_DQS_P(DDR3_DQS_P)
    ,.DDR3_DQS_N(DDR3_DQS_N));

  // fmc

  bsg_ml605_fmc fmc_inst
    (.clk_i(clk_50_mhz_lo)
    // data in
    ,.valid_i(cs_fmc_valid_lo)
    ,.data_i(cs_fmc_data_lo)
    ,.ready_o(fmc_ready_lo)
    // data out
    ,.valid_o(fmc_valid_lo)
    ,.data_o(fmc_data_lo)
    ,.ready_i(cs_fmc_ready_lo)
    // board reset in
    ,.board_reset_i(pcie_reset_lo)
    // ml605 reset out
    ,.ml605_reset_o(fmc_ml605_reset_lo)
    // fmc clk for ml605 and gateway
    ,.fmc_clk_i(clk_200_mhz_lo)
    ,.fmc_clk_div_i(clk_50_mhz_lo)
    ,.fmc_clk_200_mhz_i(clk_200_mhz_lo)
    // fmc gateway reset out
    ,.FMC_LPC_LA20_P(FMC_LPC_LA20_P) ,.FMC_LPC_LA20_N(FMC_LPC_LA20_N)
    // fmc ml605 reset in
    ,.FMC_LPC_LA23_P(FMC_LPC_LA23_P) ,.FMC_LPC_LA23_N(FMC_LPC_LA23_N)
    // fmc tx clk out
    ,.FMC_LPC_LA17_CC_P(FMC_LPC_LA17_CC_P) ,.FMC_LPC_LA17_CC_N(FMC_LPC_LA17_CC_N)
    // fmc tx data out
    ,.FMC_LPC_LA31_P(FMC_LPC_LA31_P) ,.FMC_LPC_LA31_N(FMC_LPC_LA31_N)
    ,.FMC_LPC_LA33_P(FMC_LPC_LA33_P) ,.FMC_LPC_LA33_N(FMC_LPC_LA33_N)
    ,.FMC_LPC_LA30_P(FMC_LPC_LA30_P) ,.FMC_LPC_LA30_N(FMC_LPC_LA30_N)
    ,.FMC_LPC_LA32_P(FMC_LPC_LA32_P) ,.FMC_LPC_LA32_N(FMC_LPC_LA32_N)
    ,.FMC_LPC_LA28_P(FMC_LPC_LA28_P) ,.FMC_LPC_LA28_N(FMC_LPC_LA28_N)
    ,.FMC_LPC_LA25_P(FMC_LPC_LA25_P) ,.FMC_LPC_LA25_N(FMC_LPC_LA25_N)
    ,.FMC_LPC_LA29_P(FMC_LPC_LA29_P) ,.FMC_LPC_LA29_N(FMC_LPC_LA29_N)
    ,.FMC_LPC_LA26_P(FMC_LPC_LA26_P) ,.FMC_LPC_LA26_N(FMC_LPC_LA26_N)
    ,.FMC_LPC_LA21_P(FMC_LPC_LA21_P) ,.FMC_LPC_LA21_N(FMC_LPC_LA21_N)
    ,.FMC_LPC_LA27_P(FMC_LPC_LA27_P) ,.FMC_LPC_LA27_N(FMC_LPC_LA27_N)
    ,.FMC_LPC_LA22_P(FMC_LPC_LA22_P) ,.FMC_LPC_LA22_N(FMC_LPC_LA22_N)
    // fmc rx clk out
    ,.FMC_LPC_CLK0_M2C_P(FMC_LPC_CLK0_M2C_P) ,.FMC_LPC_CLK0_M2C_N(FMC_LPC_CLK0_M2C_N)
    // fmc rx clk in
    ,.FMC_LPC_CLK1_M2C_P(FMC_LPC_CLK1_M2C_P) ,.FMC_LPC_CLK1_M2C_N(FMC_LPC_CLK1_M2C_N)
    // fmc rx data in
    ,.FMC_LPC_LA00_CC_P(FMC_LPC_LA00_CC_P) ,.FMC_LPC_LA00_CC_N(FMC_LPC_LA00_CC_N)
    ,.FMC_LPC_LA16_P(FMC_LPC_LA16_P) ,.FMC_LPC_LA16_N(FMC_LPC_LA16_N)
    ,.FMC_LPC_LA15_P(FMC_LPC_LA15_P) ,.FMC_LPC_LA15_N(FMC_LPC_LA15_N)
    ,.FMC_LPC_LA13_P(FMC_LPC_LA13_P) ,.FMC_LPC_LA13_N(FMC_LPC_LA13_N)
    ,.FMC_LPC_LA11_P(FMC_LPC_LA11_P) ,.FMC_LPC_LA11_N(FMC_LPC_LA11_N)
    ,.FMC_LPC_LA10_P(FMC_LPC_LA10_P) ,.FMC_LPC_LA10_N(FMC_LPC_LA10_N)
    ,.FMC_LPC_LA14_P(FMC_LPC_LA14_P) ,.FMC_LPC_LA14_N(FMC_LPC_LA14_N)
    ,.FMC_LPC_LA09_P(FMC_LPC_LA09_P) ,.FMC_LPC_LA09_N(FMC_LPC_LA09_N)
    ,.FMC_LPC_LA04_P(FMC_LPC_LA04_P) ,.FMC_LPC_LA04_N(FMC_LPC_LA04_N)
    ,.FMC_LPC_LA07_P(FMC_LPC_LA07_P) ,.FMC_LPC_LA07_N(FMC_LPC_LA07_N)
    ,.FMC_LPC_LA08_P(FMC_LPC_LA08_P) ,.FMC_LPC_LA08_N(FMC_LPC_LA08_N));

  // led

  assign {GPIO_LED_7
         ,GPIO_LED_6
         ,GPIO_LED_5
         ,GPIO_LED_4
         ,GPIO_LED_3
         ,GPIO_LED_2
         ,GPIO_LED_1
         ,GPIO_LED_0} = ( pcie_reset_lo == 1'b0
                       && fmc_ml605_reset_lo == 1'b0
                       && dram_phy_init_done_lo == 1'b1
                       && dram_pll_lock_lo == 1'b1
                       && boot_done_lo == 1'b1)? 8'hAA : 8'hFF;

endmodule
