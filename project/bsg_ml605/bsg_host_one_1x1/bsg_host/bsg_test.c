//------------------------------------------------------------
// University of California, San Diego - Bespoke Systems Group
//------------------------------------------------------------
// File: bsg_test.c
//
// This small program allows you to test this system without
// the whole complexity of the raw-software-stack. It reads
// a binary trace taken from the PLI in the raw-infrastructure
//
// This trace correspond to the program located in
// greenlight/module_tests/alu/arithmetic. Specifically, the
// following commit, date, and author:
//
// revision 1.12 date: 2003/01/25 01:50:57;  author: walt;
//
// If this test doesn't complete successfully, then spec2000
// benchmark will certainly fail
//
// Authors: Luis Vega - lvgutierrez@eng.ucsd.edu
//------------------------------------------------------------

#include "stdio.h"
#include "stdlib.h"

#include "bsg_ml605_pcie.h"

int main() {

  // pcie device

  bsg_ml605_pcie_device pcie;

  // status register

  unsigned int status_register_value;

  status_register_value = pcie.get_status_register_value();

  // status register stores 0x0000ffff when GATEWAY and ASIC link is up

  while(1) {
      if(status_register_value != 0x0000ffff)
          sleep(1);
      else
          break;
  }

  printf("\n[STATUS]: 0x%08x\n\n", status_register_value);

  // vta-pcie mapping:
  //
  // pcie[3] = south[0]
  // pcie[5] = test-network

  unsigned int check[14] = {0x4000004d, 0x7fffffff, 0x400007a6, 0x40000001
                           ,0x40000002, 0x40000003, 0x40000004, 0x40000005
                           ,0x40000006, 0x40000007, 0x40000008, 0x40000009
                           ,0x4000000a, 0x80000001};

  static const char filename[] = "greenlight_alu_arithmetic.trace";
  FILE *file = fopen (filename, "r");

  if (file != NULL) {

      char line[34];

      // write
      while (fgets(line, sizeof line, file) != NULL) {
        pcie.write_packet_async_if_available(3, (int) strtol(line, NULL, 2));
      }

      fclose(file);

      unsigned int data;

      // read
      for (int i=0; i < 14; i++) {
          pcie.read_packet_blocking(5, data);
          if (data == check[i]) {
              printf("[PASS]: 0x%08x\n", data);
          }
          else {
              printf("[FAIL]: received 0x%08x, expected 0x%08x\n", data, check[i]);
          }
      }
  }
  else {
      perror(filename);
  }

}
