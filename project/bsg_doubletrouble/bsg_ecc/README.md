Project description
===================

bsg_ecc, this name will be changed later

How to add SVerilog to every device
===================================
There are two file list for every device, i.e.:

* `bsg_asic/src/tcl/filelist.tcl` (file list for synth)
* `bsg_asic/src/tcl/include.tcl` (include file list needed by filelist.tcl)

How to simulate
===============
Simulation scripts will use filelist.tcl and include.tcl for setting up
simulation, therefore no need for adding new files

* `make rtl_sim` for rtl simulation

How to create bitstreams
========================
The current project, as it is today August 22th 2016, does not have any
synthizable code, therefore do not expect useful bitstreams

* Build all bitstream by:
    * `make`

Where are the bitstreams
========================

* After successfully building each bitstream, there should be located at:
    * `bsg_fpga/project/bsg_ml605/bsg_one_loopback/bsg_gateway/out/bsg_gateway_elf.bit`
    * `bsg_fpga/project/bsg_ml605/bsg_one_loopback/bsg_asic/out/ise/bsg_asic.bit`

How to program
==============

* You will need:
    * The Xilinx ML605 board
    * The DoubleTrouble board
    * Xilinx Platform Cable USB II
    * Power supply (12V) with molex connector
    * Xilinx Impact
