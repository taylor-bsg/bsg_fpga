//------------------------------------------------------------
// University of California, San Diego - Bespoke Systems Group
//------------------------------------------------------------
// File: test_bsg.v
//
// Author: Luis Vega - lvgutierrez@eng.ucsd.edu
//------------------------------------------------------------

module test_bsg;

  logic CLK_OSC_P, CLK_OSC_N;

  initial CLK_OSC_P = 1'b0;

  always #(3_335) CLK_OSC_P = ~CLK_OSC_P;

  assign CLK_OSC_N = ~CLK_OSC_P;

  logic PWR_RSTN;

  initial begin
    PWR_RSTN = 1'b1;
    #100_000;
    PWR_RSTN = 1'b0;
    $display("TIME:%08d PWR_RSTN LOW", $time);
    #150_000;
    PWR_RSTN = 1'b1;
    $display("TIME:%08d PWR_RSTN HIGH", $time);
  end

  initial begin
    #1_000_000;
    $finish;
  end


  logic FPGA_LED2, FPGA_LED3;

  // asic clk in
  logic CLK0;
  logic MSTR_SDO_CLK;

  // asic reset in
  logic AID10;

  // asic channel clk out
  logic AOC0, BOC0, COC0, DOC0;
  // asic channel data out
  //      A     B     C     D
  logic AOD0, BOD0, COD0, DOD0;
  logic AOD1, BOD1, COD1, DOD1;
  logic AOD2, BOD2, COD2, DOD2;
  logic AOD3, BOD3, COD3, DOD3;
  logic AOD4, BOD4, COD4, DOD4;
  logic AOD5, BOD5, COD5, DOD5;
  logic AOD6, BOD6, COD6, DOD6;
  logic AOD7, BOD7, COD7, DOD7;
  logic AOD8, BOD8, COD8, DOD8;
  logic AOD9, BOD9, COD9, DOD9;
  // asic channel token in
  logic AOT0, BOT0, COT0, DOT0;

  // asic channel clk in
  logic AIC0, BIC0, CIC0, DIC0;
  // asic channel data in
  //     A     B     C     D
  logic AID0, BID0, CID0, DID0;
  logic AID1, BID1, CID1, DID1;
  logic AID2, BID2, CID2, DID2;
  logic AID3, BID3, CID3, DID3;
  logic AID4, BID4, CID4, DID4;
  logic AID5, BID5, CID5, DID5;
  logic AID6, BID6, CID6, DID6;
  logic AID7, BID7, CID7, DID7;
  logic AID8, BID8, CID8, DID8;
  logic AID9, BID9, CID9, DID9;
  // asic channel token out
  logic AIT0, BIT0, CIT0, DIT0;

  bsg_gateway gateway
    (.CLK_OSC_P(CLK_OSC_P) ,.CLK_OSC_N(CLK_OSC_N)
    ,.PWR_RSTN(PWR_RSTN)
    ,.FPGA_LED2(FPGA_LED2) ,.FPGA_LED3(FPGA_LED3)

    // --------------------- ASIC ------------------------

    // clk
    ,.CLK0(CLK0)
    ,.MSTR_SDO_CLK(MSTR_SDO_CLK)
    // asic reset out
    ,.AID10(AID10)

    // channel in

    // channel clk in
    ,.AOC0(AOC0)  ,.BOC0(BOC0) ,.COC0(COC0) ,.DOC0(DOC0)
    // channel data in
    //    A            B            C            D
    ,.AOD0(AOD0) ,.BOD0(BOD0) ,.COD0(COD0) ,.DOD0(DOD0)
    ,.AOD1(AOD1) ,.BOD1(BOD1) ,.COD1(COD1) ,.DOD1(DOD1)
    ,.AOD2(AOD2) ,.BOD2(BOD2) ,.COD2(COD2) ,.DOD2(DOD2)
    ,.AOD3(AOD3) ,.BOD3(BOD3) ,.COD3(COD3) ,.DOD3(DOD3)
    ,.AOD4(AOD4) ,.BOD4(BOD4) ,.COD4(COD4) ,.DOD4(DOD4)
    ,.AOD5(AOD5) ,.BOD5(BOD5) ,.COD5(COD5) ,.DOD5(DOD5)
    ,.AOD6(AOD6) ,.BOD6(BOD6) ,.COD6(COD6) ,.DOD6(DOD6)
    ,.AOD7(AOD7) ,.BOD7(BOD7) ,.COD7(COD7) ,.DOD7(DOD7)
    ,.AOD8(AOD8) ,.BOD8(BOD8) ,.COD8(COD8) ,.DOD8(DOD8)
    ,.AOD9(AOD9) ,.BOD9(BOD9) ,.COD9(COD9) ,.DOD9(DOD9)
    // channel token out
    ,.AOT0(AOT0) ,.BOT0(BOT0) ,.COT0(COT0) ,.DOT0(DOT0)

    // channel out

    // channel clk out
    ,.AIC0(AIC0) ,.BIC0(BIC0) ,.CIC0(CIC0) ,.DIC0(DIC0)
    // channel data out
    //    A            B            C            D
    ,.AID0(AID0) ,.BID0(BID0) ,.CID0(CID0) ,.DID0(DID0)
    ,.AID1(AID1) ,.BID1(BID1) ,.CID1(CID1) ,.DID1(DID1)
    ,.AID2(AID2) ,.BID2(BID2) ,.CID2(CID2) ,.DID2(DID2)
    ,.AID3(AID3) ,.BID3(BID3) ,.CID3(CID3) ,.DID3(DID3)
    ,.AID4(AID4) ,.BID4(BID4) ,.CID4(CID4) ,.DID4(DID4)
    ,.AID5(AID5) ,.BID5(BID5) ,.CID5(CID5) ,.DID5(DID5)
    ,.AID6(AID6) ,.BID6(BID6) ,.CID6(CID6) ,.DID6(DID6)
    ,.AID7(AID7) ,.BID7(BID7) ,.CID7(CID7) ,.DID7(DID7)
    ,.AID8(AID8) ,.BID8(BID8) ,.CID8(CID8) ,.DID8(DID8)
    ,.AID9(AID9) ,.BID9(BID9) ,.CID9(CID9) ,.DID9(DID9)
    // channel token in
    ,.AIT0(AIT0) ,.BIT0(BIT0) ,.CIT0(CIT0) ,.DIT0(DIT0));


  logic ASIC_LED0, ASIC_LED1;

  bsg_asic asic
    // --------------------- GATEWAY ------------------------
    // clk
    (.CLK0(CLK0)
    ,.MSTR_SDO_CLK(MSTR_SDO_CLK)
    // reset from asic
    ,.AID10(AID10)
    // led
    ,.ASIC_LED0(ASIC_LED0) ,.ASIC_LED1(ASIC_LED1)

    // channel out

    // channel clk out
    ,.AOC0(AOC0)  ,.BOC0(BOC0) ,.COC0(COC0) ,.DOC0(DOC0)
    // channel data out
    //    A            B            C            D
    ,.AOD0(AOD0) ,.BOD0(BOD0) ,.COD0(COD0) ,.DOD0(DOD0)
    ,.AOD1(AOD1) ,.BOD1(BOD1) ,.COD1(COD1) ,.DOD1(DOD1)
    ,.AOD2(AOD2) ,.BOD2(BOD2) ,.COD2(COD2) ,.DOD2(DOD2)
    ,.AOD3(AOD3) ,.BOD3(BOD3) ,.COD3(COD3) ,.DOD3(DOD3)
    ,.AOD4(AOD4) ,.BOD4(BOD4) ,.COD4(COD4) ,.DOD4(DOD4)
    ,.AOD5(AOD5) ,.BOD5(BOD5) ,.COD5(COD5) ,.DOD5(DOD5)
    ,.AOD6(AOD6) ,.BOD6(BOD6) ,.COD6(COD6) ,.DOD6(DOD6)
    ,.AOD7(AOD7) ,.BOD7(BOD7) ,.COD7(COD7) ,.DOD7(DOD7)
    ,.AOD8(AOD8) ,.BOD8(BOD8) ,.COD8(COD8) ,.DOD8(DOD8)
    ,.AOD9(AOD9) ,.BOD9(BOD9) ,.COD9(COD9) ,.DOD9(DOD9)
    // channel token in
    ,.AOT0(AOT0) ,.BOT0(BOT0) ,.COT0(COT0) ,.DOT0(DOT0)

    // channel in

    // channel clk in
    ,.AIC0(AIC0) ,.BIC0(BIC0) ,.CIC0(CIC0) ,.DIC0(DIC0)
    // channel data in
    //    A            B            C            D
    ,.AID0(AID0) ,.BID0(BID0) ,.CID0(CID0) ,.DID0(DID0)
    ,.AID1(AID1) ,.BID1(BID1) ,.CID1(CID1) ,.DID1(DID1)
    ,.AID2(AID2) ,.BID2(BID2) ,.CID2(CID2) ,.DID2(DID2)
    ,.AID3(AID3) ,.BID3(BID3) ,.CID3(CID3) ,.DID3(DID3)
    ,.AID4(AID4) ,.BID4(BID4) ,.CID4(CID4) ,.DID4(DID4)
    ,.AID5(AID5) ,.BID5(BID5) ,.CID5(CID5) ,.DID5(DID5)
    ,.AID6(AID6) ,.BID6(BID6) ,.CID6(CID6) ,.DID6(DID6)
    ,.AID7(AID7) ,.BID7(BID7) ,.CID7(CID7) ,.DID7(DID7)
    ,.AID8(AID8) ,.BID8(BID8) ,.CID8(CID8) ,.DID8(DID8)
    ,.AID9(AID9) ,.BID9(BID9) ,.CID9(CID9) ,.DID9(DID9)
    // channel token out
    ,.AIT0(AIT0) ,.BIT0(BIT0) ,.CIT0(CIT0) ,.DIT0(DIT0));

endmodule

config test_bsg_rtl;
  design test_bsg;
  instance test_bsg.gateway liblist bsg_gateway;
  instance test_bsg.asic liblist bsg_asic;
endconfig

config test_bsg_gate;
  design test_bsg;
  instance test_bsg.gateway liblist bsg_gateway_gate;
  instance test_bsg.asic liblist bsg_asic_gate;
endconfig
