//------------------------------------------------------------
// University of California, San Diego - Bespoke Systems Group
//------------------------------------------------------------
// File: bsg_asic.v
//
// Author: Luis Vega - lvgutierrez@eng.ucsd.edu
//------------------------------------------------------------

module bsg_asic
  // clk
  (input CLK0
  ,input MSTR_SDO_CLK

  // reset
  ,input AID10

  // led
  ,output ASIC_LED0, ASIC_LED1

  //-------- GATEWAY --------

  // channel out

  // channel clk out
  ,output AOC0, BOC0, COC0, DOC0
  // channel data out
  //       A     B     C     D
  ,output AOD0, BOD0, COD0, DOD0
  ,output AOD1, BOD1, COD1, DOD1
  ,output AOD2, BOD2, COD2, DOD2
  ,output AOD3, BOD3, COD3, DOD3
  ,output AOD4, BOD4, COD4, DOD4
  ,output AOD5, BOD5, COD5, DOD5
  ,output AOD6, BOD6, COD6, DOD6
  ,output AOD7, BOD7, COD7, DOD7
  ,output AOD8, BOD8, COD8, DOD8
  ,output AOD9, BOD9, COD9, DOD9
  // channel token in
  ,input AOT0, BOT0, COT0, DOT0

  // channel in

  // channel clk in
  ,input AIC0, BIC0, CIC0, DIC0
  // channel data in
  //      A     B     C     D
  ,input AID0, BID0, CID0, DID0
  ,input AID1, BID1, CID1, DID1
  ,input AID2, BID2, CID2, DID2
  ,input AID3, BID3, CID3, DID3
  ,input AID4, BID4, CID4, DID4
  ,input AID5, BID5, CID5, DID5
  ,input AID6, BID6, CID6, DID6
  ,input AID7, BID7, CID7, DID7
  ,input AID8, BID8, CID8, DID8
  ,input AID9, BID9, CID9, DID9
  // channel token out
  ,output AIT0,  BIT0, CIT0, DIT0);

  // clock

  logic core_clk_lo, io_master_clk_lo;

  bsg_asic_clk clk
    (.core_clk_i(CLK0)
    ,.io_clk_i(MSTR_SDO_CLK)
    ,.core_clk_o(core_clk_lo)
    ,.io_clk_o(io_master_clk_lo));

  // reset

  logic reset_lo;

  assign reset_lo = AID10;

  // io

  logic [3:0] io_clk_li;
  logic [9:0] io_data_li [3:0];
  logic [3:0] io_token_lo;

  logic [3:0] im_clk_lo;
  logic [9:0] im_data_lo [3:0];
  logic [3:0] token_clk_li;

  // channel in

  assign io_clk_li = {DIC0, CIC0, BIC0, AIC0};

  assign io_data_li = {{DID9, DID8, DID7, DID6, DID5, DID4, DID3, DID2, DID1, DID0}
                      ,{CID9, CID8, CID7, CID6, CID5, CID4, CID3, CID2, CID1, CID0}
                      ,{BID9, BID8, BID7, BID6, BID5, BID4, BID3, BID2, BID1, BID0}
                      ,{AID9, AID8, AID7, AID6, AID5, AID4, AID3, AID2, AID1, AID0}};

  assign {DIT0, CIT0, BIT0, AIT0} = io_token_lo;

  // channel out

  assign {DOC0, COC0, BOC0, AOC0} = im_clk_lo;

  assign {DOD9, DOD8, DOD7, DOD6, DOD5, DOD4, DOD3, DOD2, DOD1, DOD0} = im_data_lo[3];
  assign {COD9, COD8, COD7, COD6, COD5, COD4, COD3, COD2, COD1, COD0} = im_data_lo[2];
  assign {BOD9, BOD8, BOD7, BOD6, BOD5, BOD4, BOD3, BOD2, BOD1, BOD0} = im_data_lo[1];
  assign {AOD9, AOD8, AOD7, AOD6, AOD5, AOD4, AOD3, AOD2, AOD1, AOD0} = im_data_lo[0];

  assign token_clk_li = {DOT0, COT0, BOT0, AOT0};

  // to gateway

  logic clk_r;

  always_ff @(negedge io_master_clk_lo)
    if (reset_lo)
      clk_r <= 1'b0;
    else
      clk_r <= ~clk_r;

  assign im_clk_lo = {4{clk_r}};

  logic [9:0] cnt_r;

  always_ff @(posedge io_master_clk_lo)
    if (reset_lo)
      cnt_r <= 10'd0;
    else
      cnt_r <= cnt_r + 1;

  assign im_data_lo = {cnt_r, cnt_r, cnt_r, cnt_r};

  // from gateway (non-synthesizable)

  genvar i;
  for (i = 0; i < 4; i++) begin

    always @(posedge io_clk_li[i])
      $display("asic[%1d], data_in[posedge]:%03x", i, io_data_li[i]);

    always @(negedge io_clk_li[i])
      $display("asic[%1d], data_in[negedge]:%03x", i, io_data_li[i]);

  end

  // led
  assign ASIC_LED0 = reset_lo;
  assign ASIC_LED1 = reset_lo;

endmodule
