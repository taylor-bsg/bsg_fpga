#------------------------------------------------------------
# University of California, San Diego - Bespoke Systems Group
#------------------------------------------------------------
# File: filelist.tcl
#
# Author: Luis Vega - lvgutierrez@eng.ucsd.edu
#------------------------------------------------------------

set SVERILOG_SOURCE_FILES [join "
  $bsg_src_dir/v/bsg_asic.v
  $bsg_src_dir/v/bsg_asic_clk.v
"]
