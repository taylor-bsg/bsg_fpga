//------------------------------------------------------------
// University of California, San Diego - Bespoke Systems Group
//------------------------------------------------------------
// File: bsg_gateway.v
//
// Author: Luis Vega - lvgutierrez@eng.ucsd.edu
//------------------------------------------------------------

module bsg_gateway
  // clk osc
  (input CLK_OSC_P, CLK_OSC_N
  // reset
  ,input PWR_RSTN
`ifdef MICROBLAZE
  // voltage-rail enable
  ,output logic ASIC_CORE_EN, ASIC_IO_EN
  // current monitor
  ,output logic CUR_MON_ADDR0, CUR_MON_ADDR1
  ,inout CUR_MON_SCL, CUR_MON_SDA
  // potentiometer
  ,output logic DIG_POT_ADDR0, DIG_POT_ADDR1
  ,output logic DIG_POT_INDEP, DIG_POT_NRST
  ,inout DIG_POT_SCL, DIG_POT_SDA
  // uart
  ,input UART_RX
  ,output UART_TX
  // led
  ,output logic FPGA_LED0, FPGA_LED1
`endif
  ,output FPGA_LED2, FPGA_LED3

  // -------- ASIC --------

  // clk
  ,output CLK0
  ,output MSTR_SDO_CLK

  // asic reset
  ,output AID10

  // channel in

  // channel clk in
  ,input AOC0, BOC0, COC0, DOC0
  // channel data in
  //      A     B     C     D
  ,input AOD0, BOD0, COD0, DOD0
  ,input AOD1, BOD1, COD1, DOD1
  ,input AOD2, BOD2, COD2, DOD2
  ,input AOD3, BOD3, COD3, DOD3
  ,input AOD4, BOD4, COD4, DOD4
  ,input AOD5, BOD5, COD5, DOD5
  ,input AOD6, BOD6, COD6, DOD6
  ,input AOD7, BOD7, COD7, DOD7
  ,input AOD8, BOD8, COD8, DOD8
  ,input AOD9, BOD9, COD9, DOD9
  // channel token out
  ,output AOT0, BOT0, COT0, DOT0

  // channel out

  // channel clk out
  ,output AIC0, BIC0, CIC0, DIC0
  // channel data out
  //       A     B     C     D
  ,output AID0, BID0, CID0, DID0
  ,output AID1, BID1, CID1, DID1
  ,output AID2, BID2, CID2, DID2
  ,output AID3, BID3, CID3, DID3
  ,output AID4, BID4, CID4, DID4
  ,output AID5, BID5, CID5, DID5
  ,output AID6, BID6, CID6, DID6
  ,output AID7, BID7, CID7, DID7
  ,output AID8, BID8, CID8, DID8
  ,output AID9, BID9, CID9, DID9
  // channel token in
  ,input AIT0, BIT0, CIT0, DIT0);

  // clock generation

  logic mb_clk_lo;
  logic core_clk_lo;
  logic io_master_clk_lo;
  logic locked_lo;

  bsg_gateway_clk clk
    (.clk_150_mhz_p_i(CLK_OSC_P) ,.clk_150_mhz_n_i(CLK_OSC_N)
    // microblaze clock
    ,.mb_clk_o(mb_clk_lo)
    // internal clocks
    ,.int_core_clk_o(core_clk_lo)
    ,.int_io_master_clk_o(io_master_clk_lo)
    // external clocks
    ,.ext_core_clk_o(CLK0)
    ,.ext_io_master_clk_o(MSTR_SDO_CLK)
    // locked
    ,.locked_o(locked_lo));

`ifdef MICROBLAZE

  // power control

  logic [11:0] gpio;
  logic cpu_override_output_p;
  logic cpu_override_output_n;

  assign cpu_override_output_p = gpio[11];
  assign cpu_override_output_n = gpio[10];

  always_comb begin
    FPGA_LED0 = 1'b1;
    FPGA_LED1 = 1'b1;
    DIG_POT_INDEP = 1'b1;
    DIG_POT_NRST = 1'b1;
    DIG_POT_ADDR0 = 1'b1;
    DIG_POT_ADDR1 = 1'b1;
    CUR_MON_ADDR0 = 1'b1;
    CUR_MON_ADDR1 = 1'b1;
    ASIC_IO_EN = 1'b1;
    ASIC_CORE_EN = 1'b1;
    if (cpu_override_output_p == 1'b1 && cpu_override_output_n == 1'b0 && PWR_RSTN == 1'b1) begin
      FPGA_LED0 = gpio[0];
      FPGA_LED1 = gpio[1];
      DIG_POT_INDEP = gpio[2];
      DIG_POT_NRST = gpio[3];
      DIG_POT_ADDR0 = gpio[4];
      DIG_POT_ADDR1 = gpio[5];
      CUR_MON_ADDR0 = gpio[6];
      CUR_MON_ADDR1 = gpio[7];
      ASIC_IO_EN = gpio[8];
      ASIC_CORE_EN = gpio[9];
    end
  end

  (* BOX_TYPE = "user_black_box" *)
  board_ctrl board_ctrl_i
    (.RESET(PWR_RSTN)
    ,.CLK_50(mb_clk_lo)
    ,.CLK_LOCKED(locked_lo)
    ,.axi_iic_dig_pot_Gpo_pin()
    ,.axi_iic_dig_pot_Sda_pin(DIG_POT_SDA)
    ,.axi_iic_dig_pot_Scl_pin(DIG_POT_SCL)
    ,.axi_iic_cur_mon_Gpo_pin()
    ,.axi_iic_cur_mon_Sda_pin(CUR_MON_SDA)
    ,.axi_iic_cur_mon_Scl_pin(CUR_MON_SCL)
    ,.axi_gpio_0_GPIO_IO_O_pin(gpio)
    ,.axi_gpio_0_GPIO2_IO_I_pin()
    ,.axi_uartlite_0_RX_pin(UART_RX)
    ,.axi_uartlite_0_TX_pin(UART_TX));

`endif

  logic [3:0] io_clk_li;
  logic [9:0] io_data_li [3:0];
  logic [3:0] io_token_lo;

  logic [3:0] im_clk_lo;
  logic [9:0] im_data_lo [3:0];
  logic [3:0] token_clk_li;

  // io

  // channel in

  assign io_clk_li = {DOC0, COC0, BOC0, AOC0};

  assign io_data_li = {{DOD9, DOD8, DOD7, DOD6, DOD5, DOD4, DOD3, DOD2, DOD1, DOD0}
                      ,{COD9, COD8, COD7, COD6, COD5, COD4, COD3, COD2, COD1, COD0}
                      ,{BOD9, BOD8, BOD7, BOD6, BOD5, BOD4, BOD3, BOD2, BOD1, BOD0}
                      ,{AOD9, AOD8, AOD7, AOD6, AOD5, AOD4, AOD3, AOD2, AOD1, AOD0}};

  assign {DOT0, COT0, BOT0, AOT0} = io_token_lo;

  // channel out

  assign {DIC0, CIC0, BIC0, AIC0} = im_clk_lo;

  assign {DID9, DID8, DID7, DID6, DID5, DID4, DID3, DID2, DID1, DID0} = im_data_lo[3];
  assign {CID9, CID8, CID7, CID6, CID5, CID4, CID3, CID2, CID1, CID0} = im_data_lo[2];
  assign {BID9, BID8, BID7, BID6, BID5, BID4, BID3, BID2, BID1, BID0} = im_data_lo[1];
  assign {AID9, AID8, AID7, AID6, AID5, AID4, AID3, AID2, AID1, AID0} = im_data_lo[0];

  assign token_clk_li = {DIT0, CIT0, BIT0, AIT0};

  // to asic

  logic clk_r;

  always_ff @(negedge io_master_clk_lo)
    if (~PWR_RSTN)
      clk_r <= 1'b0;
    else
      clk_r <= ~clk_r;

  assign im_clk_lo = {4{clk_r}};

  logic [9:0] cnt_r;

  always_ff @(posedge io_master_clk_lo)
    if (~PWR_RSTN)
      cnt_r <= 10'd0;
    else
      cnt_r <= cnt_r + 1;

  assign im_data_lo = {cnt_r, cnt_r, cnt_r, cnt_r};

  // from asic (non-synthesizable)

  genvar i;
  for (i = 0; i < 4; i++) begin

    always @(posedge io_clk_li[i])
      $display("gateway[%1d], data_in[posedge]:%03x", i, io_data_li[i]);

    always @(negedge io_clk_li[i])
      $display("gateway[%1d], data_in[negedge]:%03x", i, io_data_li[i]);

  end

  // reset for asic
  assign AID10 = ~PWR_RSTN;

  // led
  assign FPGA_LED2 = ~PWR_RSTN;
  assign FPGA_LED3 = ~PWR_RSTN;

endmodule
