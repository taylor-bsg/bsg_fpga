#------------------------------------------------------------
# University of California, San Diego - Bespoke Systems Group
#------------------------------------------------------------
# File: common.tcl
#
# Author: Luis Vega - lvgutierrez@eng.ucsd.edu
#------------------------------------------------------------

# common variable for synplify and xilinx

set bsg_top_name $::env(BSG_TOP_NAME)
set bsg_src_dir $::env(BSG_WORK_DIR)/src
set bsg_out_xps_dir $::env(BSG_OUT_XPS_DIR)
set bsg_out_syn_dir $::env(BSG_OUT_SYN_DIR)

set device_tech spartan6
set device_name xc6slx150
set device_package fgg676
set device_speed_grade -3
