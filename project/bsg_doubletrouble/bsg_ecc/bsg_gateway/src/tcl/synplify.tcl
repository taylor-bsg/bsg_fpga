#------------------------------------------------------------
# University of California, San Diego - Bespoke Systems Group
#------------------------------------------------------------
# File: synplify.tcl
#
# synplify flow
#
# Author: Luis Vega - lvgutierrez@eng.ucsd.edu
#------------------------------------------------------------

source $::env(BSG_WORK_DIR)/src/tcl/common.tcl

project -new $bsg_out_syn_dir

# verilog files
source $bsg_src_dir/tcl/filelist.tcl
foreach f $SVERILOG_SOURCE_FILES {add_file -verilog $f}

# verilog files
source $bsg_src_dir/tcl/include.tcl
foreach f $SVERILOG_INCLUDE_PATHS {set_option -include_path $f}

# constraint
add_file -constraint $bsg_src_dir/fdc/bsg_gateway.fdc

# microblaze (power control)
if {$::env(MICROBLAZE) == 1} {
  add_file -edif $bsg_out_xps_dir/implementation/board_ctrl_axi4lite_0_wrapper.ndf
  add_file -edif $bsg_out_xps_dir/implementation/board_ctrl_axi_gpio_0_wrapper.ndf
  add_file -edif $bsg_out_xps_dir/implementation/board_ctrl_axi_iic_cur_mon_wrapper.ndf
  add_file -edif $bsg_out_xps_dir/implementation/board_ctrl_axi_iic_dig_pot_wrapper.ndf
  add_file -edif $bsg_out_xps_dir/implementation/board_ctrl_axi_uartlite_0_wrapper.ndf
  add_file -edif $bsg_out_xps_dir/implementation/board_ctrl_debug_module_wrapper.ndf
  add_file -edif $bsg_out_xps_dir/implementation/board_ctrl_microblaze_0_bram_block_wrapper.ndf
  add_file -edif $bsg_out_xps_dir/implementation/board_ctrl_microblaze_0_d_bram_ctrl_wrapper.ndf
  add_file -edif $bsg_out_xps_dir/implementation/board_ctrl_microblaze_0_dlmb_wrapper.ndf
  add_file -edif $bsg_out_xps_dir/implementation/board_ctrl_microblaze_0_i_bram_ctrl_wrapper.ndf
  add_file -edif $bsg_out_xps_dir/implementation/board_ctrl_microblaze_0_ilmb_wrapper.ndf
  add_file -edif $bsg_out_xps_dir/implementation/board_ctrl_microblaze_0_wrapper.ndf
  add_file -edif $bsg_out_xps_dir/implementation/board_ctrl_proc_sys_reset_0_wrapper.ndf
  add_file -edif $bsg_out_xps_dir/implementation/board_ctrl.ndf
  add_file -verilog $bsg_out_xps_dir/hdl/board_ctrl.v
  set_option -hdl_define -set MICROBLAZE
}

# options
set_option -technology $device_tech
set_option -part $device_name
set_option -package $device_package
set_option -speed_grade $device_speed_grade
set_option -top_module $bsg_top_name
set_option -symbolic_fsm_compiler 1
set_option -frequency auto
set_option -vlog_std sysv
set_option -enable64bit 1
set_option -resource_sharing 1
set_option -pipe 1
set_option -write_verilog 1
set_option -maxfan 1000

# project
project -result_format "edif"
project -result_file $bsg_out_syn_dir/$bsg_top_name.edn
project -run
project -save $bsg_out_syn_dir/$bsg_top_name.prj
