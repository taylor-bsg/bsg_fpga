#------------------------------------------------------------
# University of California, San Diego - Bespoke Systems Group
#------------------------------------------------------------
# File: xilinx.tcl
#
# xilinx flow
#
# Author: Luis Vega - lvgutierrez@eng.ucsd.edu
#------------------------------------------------------------

source $::env(BSG_WORK_DIR)/src/tcl/common.tcl

set bsg_out_ise_dir $::env(BSG_OUT_ISE_DIR)

set project_dir $bsg_out_ise_dir/$bsg_top_name
set proj_exts [ list ise xise gise ]

foreach ext $proj_exts {
  set proj_name "${project_dir}.$ext"
  if { [ file exists $proj_name ] } {
    file delete $proj_name
  }
}

project new $project_dir

project set family $device_tech
project set device $device_name
project set package $device_package
project set speed $device_speed_grade

project set "Netlist Translation Type" "Timestamp"
project set "Other NGDBuild Command Line Options" "-verbose"
project set "Generate Detailed MAP Report" TRUE
project set {Place & Route Effort Level (Overall)} "High"

xfile add $bsg_out_syn_dir/$bsg_top_name.edn
xfile add $bsg_out_syn_dir/$bsg_top_name.ncf
xfile add $bsg_out_syn_dir/synplicity.ucf
xfile add $bsg_out_xps_dir/implementation/board_ctrl_stub.bmm

project set top $bsg_top_name

process run "Generate Programming File"
