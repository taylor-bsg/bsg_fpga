#------------------------------------------------------------
# University of California, San Diego - Bespoke Systems Group
#------------------------------------------------------------
# File: vcs.tcl
#
# generate library for VCS
# generate filelist for VCS
#
# Author: Luis Vega - lvgutierrez@eng.ucsd.edu
#------------------------------------------------------------

source $::env(BSG_WORK_DIR)/src/tcl/common.tcl
source $::env(BSG_WORK_DIR)/src/tcl/filelist.tcl
source $::env(BSG_WORK_DIR)/src/tcl/include.tcl

if {$::argc > 0 && [lindex $::argv 0] == "library"} {
  puts "library bsg_gateway"
}

if {[info exists SVERILOG_SOURCE_FILES]} {
  set len [llength $SVERILOG_SOURCE_FILES]
}

if {$::argc > 0 && [lindex $::argv 0] == "library"} {
  set i 0
  foreach f $SVERILOG_SOURCE_FILES {
    if {$i == [expr $len - 1]} {
      puts "$f"
    } else {
      puts "$f,"
    }
    incr i
  }
} elseif {$::argc > 0 && [lindex $::argv 0] == "filelist"} {
  foreach f $SVERILOG_SOURCE_FILES {puts $f}
}

if {[info exists SVERILOG_INCLUDE_PATHS]} {
  set len [llength $SVERILOG_INCLUDE_PATHS]
}

if {$::argc > 0 && [lindex $::argv 0] == "library"} {
  if {$len > 0} {
    puts "-incdir"
  }
  set i 0
  foreach f $SVERILOG_INCLUDE_PATHS {
    if {$i == [expr $len - 1]} {
      puts "$f"
    } else {
      puts "$f,"
    }
    incr i
  }
}

if {$::argc > 0 && [lindex $::argv 0] == "library"} {
  puts ";"
}
