Project description
===================

Create basic flash-image (mcs) for Gateway-FPGA on the DoubleTrouble board. This image
contain only a microblaze system (hardware and software) that controls the power
management on DoubleTrouble board. After programming the SPI flash on DoubleTrouble,
everytime you power-up the board it will configure the Gateway-FPGA with this flash-image

How to setup
============

* Remember to define variables in [Makefile.include](https://bitbucket.org/taylor-bsg/bsg_fpga/src/master/project/Makefile.include)

How to build
============

* Run `make`

Where is the flash-image file?
==============================

* After succesfully building it, the flash-image (mcs) should be located at:
    * `bsg_fpga/project/bsg_gateway_flash/out/bsg_gateway_flash.mcs`

How to program
==============

* You will need:
    * The DoubleTrouble board
    * Xilinx Platform Cable USB II
    * Power supply (12V) with molex connector
    * Xilinx Impact
