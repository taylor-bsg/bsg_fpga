#------------------------------------------------------------
# University of California, San Diego - Bespoke Systems Group
#------------------------------------------------------------
# File: vcs.tcl
#
# generate library for VCS
# generate filelist for VCS
#
# Author: Luis Vega - lvgutierrez@eng.ucsd.edu
#------------------------------------------------------------

source $::env(BSG_FPGA_IP_DIR)/bsg_asic/common/tcl/common.tcl
source $::env(BSG_DESIGNS_DIR)/toplevels/$bsg_asic_top_name/tcl/filelist.tcl
source $::env(BSG_DESIGNS_DIR)/toplevels/$bsg_asic_top_name/tcl/include.tcl

if {$::argc > 0 && [lindex $::argv 0] == "library"} {
  puts "library bsg_asic"
}

set len [llength $SVERILOG_SOURCE_FILES]
set i 0

if {$::argc > 0 && [lindex $::argv 0] == "library"} {
  foreach f $SVERILOG_SOURCE_FILES {
    if {$i == [expr $len - 1]} {
      puts "$f"
    } else {
      puts "$f,"
    }
    incr i
  }
} elseif {$::argc > 0 && [lindex $::argv 0] == "filelist"} {
  foreach f $SVERILOG_SOURCE_FILES {puts $f}
}

set len [llength $SVERILOG_INCLUDE_PATHS]
set i 0

if {$::argc > 0 && [lindex $::argv 0] == "library"} {
  if {$len > 0} {
    puts "-incdir"
  }
  foreach f $SVERILOG_INCLUDE_PATHS {
    if {$i == [expr $len - 1]} {
      puts "$f"
    } else {
      puts "$f,"
    }
    incr i
  }
  puts ";"
}
