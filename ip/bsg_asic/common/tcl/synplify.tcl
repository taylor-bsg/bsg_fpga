#------------------------------------------------------------
# University of California, San Diego - Bespoke Systems Group
#------------------------------------------------------------
# File: synplify.tcl
#
# synplify flow
#
# Author: Luis Vega - lvgutierrez@eng.ucsd.edu
#------------------------------------------------------------

source $::env(BSG_FPGA_IP_DIR)/bsg_asic/common/tcl/common.tcl

project -new $bsg_out_syn_dir

# fpga specific files
add_file -verilog $bsg_fpga_ip_dir/bsg_asic/common/v/$bsg_fpga_top_name.v
add_file -verilog $bsg_fpga_ip_dir/bsg_asic/common/v/bsg_asic_clk.v
add_file -constraint $bsg_fpga_ip_dir/bsg_asic/common/fdc/$bsg_fpga_top_name.fdc

# bsg_designs verilog source, filter out designware files since they are
# added as a library using set_option
source $::env(BSG_DESIGNS_DIR)/toplevels/$bsg_asic_top_name/tcl/filelist.tcl

foreach f $SVERILOG_SOURCE_FILES {

  set is_designware [string first $::env(BSG_DESIGNWARE_DIR) $f]

  if {$is_designware != 0} {
    add_file -verilog $f
  }

}

# bsg_designs include paths
source $::env(BSG_DESIGNS_DIR)/toplevels/$bsg_asic_top_name/tcl/include.tcl
foreach f $SVERILOG_INCLUDE_PATHS {set_option -include_path $f}

# options
set_option -technology $device_tech
set_option -part $device_name
set_option -package $device_package
set_option -speed_grade $device_speed_grade
set_option -top_module $bsg_fpga_top_name
set_option -symbolic_fsm_compiler 1
set_option -frequency auto
set_option -vlog_std sysv
set_option -enable64bit 1
set_option -resource_sharing 1
set_option -pipe 1
set_option -write_verilog 1
set_option -maxfan 1000

# designware
set_option -dc_root $::env(DC_HOME)
set_option -dw_library {dw_foundation}
set_option -enable_DesignWare 0

# project
project -result_format "edif"
project -result_file $bsg_out_syn_dir/$bsg_fpga_top_name.edn
project -run
project -save $bsg_out_syn_dir/$bsg_fpga_top_name.prj
