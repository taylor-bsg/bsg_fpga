///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2017 Xilinx, Inc.
// All Rights Reserved
///////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor     : Xilinx
// \   \   \/     Version    : 14.7
//  \   \         Application: Xilinx CORE Generator
//  /   /         Filename   : bsg_asic_chipscope_icon.v
// /___/   /\     Timestamp  : Fri Jun 09 16:05:57 PDT 2017
// \   \  /  \
//  \___\/\___\
//
// Design Name: Verilog Synthesis Wrapper
///////////////////////////////////////////////////////////////////////////////
// This wrapper is used to integrate with Project Navigator and PlanAhead

`timescale 1ns/1ps

module bsg_asic_chipscope_icon(
    CONTROL0) /* synthesis syn_black_box syn_noprune=1 */;


output [35 : 0] CONTROL0;

endmodule
