module bsg_asic_chipscope
  (input clk_i
  ,input [255:0] data_i);

  wire[36:0] ctrl;

  bsg_asic_chipscope_icon icon
    (.CONTROL0(ctrl));

  bsg_asic_chipscope_ila ila
    (.CONTROL(ctrl)
    ,.CLK(clk_i)
    ,.TRIG0(data_i));

endmodule
