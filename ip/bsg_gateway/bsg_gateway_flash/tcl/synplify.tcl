#------------------------------------------------------------
# University of California, San Diego - Bespoke Systems Group
#------------------------------------------------------------
# File: synplify.tcl
#
# Authors: Luis Vega - lvgutierrez@eng.ucsd.edu
#------------------------------------------------------------

# synplify synthesis flow

set bsg_fpga_ip_dir $::env(BSG_FPGA_IP_DIR)
source $bsg_fpga_ip_dir/bsg_gateway/bsg_gateway_flash/tcl/common.tcl

project -new $syn_output_dir

# verilog files
source $bsg_fpga_ip_dir/bsg_gateway/bsg_gateway_flash/tcl/synplify_files.tcl

# options
set_option -technology $device_tech
set_option -part $device_name
set_option -package $device_package
set_option -speed_grade $device_speed_grade
set_option -top_module $bsg_top_name
set_option -include_path $bsg_ip_cores_dir/bsg_misc
set_option -symbolic_fsm_compiler 1
set_option -frequency auto
set_option -vlog_std sysv
set_option -enable64bit 1
set_option -resource_sharing 1
set_option -pipe 1
set_option -write_verilog 1
set_option -maxfan 1000

# project
project -result_format "edif"
project -result_file $syn_output_dir/$bsg_top_name.edn
project -run
project -save $syn_output_dir/$bsg_top_name.prj
