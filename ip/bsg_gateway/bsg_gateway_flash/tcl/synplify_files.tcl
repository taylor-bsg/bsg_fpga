#------------------------------------------------------------
# University of California, San Diego - Bespoke Systems Group
#------------------------------------------------------------
# File: synplify_files.tcl
#
# Authors: Luis Vega - lvgutierrez@eng.ucsd.edu
#------------------------------------------------------------

# bsg gateway (top)
add_file -verilog $bsg_fpga_ip_dir/bsg_gateway/bsg_gateway_flash/v/$bsg_top_name.v
add_file -constraint $bsg_fpga_ip_dir/bsg_gateway/bsg_gateway_flash/fdc/$bsg_top_name.fdc

# bsg gateway pll flash
add_file -verilog $bsg_fpga_ip_dir/bsg_gateway/bsg_gateway_flash/v/bsg_gateway_flash_pll.v

# microblaze (power control)
add_file -edif $xps_output_dir/implementation/board_ctrl_axi4lite_0_wrapper.ndf
add_file -edif $xps_output_dir/implementation/board_ctrl_axi_gpio_0_wrapper.ndf
add_file -edif $xps_output_dir/implementation/board_ctrl_axi_iic_cur_mon_wrapper.ndf
add_file -edif $xps_output_dir/implementation/board_ctrl_axi_iic_dig_pot_wrapper.ndf
add_file -edif $xps_output_dir/implementation/board_ctrl_axi_uartlite_0_wrapper.ndf
add_file -edif $xps_output_dir/implementation/board_ctrl_debug_module_wrapper.ndf
add_file -edif $xps_output_dir/implementation/board_ctrl_microblaze_0_bram_block_wrapper.ndf
add_file -edif $xps_output_dir/implementation/board_ctrl_microblaze_0_d_bram_ctrl_wrapper.ndf
add_file -edif $xps_output_dir/implementation/board_ctrl_microblaze_0_dlmb_wrapper.ndf
add_file -edif $xps_output_dir/implementation/board_ctrl_microblaze_0_i_bram_ctrl_wrapper.ndf
add_file -edif $xps_output_dir/implementation/board_ctrl_microblaze_0_ilmb_wrapper.ndf
add_file -edif $xps_output_dir/implementation/board_ctrl_microblaze_0_wrapper.ndf
add_file -edif $xps_output_dir/implementation/board_ctrl_proc_sys_reset_0_wrapper.ndf
add_file -edif $xps_output_dir/implementation/board_ctrl.ndf
add_file -verilog $xps_output_dir/hdl/board_ctrl.v
