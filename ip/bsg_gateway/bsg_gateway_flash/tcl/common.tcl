#------------------------------------------------------------
# University of California, San Diego - Bespoke Systems Group
#------------------------------------------------------------
# File: common.tcl
#
# Authors: Luis Vega - lvgutierrez@eng.ucsd.edu
#------------------------------------------------------------

# common variable for synplify and xilinx

set bsg_top_name $::env(BSG_TOP_NAME)

set bsg_work_dir $::env(BSG_WORK_DIR)
set bsg_ip_cores_dir $::env(BSG_IP_CORES_DIR)
set bsg_fpga_ip_dir $::env(BSG_FPGA_IP_DIR)

set syn_output_dir $bsg_work_dir/out/syn
set ise_output_dir $bsg_work_dir/out/ise
set xps_output_dir $bsg_work_dir/out/xps

set device_tech spartan6
set device_name xc6slx150
set device_package fgg676
set device_speed_grade -3
