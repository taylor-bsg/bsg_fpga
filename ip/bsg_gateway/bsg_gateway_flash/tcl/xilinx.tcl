#------------------------------------------------------------
# University of California, San Diego - Bespoke Systems Group
#------------------------------------------------------------
# File: xilinx.tcl
#
# Authors: Luis Vega - lvgutierrez@eng.ucsd.edu
#------------------------------------------------------------

# xilinx flow

set bsg_fpga_ip_dir $::env(BSG_FPGA_IP_DIR)
source $bsg_fpga_ip_dir/bsg_gateway/bsg_gateway_flash/tcl/common.tcl

set project_dir $ise_output_dir/$bsg_top_name
set proj_exts [ list ise xise gise ]

foreach ext $proj_exts {
  set proj_name "${project_dir}.$ext"
  if { [ file exists $proj_name ] } {
    file delete $proj_name
  }
}

project new $project_dir

project set family $device_tech
project set device $device_name
project set package $device_package
project set speed $device_speed_grade

project set "Netlist Translation Type" "Timestamp"
project set "Other NGDBuild Command Line Options" "-verbose"
project set "Generate Detailed MAP Report" TRUE
project set {Place & Route Effort Level (Overall)} "High"

xfile add $syn_output_dir/$bsg_top_name.edn
xfile add $syn_output_dir/$bsg_top_name.ncf
xfile add $syn_output_dir/synplicity.ucf
xfile add $xps_output_dir/implementation/board_ctrl_stub.bmm

project set top $bsg_top_name

process run "Generate Programming File"
