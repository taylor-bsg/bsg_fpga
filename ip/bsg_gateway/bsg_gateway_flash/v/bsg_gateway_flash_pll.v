//------------------------------------------------------------
// University of California, San Diego - Bespoke Systems Group
//------------------------------------------------------------
// File: bsg_gateway_flash_pll.v
//
// Authors: Luis Vega - lvgutierrez@eng.ucsd.edu
//------------------------------------------------------------

module bsg_gateway_flash_pll
  (input clk_150_mhz_p_i, clk_150_mhz_n_i
  ,output clk_50_mhz_o
  ,output locked_o);

  logic ibufgds_clk_150_mhz_lo;

  IBUFGDS #
    (.DIFF_TERM("TRUE"))
  ibufgds_inst
    (.I(clk_150_mhz_p_i) ,.IB(clk_150_mhz_n_i)
    ,.O(ibufgds_clk_150_mhz_lo));

  logic pll_fb_lo;
  logic pll_50_mhz_lo;

  PLL_ADV #
    (.BANDWIDTH("OPTIMIZED")
    ,.CLKFBOUT_MULT(7)
    ,.CLKFBOUT_PHASE(0.0)
    ,.CLKIN1_PERIOD(6.667)
    ,.CLKIN2_PERIOD(6.667)
    // clk 50 MHz
    ,.CLKOUT0_DIVIDE(21)
    ,.CLKOUT0_DUTY_CYCLE(0.5)
    ,.CLKOUT0_PHASE(0.0)
    ,.CLKOUT1_DIVIDE(1)
    ,.CLKOUT1_DUTY_CYCLE(0.5)
    ,.CLKOUT1_PHASE(0.0)
    ,.CLKOUT2_DIVIDE(1)
    ,.CLKOUT2_DUTY_CYCLE(0.5)
    ,.CLKOUT2_PHASE(0.0)
    ,.CLKOUT3_DIVIDE(21)
    ,.CLKOUT3_DUTY_CYCLE(0.5)
    ,.CLKOUT3_PHASE(0.0)
    ,.CLKOUT4_DIVIDE(1)
    ,.CLKOUT4_DUTY_CYCLE(0.5)
    ,.CLKOUT4_PHASE(0.0)
    ,.CLKOUT5_DIVIDE(8)
    ,.CLKOUT5_DUTY_CYCLE(0.5)
    ,.CLKOUT5_PHASE(0.0)
    ,.COMPENSATION("INTERNAL")
    ,.DIVCLK_DIVIDE(1)
    ,.REF_JITTER(0.100)
    ,.SIM_DEVICE("SPARTAN6"))
  pll_adv_inst
    (.CLKFBDCM()
    ,.CLKFBOUT(pll_fb_lo)
    ,.CLKOUT0(pll_50_mhz_lo)
    ,.CLKOUT1()
    ,.CLKOUT2()
    ,.CLKOUT3()
    ,.CLKOUT4()
    ,.CLKOUT5()
    ,.CLKOUTDCM0()
    ,.CLKOUTDCM1()
    ,.CLKOUTDCM2()
    ,.CLKOUTDCM3()
    ,.CLKOUTDCM4()
    ,.CLKOUTDCM5()
    ,.DO()
    ,.DRDY()
    ,.LOCKED(locked_o)
    ,.CLKFBIN(pll_fb_lo)
    ,.CLKIN1(ibufgds_clk_150_mhz_lo)
    ,.CLKIN2(1'b0)
    ,.CLKINSEL(1'b1)
    ,.DADDR(5'b00000)
    ,.DCLK(1'b0)
    ,.DEN(1'b0)
    ,.DI(16'h0000)
    ,.DWE(1'b0)
    ,.RST(1'b0)
    ,.REL(1'b0));

  // clock 50 mhz
  BUFG bufg_50_inst
    (.I(pll_50_mhz_lo)
    ,.O(clk_50_mhz_o));

endmodule
