//------------------------------------------------------------
// University of California, San Diego - Bespoke Systems Group
//------------------------------------------------------------
// File: bsg_gateway_flash.v
//
// Authors: Luis Vega - lvgutierrez@eng.ucsd.edu
//------------------------------------------------------------

module bsg_gateway_flash
  // clk osc
  (input CLK_OSC_P, CLK_OSC_N
  // reset
  ,input PWR_RSTN
  // voltage-rail enable
  ,output logic ASIC_CORE_EN, ASIC_IO_EN
  // current monitor
  ,output logic CUR_MON_ADDR0, CUR_MON_ADDR1
  ,inout CUR_MON_SCL, CUR_MON_SDA
  // potentiometer
  ,output logic DIG_POT_ADDR0, DIG_POT_ADDR1
  ,output logic DIG_POT_INDEP, DIG_POT_NRST
  ,inout DIG_POT_SCL, DIG_POT_SDA
  // uart
  ,input UART_RX
  ,output UART_TX
  // led
  ,output logic FPGA_LED0, FPGA_LED1);

  // clock generation

  logic clk_50_mhz_lo;
  logic locked_lo;

  bsg_gateway_flash_pll pll_inst
    (.clk_150_mhz_p_i(CLK_OSC_P) ,.clk_150_mhz_n_i(CLK_OSC_N)
    ,.clk_50_mhz_o(clk_50_mhz_lo)
    ,.locked_o(locked_lo));

  // power control

  logic [11:0] gpio;
  logic cpu_override_output_p;
  logic cpu_override_output_n;

  assign cpu_override_output_p = gpio[11];
  assign cpu_override_output_n = gpio[10];

  always_comb begin
    FPGA_LED0 = 1'b1;
    FPGA_LED1 = 1'b1;
    DIG_POT_INDEP = 1'b1;
    DIG_POT_NRST = 1'b1;
    DIG_POT_ADDR0 = 1'b1;
    DIG_POT_ADDR1 = 1'b1;
    CUR_MON_ADDR0 = 1'b1;
    CUR_MON_ADDR1 = 1'b1;
    ASIC_IO_EN = 1'b1;
    ASIC_CORE_EN = 1'b1;
    if (cpu_override_output_p == 1'b1 && cpu_override_output_n == 1'b0 && PWR_RSTN == 1'b1) begin
      FPGA_LED0 = gpio[0];
      FPGA_LED1 = gpio[1];
      DIG_POT_INDEP = gpio[2];
      DIG_POT_NRST = gpio[3];
      DIG_POT_ADDR0 = gpio[4];
      DIG_POT_ADDR1 = gpio[5];
      CUR_MON_ADDR0 = gpio[6];
      CUR_MON_ADDR1 = gpio[7];
      ASIC_IO_EN = gpio[8];
      ASIC_CORE_EN = gpio[9];
    end
  end

  (* BOX_TYPE = "user_black_box" *)
  board_ctrl board_ctrl_i
    (.RESET(PWR_RSTN)
    ,.CLK_50(clk_50_mhz_lo)
    ,.CLK_LOCKED(locked_lo)
    ,.axi_iic_dig_pot_Gpo_pin()
    ,.axi_iic_dig_pot_Sda_pin(DIG_POT_SDA)
    ,.axi_iic_dig_pot_Scl_pin(DIG_POT_SCL)
    ,.axi_iic_cur_mon_Gpo_pin()
    ,.axi_iic_cur_mon_Sda_pin(CUR_MON_SDA)
    ,.axi_iic_cur_mon_Scl_pin(CUR_MON_SCL)
    ,.axi_gpio_0_GPIO_IO_O_pin(gpio)
    ,.axi_gpio_0_GPIO2_IO_I_pin()
    ,.axi_uartlite_0_RX_pin(UART_RX)
    ,.axi_uartlite_0_TX_pin(UART_TX));

endmodule
